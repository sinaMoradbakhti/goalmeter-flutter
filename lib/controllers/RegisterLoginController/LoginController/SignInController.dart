import 'package:flutter/material.dart';
import 'package:flutter/material.dart' as prefix0;
import 'package:goal_meter/models/Statics/Strings.dart';
import 'package:goal_meter/models/Statics/Statics.dart';
import 'package:goal_meter/controllers/RegisterLoginController/middleWare.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:goal_meter/controllers/mainTabbar/MainTabBar.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:goal_meter/models/User/User.dart';

class SignInController extends StatefulWidget {

  static Widget btnSignInLoading = Center(
    child: CircularProgressIndicator(
      valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
    ),
  );
  static Widget btnSignInText = Text(Strings.shared.signInFirstPage, style: TextStyle(fontSize: 20, color: Colors.white), textAlign: TextAlign.center);
  static Widget btnSignIn = btnSignInText;

  @override
  _SignInControllerState createState() => _SignInControllerState();
}

class _SignInControllerState extends State<SignInController> {

  final FirebaseAuth auth = FirebaseAuth.instance;
  Future<FirebaseUser> loginUserWithFireBase(mainContext) async {
    print("USER*****: ${MiddleWare.shared.emailLoginController.text} - ${MiddleWare.shared.passwordLoginController}");
    auth.signInWithEmailAndPassword(
        email: MiddleWare.shared.emailLoginController.text,
        password: MiddleWare.shared.passwordLoginController.text
    ).then((FirebaseUser usr)=>(errorWhileSigningUp(mainContext,Strings.shared.userSingInSuccess,true,usr))).catchError((e)=>errorWhileSigningUp(mainContext,e.toString(),false,null));
  }
  void errorWhileSigningUp(mainContext, String Error, bool isSuccess,FirebaseUser usr) async
  {
    if(!isSuccess)
    {
      setState(() {
        SignInController.btnSignIn = SignInController.btnSignInText;
      });
      var msg = Statics.shared.snackBarMsg(Error);
      Scaffold.of(mainContext).showSnackBar(msg);
    }
    else
    {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setBool(Statics.shared.sharedPreferencesIndexes.isAppLoggedIn,true);

      UserObject.shared.updateInfoToCache(usr.displayName, usr.email, usr.photoUrl, usr.uid,usr.phoneNumber);

      //Statics.shared.data.refreshAllGoals();
      Navigator.pushReplacement(context, new MaterialPageRoute(builder: (context) => new MainTabBar()));
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: AppBar(
          title: Text(Strings.shared.signInFirstPage, style: TextStyle(color: Color.fromRGBO(53, 104, 89, 1), fontSize: 22)),
          backgroundColor: Colors.white,
          centerTitle: true,
          elevation: 0,
          iconTheme: IconThemeData(
              color: Color.fromRGBO(0, 0, 0, 1)
          ),
        ),
        backgroundColor: Colors.white,
        body: Builder(
          builder: (mainContext) =>
              Container(
                margin: const EdgeInsets.only(left: 16, right: 16, top: 16, bottom: 16),
                child: ListView(
                  children: [
                    Container(
                        child: TextField(
                          controller: MiddleWare.shared.emailLoginController,
                          style: TextStyle(fontSize: 20,color: Colors.black),
                          decoration: InputDecoration(
                            hintText: Strings.shared.emailHint,
                            labelText: Strings.shared.emailHint,
                            hintStyle: TextStyle(fontSize: 20, color: Colors.grey),
                            icon: Image.asset('Resources/icons/email.png', width: 35, height: 35),
                            border: OutlineInputBorder(),
                          ),
                        ),
                        padding: const EdgeInsets.only(left: 16, right: 16),
                        height: 55,
                        margin: const EdgeInsets.only(bottom: 20)
                    ),
                    Container(
                        child: TextField(
                          controller: MiddleWare.shared.passwordLoginController,
                          style: TextStyle(fontSize: 20,color: Colors.black),
                          obscureText: true,
                          decoration: InputDecoration(
                            hintText: Strings.shared.passwordHint,
                            labelText: Strings.shared.passwordHint,
                            hintStyle: TextStyle(fontSize: 20, color: Colors.grey),
                            icon: Image.asset('Resources/icons/key.png', width: 35, height: 35,),
                            border: OutlineInputBorder(),
                          ),
                        ),
                        padding: const EdgeInsets.only(left: 16, right: 16),
                        height: 55,
                        margin: const EdgeInsets.only(bottom: 40)
                    ),
                    Container(
                      child: ButtonTheme(
                        child: FlatButton(
                          child: SignInController.btnSignIn,
                          onPressed: (){
                            setState(() {
                              SignInController.btnSignIn = SignInController.btnSignInLoading;
                              var md = MiddleWare.shared;
                              if(md.emailLoginController.text.isEmpty || md.passwordLoginController.text.isEmpty)
                              {
                                SignInController.btnSignIn = SignInController.btnSignInText;
                                var msg = Statics.shared.snackBarMsg(Strings.shared.fillTheFieldsSnackBar);
                                Scaffold.of(mainContext).showSnackBar(msg);
                              }
                              else
                              {
                                loginUserWithFireBase(mainContext);
                              }
                            });
                          },
                          //color: Colors.blue,
                          padding: const EdgeInsets.only(top: 12.5, bottom: 12.5),
                        ),
                        minWidth: (MediaQuery.of(context).size.width - 64),
                      ),
                      decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), color: Colors.blue),
                      margin: const EdgeInsets.only(left: 16, right: 16),
                    ),
                  ],
                ),
              ),
        )
    );
  }
}
