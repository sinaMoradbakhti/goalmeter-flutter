import 'package:flutter/material.dart';
import 'package:goal_meter/controllers/RegisterLoginController/middleWare.dart';
import 'package:goal_meter/models/Statics/Statics.dart';
import 'package:goal_meter/models/Statics/Strings.dart';
import 'package:goal_meter/models/User/User.dart';
import 'functions.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:goal_meter/controllers/mainTabbar/MainTabBar.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:goal_meter/models/User/User.dart';

class RegisterLoginController extends StatefulWidget {

  ProgressDialog pr;
  String btnGoogleDefaultImg = "Resources/buttons/googleSignin.png";

  final GoogleSignIn googleSignIn = GoogleSignIn();
  final FirebaseAuth auth = FirebaseAuth.instance;


  @override
  _RegisterLoginControllerState createState() => _RegisterLoginControllerState();
}

class _RegisterLoginControllerState extends State<RegisterLoginController> {

  Future<FirebaseUser> handleSignInWithGoogle() async {
    this.widget.pr.show();
    final GoogleSignInAccount googleUser = await this.widget.googleSignIn.signIn();
    final GoogleSignInAuthentication googleAuth = await googleUser.authentication;

    final AuthCredential credential = GoogleAuthProvider.getCredential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );

    final FirebaseUser user = await this.widget.auth.signInWithCredential(credential);
    print("signed in " + user.displayName);

    UserObject.shared.updateInfoToCache(user.displayName, user.email, user.photoUrl, user.uid,user.phoneNumber);

    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool(Statics.shared.sharedPreferencesIndexes.isAppLoggedIn,true);

    Statics.shared.app.isSignedInWithGoogle = true;
    refreshGoogleSignData();

    return user;
  }
  RegisterLoginController()
  {
    refreshGoogleSignData();
  }
  void refreshGoogleSignData()
  {
    setState(() {
      if(Statics.shared.app.isSignedInWithGoogle)
      {
        this.widget.btnGoogleDefaultImg = "Resources/buttons/googleSignout.png";
        //Statics.shared.data.refreshAllGoals();
        Navigator.pushReplacement(context, new MaterialPageRoute(builder: (context) => new MainTabBar()));
      }
      else
      {
        this.widget.pr.hide();
        this.widget.btnGoogleDefaultImg = "Resources/buttons/googleSignin.png";
      }
    });
  }

  @override
  Widget build(BuildContext context) {

    if(MiddleWare.shared.isFirstInit){
      this.widget.pr = new ProgressDialog(context,ProgressDialogType.Normal);
      this.widget.pr.setMessage(Strings.shared.modalProgressLoading);
      MiddleWare.shared.isFirstInit = false;
    }

    return new Scaffold(
        backgroundColor: Colors.white,
        body: new Container(
            height: (MediaQuery.of(context).size.height),
            width: (MediaQuery.of(context).size.width),
            decoration: new BoxDecoration(
              image: new DecorationImage(
                  image: new AssetImage('Resources/Images/registerloginpage.jpg'),
                  fit: BoxFit.cover
              ),
            ),
            child: new Column(
              mainAxisSize: MainAxisSize.max,
              children: [
                new Container(height: (MediaQuery.of(context).size.height * 0.6))
                ,
                new Container(
                  height: (MediaQuery.of(context).size.height * 0.4),
                  alignment: Alignment.bottomCenter,
                  width: (MediaQuery.of(context).size.width),
                  child: new Column(
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      ButtonTheme(
                        child: FlatButton(
                          child: Image.asset(widget.btnGoogleDefaultImg,fit: BoxFit.cover,height: 60,width: (MediaQuery.of(context).size.width) * 0.9),
                          padding: const EdgeInsets.all(0),
                          onPressed: (){
                            setState(() {
                              if(!Statics.shared.app.isSignedInWithGoogle)
                              {
                                handleSignInWithGoogle()
                                    .then((FirebaseUser user) {print("FireBaseXXX Ok");})
                                    .catchError((e) {this.widget.pr.hide();});
                              }
                              else
                              {
                                widget.googleSignIn.signOut();
                                Statics.shared.app.isSignedInWithGoogle = false;
                                refreshGoogleSignData();
                              }
                            });
                          },
                        ),
                        height: 60,
                      ),
                      SizedBox(height: 3),
                      facebookSigninButton(context),
                      SizedBox(height: 3),
                      normalSigninButton(context),
                      new Expanded(child: Container(),),
                      privacyContainer()
                    ],
                  ),
                )
              ],
            )
        )
    );
  }
}
