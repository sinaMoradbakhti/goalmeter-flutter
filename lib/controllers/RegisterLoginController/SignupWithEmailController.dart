import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:goal_meter/controllers/RegisterLoginController/functions.dart';
import 'package:goal_meter/models/Statics/Statics.dart';
import 'package:goal_meter/models/Statics/Strings.dart';
import 'middleWare.dart';
import 'package:goal_meter/models/Statics/Statics.dart';
import 'package:goal_meter/controllers/RegisterLoginController/LoginController/SignInController.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:goal_meter/controllers/mainTabbar/MainTabBar.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:goal_meter/models/User/User.dart';

class SignupWithEmail extends StatefulWidget {

  bool passwordEyeWidgetStatus = false;
  Widget passwordEyeWidget = Icon(Icons.remove_red_eye, color: Colors.grey,);
  static const Widget passwordEyeWidgetOn = Icon(Icons.remove_red_eye, color: Colors.black,);
  static const Widget passwordEyeWidgetOff = Icon(Icons.remove_red_eye, color: Colors.grey,);

  static Widget pressLoginBtnWidget = Text("${Strings.shared.signInFirstPage}", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17, decoration: TextDecoration.underline));
  static Widget pressSignUpBtnWidget = Text(Strings.shared.signUpFirstPage, style: TextStyle(fontSize: 20, color: Colors.white), textAlign: TextAlign.center);
  static Widget pressSignUpLoadingWidget = Center(
    child: CircularProgressIndicator(
      valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
    ),
  );
  static var btnSignUp = SignupWithEmail.pressSignUpBtnWidget;

  final FirebaseAuth auth = FirebaseAuth.instance;

  @override
  _SignupWithEmailState createState() => _SignupWithEmailState();
}

class _SignupWithEmailState extends State<SignupWithEmail> {

  double btnSignUpWidth = 100;
  void signUpBtnWidth(context,bool isLoading){
    setState(() {
      if(isLoading)
      {
        btnSignUpWidth = 100;
      }
      else
      {
        btnSignUpWidth = (MediaQuery.of(context).size.width - 64);
      }
    });
  }
  Future<FirebaseUser> createUserWithFireBase(mainContext) async {
    widget.auth.createUserWithEmailAndPassword(
        email: MiddleWare.shared.emailController.text,
        password: MiddleWare.shared.passwordController.text
    ).then((FirebaseUser usr)=>(errorWhileSigningUp(mainContext,Strings.shared.userSingUpSuccess,true,usr))).catchError((e)=>errorWhileSigningUp(mainContext,e.toString(),false,null));
  }
  void errorWhileSigningUp(mainContext, String Error,bool isSuccess,FirebaseUser usr) async
  {
    if(!isSuccess)
    {
      this.setState((){
        signUpBtnWidth(context,false);
        SignupWithEmail.btnSignUp = SignupWithEmail.pressSignUpBtnWidget;
        var msg = Statics.shared.snackBarMsg(Error);
        Scaffold.of(mainContext).showSnackBar(msg);
      });
    }
    else
    {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setBool(Statics.shared.sharedPreferencesIndexes.isAppLoggedIn,true);

      UserObject.shared.updateInfoToCache(usr.displayName, usr.email, usr.photoUrl, usr.uid,usr.phoneNumber);

      //Statics.shared.data.refreshAllGoals();
      Navigator.pushReplacement(context, new MaterialPageRoute(builder: (context) => new MainTabBar()));
    }
  }

  @override
  Widget build(BuildContext context) {
    signUpBtnWidth(context, false);
    return new Scaffold(
        appBar: AppBar(
          title: Text(Strings.shared.signUpWithEmail, style: TextStyle(color: Color.fromRGBO(53, 104, 89, 1), fontSize: 22)),
          backgroundColor: Colors.white,
          centerTitle: true,
          elevation: 0,
          iconTheme: IconThemeData(
            color: Color.fromRGBO(0, 0, 0, 1)
          ),
        ),
        backgroundColor: Colors.white,
        body: Builder(
          builder: (mainContext) =>
              Container(
                margin: const EdgeInsets.only(left: 16, right: 16, top: 16, bottom: 16),
                child: ListView(
                  children: [
                    Container(
                      child: TextField(
                        controller: MiddleWare.shared.fullNameController,
                        style: TextStyle(fontSize: 20,color: Colors.black),
                        decoration: InputDecoration(
                          labelText: Strings.shared.fullNameHint,
                          hintText: Strings.shared.fullNameHint,
                          hintStyle: TextStyle(fontSize: 20, color: Colors.grey),
                          icon: Image.asset('Resources/icons/user.png', width: 35, height: 35),
                          border: OutlineInputBorder(),
                        ),
                      ),
                      padding: const EdgeInsets.only(left: 16, right: 16),
                      height: 55,
                      margin: const EdgeInsets.only(bottom: 20),
                    ),
                    Container(
                        child: TextField(
                          controller: MiddleWare.shared.emailController,
                          style: TextStyle(fontSize: 20,color: Colors.black),
                          decoration: InputDecoration(
                              labelText: Strings.shared.emailHint,
                              hintText: Strings.shared.emailHint,
                              hintStyle: TextStyle(fontSize: 20, color: Colors.grey),
                              icon: Image.asset('Resources/icons/email.png', width: 35, height: 35),
                              border: OutlineInputBorder()
                          ),
                        ),
                        padding: const EdgeInsets.only(left: 16, right: 16),
                        height: 55,
                        margin: const EdgeInsets.only(bottom: 20)
                    ),
                    Container(
                        child: TextField(
                          controller: MiddleWare.shared.passwordController,
                          style: TextStyle(fontSize: 20,color: Colors.black),
                          obscureText: !this.widget.passwordEyeWidgetStatus,
                          decoration: InputDecoration(
                              labelText: Strings.shared.passwordHint,
                              hintText: Strings.shared.passwordHint,
                              hintStyle: TextStyle(fontSize: 20, color: Colors.grey),
                              icon: Image.asset('Resources/icons/key.png', width: 35, height: 35,),
                              border: OutlineInputBorder(),
                              suffixIcon: IconButton(
                                icon: this.widget.passwordEyeWidget,
                                onPressed: (){
                                  setState(() {
                                    if(!this.widget.passwordEyeWidgetStatus){
                                      this.widget.passwordEyeWidget = SignupWithEmail.passwordEyeWidgetOn;
                                      this.widget.passwordEyeWidgetStatus = true;
                                    }
                                    else{
                                      this.widget.passwordEyeWidget = SignupWithEmail.passwordEyeWidgetOff;
                                      this.widget.passwordEyeWidgetStatus = false;
                                    }
                                  });
                                },
                                padding: const EdgeInsets.all(0),
                              )// icon Button
                          ),
                        ),
                        padding: const EdgeInsets.only(left: 16, right: 16),
                        height: 55,
                        margin: const EdgeInsets.only(bottom: 40)
                    ),
                    Container(
                      child: ButtonTheme(
                        child: FlatButton(
                          child: SignupWithEmail.btnSignUp,
                          onPressed: (){
                            setState(() {
                              signUpBtnWidth(context,true);
                              SignupWithEmail.btnSignUp = SignupWithEmail.pressSignUpLoadingWidget;
                              var md = MiddleWare.shared;
                              if(md.fullNameController.text.isEmpty || md.emailController.text.isEmpty || md.passwordController.text.isEmpty)
                              {
                                signUpBtnWidth(context,false);
                                SignupWithEmail.btnSignUp = SignupWithEmail.pressSignUpBtnWidget;
                                var msg = Statics.shared.snackBarMsg(Strings.shared.fillTheFieldsSnackBar);
                                Scaffold.of(mainContext).showSnackBar(msg);
                              }
                              else
                              {
                                createUserWithFireBase(mainContext);
                              }
                            });
                          },
                          //color: Colors.blue,
                          padding: const EdgeInsets.only(top: 12.5, bottom: 12.5),
                        ),
                        minWidth: btnSignUpWidth,
                      ),
                      decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), color: Colors.blue),
                      margin: const EdgeInsets.only(right: 16, left: 16),
                    ),
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Center(
                            child: Text(Strings.shared.haveAnAccountQ,style: TextStyle(fontWeight: FontWeight.normal, fontSize: 17)),
                          ),
                          Center(
                              child: FlatButton(
                                child: SignupWithEmail.pressLoginBtnWidget,
                                color: Colors.transparent,
                                padding: const EdgeInsets.all(0),
                                onPressed: (){
                                  Navigator.push(context, new MaterialPageRoute(builder: (context) => SignInController()));
                                },
                              )
                          )
                        ],
                      ),
                      margin: const EdgeInsets.only(top: 20),
                    )
                  ],
                ),
              ),
        )
    );
  }
}
