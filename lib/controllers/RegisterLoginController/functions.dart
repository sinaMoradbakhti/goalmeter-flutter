import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'SignupWithEmailController.dart';


Container privacyContainer()
{
  return new Container(
      child: Center(
          child: new Row(
            children: [
              RaisedButton(
                onPressed: (){},
                child: Text("Terms & Conditions", style: TextStyle(
                    fontSize: 16, color: Colors.white, fontWeight: FontWeight.bold)
                ),
                color: Colors.transparent,
                splashColor: Colors.transparent,
                elevation: 0,
              ),
              Text(" and",
                style: TextStyle(fontSize: 17, color: Colors.white),
              ),
              RaisedButton(
                onPressed: (){},
                child: Text("Privacy & Policy", style: TextStyle(
                    fontSize: 16, color: Colors.white, fontWeight: FontWeight.bold)
                ),
                color: Colors.transparent,
                splashColor: Colors.transparent,
                elevation: 0,
              ),
            ],
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
          )
      ),
    height: 50,
  );
}
ButtonTheme facebookSigninButton(context)
{
  return ButtonTheme(
    child: FlatButton(
      child: Image.asset('Resources/buttons/facbookSignin.png',fit: BoxFit.cover,height: 60,width: (MediaQuery.of(context).size.width) * 0.9),
      padding: const EdgeInsets.all(0),
      onPressed: (){},
    ),
    height: 60,
  );
}
ButtonTheme normalSigninButton(context)
{
  return ButtonTheme(
    child: FlatButton(
      child: Image.asset('Resources/buttons/signin.png',fit: BoxFit.cover,height: 60,width: (MediaQuery.of(context).size.width) * 0.9),
      padding: const EdgeInsets.all(0),
      onPressed: (){
        Navigator.push(context, new MaterialPageRoute(builder: (context) => new SignupWithEmail()));
      },
    ),
    height: 60,
  );
}