import 'package:flutter/material.dart';
import 'package:flutter/material.dart' as prefix0;
import 'package:goal_meter/models/DropDownWidgets/languages.dart';
import 'package:goal_meter/models/Statics/Statics.dart';
import 'package:shared_preferences/shared_preferences.dart';

List<DropdownMenuItem> dropDownLanguagesList()
{
  List<DropdownMenuItem> items = [];
  for(var i = 0; i < languagesList.length; i++) {
    items.add(
        DropdownMenuItem(
          child: LanguagesDropdownWidget(languagesList[i].flag,languagesList[i].title),
          key: Key("${languagesList[i].index}"),
          value: languagesList[i].title,
        )
    );
  }
  return items;
}
void setLanguageInCache(int langIndex) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setInt(Statics.shared.sharedPreferencesIndexes.appLang, langIndex);
}