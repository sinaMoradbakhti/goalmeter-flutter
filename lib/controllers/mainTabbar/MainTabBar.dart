import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:goal_meter/controllers/mainTabbar/MiddleWare/MiddleWare.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:goal_meter/models/Statics/Statics.dart';
import 'package:goal_meter/models/User/User.dart';

class MainTabBar extends StatefulWidget {

  BottomNavigationBar navBar;
  MainTabBar mainTabBar;
  MainTabBarState myChild;
  MiddleWare mdw = MiddleWare.shared;

  @override
  MainTabBarState createState(){
    this.mainTabBar = this;
    return MainTabBarState();
  }
}

class MainTabBarState extends State<MainTabBar> with TickerProviderStateMixin {

  GlobalKey scaffold = GlobalKey();
  void clickToChangeMenu(index)
  {
    setState(() {
      MiddleWare.shared.variables.currentIndex = index;
    });
    //MiddleWare.shared.variables.tabc.animateTo(index);
    MiddleWare.shared.variables.tabc.index = index;
  }

  MainTabBarState(){
    MiddleWare.shared.variables.tabc = TabController(length: 5, vsync: this);
    // update user in FireStore
    UserObject.shared.updateInfoFromCache().then((user){
      Firestore.instance
          .collection('users')
          .document(user.uid)
          .get()
          .then((DocumentSnapshot ds) {
        if(!ds.exists)
        {
          Firestore.instance.collection('users').document(UserObject.shared.uid)
              .setData({ 'uid': user.uid, 'phoneNumber': user.phoneNumber,'email': user.email,'photo': user.photoURL,'fullName': user.fullName});

          Statics.shared.user.uid = user.uid;
          Statics.shared.user.email = user.email;
          Statics.shared.user.fullName = user.fullName;
          Statics.shared.user.photoURL = user.photoURL;
          Statics.shared.user.phoneNumber = user.phoneNumber;
        }
        else
        {
          Statics.shared.user.uid = user.uid;
          Statics.shared.user.email = user.email;
          Statics.shared.user.fullName = user.fullName;
          Statics.shared.user.photoURL = user.photoURL;
          Statics.shared.user.phoneNumber = user.phoneNumber;
        }
        // use ds as a snapshot
      }).catchError((error){
        print("ERRORX:${error.toString()}");
      });
    });

  }

  @override
  Widget build(BuildContext context) {

    this.widget.myChild = this;
    this.widget.navBar = BottomNavigationBar(
      items: [
        BottomNavigationBarItem(icon: new Icon(FontAwesomeIcons.list, size: 20), title: Text("Todo List")),
        BottomNavigationBarItem(icon: Icon(Icons.assignment), title: Text("Goals")),
        BottomNavigationBarItem(icon: Icon(Icons.settings), title: Text("Setting")),
        BottomNavigationBarItem(icon: new Icon(FontAwesomeIcons.rocket, size: 20), title: Text("Plus")),
        BottomNavigationBarItem(icon: Icon(Icons.add_circle_outline), title: Text("Add"))
      ],
      backgroundColor: Colors.white30,
      type: BottomNavigationBarType.shifting,
      showSelectedLabels: false,
      fixedColor: Colors.black,
      unselectedItemColor: Colors.grey,
      currentIndex: MiddleWare.shared.variables.currentIndex,
      onTap: (index){
        clickToChangeMenu(index);
      },
    );
    return Scaffold(
      backgroundColor: Colors.white,
      body: TabBarView(
        children: [
          MiddleWare.shared.variables.tab_todoList,
          MiddleWare.shared.variables.tab_goals,
          MiddleWare.shared.variables.tab_setting,
          MiddleWare.shared.variables.tab_plus,
          MiddleWare.shared.variables.tab_add
        ],
        controller: MiddleWare.shared.variables.tabc,
        physics: NeverScrollableScrollPhysics(),
      ),
      bottomNavigationBar: this.widget.navBar,
      key: scaffold,
    );
  }
}
