import 'package:flutter/material.dart';
import 'package:goal_meter/controllers/mainTabbar/anotherController/Add/Add.dart';
import 'package:goal_meter/controllers/mainTabbar/anotherController/Goals/Goals.dart';
import 'package:goal_meter/controllers/mainTabbar/anotherController/Plus/Plus.dart';
import 'package:goal_meter/controllers/mainTabbar/anotherController/Setting/Setting.dart';
import 'package:goal_meter/controllers/mainTabbar/anotherController/TodoList/TodoList.dart';

class MiddleWareVars
{
  int currentIndex = 0;
  TabController tabc;

  ToDoListVC tab_todoList = new ToDoListVC();
  GoalsVC tab_goals = new GoalsVC();
  SettingVC tab_setting = new SettingVC();
  PlusVC tab_plus = new PlusVC();
  AddVC tab_add = new AddVC();
}