import 'package:flutter/material.dart';
import 'package:goal_meter/controllers/mainTabbar/anotherController/Add/PickGoal.dart';
import 'package:goal_meter/controllers/mainTabbar/anotherController/Add/Task/AddTaskTabBarView.dart';
import 'package:goal_meter/models/Statics/Statics.dart';
import 'package:goal_meter/models/Statics/Strings.dart';
import 'package:goal_meter/views/GoalBoxWidget/BoxWidget.dart';
import 'package:goal_meter/models/Goal/GoalObject.dart';
import 'PickGoal.dart'; // add goal VC ; after picked a goal user would push there.

class AddVC extends StatefulWidget {

  AddVCState myChild;
  var mainContext;
  List<GoalObject> goals = Statics.shared.models.goals;

  void pickedGoal(GoalObject goal)
  {
    if(mainContext != null)
      {
        Navigator.push(this.myChild._scaffold.currentContext, new MaterialPageRoute(builder: (mainContext) => new PickGoal(goal)));
      }
  }

  @override
  AddVCState createState() => AddVCState();
}

class AddVCState extends State<AddVC> {

  GlobalKey _scaffold = GlobalKey();

  @override
  Widget build(BuildContext context) {
    this.widget.myChild = this;
    this.widget.mainContext = context;
    double boxSize = (MediaQuery.of(context).size.width - 21) / 2;
    List<Widget> myList = [];
    for(int i = 0; i < this.widget.goals.length; i+=2)
      {
        List<Widget> childs = [];
        if(i+1 < this.widget.goals.length)
          {
            childs = [BoxWidget.BoxWidgetWithoutGoalOBJ(boxSize,this.widget.goals[i],this.widget),
              SizedBox(width: 5),
              BoxWidget.BoxWidgetWithoutGoalOBJ(boxSize,this.widget.goals[i+1],this.widget)];
          }
        else
          {
            childs = [BoxWidget.BoxWidgetWithoutGoalOBJ(boxSize,this.widget.goals[i],this.widget),
              SizedBox(width: 5),
              BoxWidget.BoxWidgetWithoutGoalOBJ(boxSize,null,this.widget)
            ];
          }

        myList.add(Row(
          mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.center,
          children: childs,
        ));
        myList.add(SizedBox(height: 5));
      }

    return new Scaffold(
        appBar: AppBar(
          title: Text(Strings.shared.pickACategoryTopPage, style: TextStyle(color: Color.fromRGBO(53, 104, 89, 1), fontSize: 22)),
          backgroundColor: Colors.white,
          centerTitle: true,
          elevation: 0,
          iconTheme: IconThemeData(
              color: Color.fromRGBO(0, 0, 0, 1)
          ),
        ),
        backgroundColor: Colors.white,
        body: Builder(
          builder: (mainContext) =>
              Container(
                margin: const EdgeInsets.only(left: 8, right: 8, top: 16, bottom: 16),
                child: Stack(
                  children: [
                    ListView(
                      children: myList,
                    ),
                    Container(
                      child: FloatingActionButton(
                        child: Icon(Icons.library_add),
                        onPressed: (){

                          Navigator.push(
                            mainContext,
                            MaterialPageRoute(builder: (context) => new AddTaskTabBarView()),
                          );

                        },
                      ),
                      alignment: Alignment.bottomRight,
                    )
                  ],
                )
              ),
        ),
        key: _scaffold,
    );
  }
}

