import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:goal_meter/controllers/mainTabbar/anotherController/Add/MiddleWare.dart';
import 'package:goal_meter/models/Statics/Statics.dart';
import 'package:goal_meter/models/Statics/Strings.dart';
import 'package:goal_meter/models/Goal/GoalObject.dart';
import 'MiddleWare.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dashed_container/dashed_container.dart';

import 'package:progress_hud/progress_hud.dart';
import 'package:goal_meter/models/server/CatchDataFromServer.dart';

import 'package:goal_meter/views/GoalProgressWidget/GoalProgressWidget.dart';

class CreateGoal extends StatefulWidget {

  GoalObject myGoal;
  CreateGoalState instance;
  String selectedLevel = "distance";

  void firstCheckGoalStatus(int medal)
  {
    MiddleWare.shared.selectedModalNumber = medal;

    MiddleWare.shared.medal1 = "Resources/icons/CreateGoal/medal1-off.png";
    MiddleWare.shared.medal2 = "Resources/icons/CreateGoal/medal2-off.png";
    MiddleWare.shared.medal3 = "Resources/icons/CreateGoal/medal3-off.png";
    MiddleWare.shared.medal4 = "Resources/icons/CreateGoal/medal4-off.png";
    MiddleWare.shared.medal5 = "Resources/icons/CreateGoal/medal5-off.png";
    MiddleWare.shared.medal1Status = Container(child: Icon(FontAwesomeIcons.circle, color: Colors.grey));
    MiddleWare.shared.medal2Status = Container(child: Icon(FontAwesomeIcons.circle, color: Colors.grey));
    MiddleWare.shared.medal3Status = Container(child: Icon(FontAwesomeIcons.circle, color: Colors.grey));
    MiddleWare.shared.medal4Status = Container(child: Icon(FontAwesomeIcons.circle, color: Colors.grey));
    MiddleWare.shared.medal5Status = Container(child: Icon(FontAwesomeIcons.circle, color: Colors.grey));
    if(MiddleWare.shared.myGoalLevel == 1)
    {
      MiddleWare.shared.levelTitle = Strings.shared.progressLevelBeginner;
      MiddleWare.shared.selectedMedalText = Strings.shared.progressLevelBeginner;
      MiddleWare.shared.medal1 = "Resources/icons/CreateGoal/medal1-off.png";
      MiddleWare.shared.medal1Status = Container(child: Icon(FontAwesomeIcons.circle, color: Colors.green));
    }
    else if(MiddleWare.shared.myGoalLevel == 2)
    {
      MiddleWare.shared.levelTitle = Strings.shared.progressLevelIntermediate;
      MiddleWare.shared.selectedMedalText = Strings.shared.progressLevelIntermediate;
      MiddleWare.shared.medal1 = "Resources/icons/CreateGoal/medal1.png";
      MiddleWare.shared.medal2 = "Resources/icons/CreateGoal/medal2-off.png";
      MiddleWare.shared.medal1Status = Container(child: Icon(FontAwesomeIcons.checkCircle, color: Colors.green));
      MiddleWare.shared.medal2Status = Container(child: Icon(FontAwesomeIcons.circle, color: Colors.green));
    }
    else if(MiddleWare.shared.myGoalLevel == 3)
    {
      MiddleWare.shared.levelTitle = Strings.shared.progressLevelUpperIntermediate;
      MiddleWare.shared.selectedMedalText = Strings.shared.progressLevelUpperIntermediate;
      MiddleWare.shared.medal1 = "Resources/icons/CreateGoal/medal1.png";
      MiddleWare.shared.medal2 = "Resources/icons/CreateGoal/medal2.png";
      MiddleWare.shared.medal3 = "Resources/icons/CreateGoal/medal3-off.png";
      MiddleWare.shared.medal1Status = Container(child: Icon(FontAwesomeIcons.checkCircle, color: Colors.green));
      MiddleWare.shared.medal2Status = Container(child: Icon(FontAwesomeIcons.checkCircle, color: Colors.green));
      MiddleWare.shared.medal3Status = Container(child: Icon(FontAwesomeIcons.circle, color: Colors.green));
    }
    else if(MiddleWare.shared.myGoalLevel == 4)
    {
      MiddleWare.shared.levelTitle = Strings.shared.progressLevelAdvanced;
      MiddleWare.shared.selectedMedalText = Strings.shared.progressLevelAdvanced;
      MiddleWare.shared.medal1 = "Resources/icons/CreateGoal/medal1.png";
      MiddleWare.shared.medal2 = "Resources/icons/CreateGoal/medal2.png";
      MiddleWare.shared.medal3 = "Resources/icons/CreateGoal/medal3.png";
      MiddleWare.shared.medal4 = "Resources/icons/CreateGoal/medal4-off.png";
      MiddleWare.shared.medal1Status = Container(child: Icon(FontAwesomeIcons.checkCircle, color: Colors.green));
      MiddleWare.shared.medal2Status = Container(child: Icon(FontAwesomeIcons.checkCircle, color: Colors.green));
      MiddleWare.shared.medal3Status = Container(child: Icon(FontAwesomeIcons.checkCircle, color: Colors.green));
      MiddleWare.shared.medal4Status = Container(child: Icon(FontAwesomeIcons.circle, color: Colors.green));
    }
    else if(MiddleWare.shared.myGoalLevel == 5)
    {
      MiddleWare.shared.levelTitle = Strings.shared.progressLevelMaster;
      MiddleWare.shared.selectedMedalText = Strings.shared.progressLevelMaster;
      MiddleWare.shared.medal1 = "Resources/icons/CreateGoal/medal1.png";
      MiddleWare.shared.medal2 = "Resources/icons/CreateGoal/medal2.png";
      MiddleWare.shared.medal3 = "Resources/icons/CreateGoal/medal3.png";
      MiddleWare.shared.medal4 = "Resources/icons/CreateGoal/medal4.png";
      MiddleWare.shared.medal5 = "Resources/icons/CreateGoal/medal5-off.png";
      MiddleWare.shared.medal1Status = Container(child: Icon(FontAwesomeIcons.checkCircle, color: Colors.green));
      MiddleWare.shared.medal2Status = Container(child: Icon(FontAwesomeIcons.checkCircle, color: Colors.green));
      MiddleWare.shared.medal3Status = Container(child: Icon(FontAwesomeIcons.checkCircle, color: Colors.green));
      MiddleWare.shared.medal4Status = Container(child: Icon(FontAwesomeIcons.checkCircle, color: Colors.green));
      MiddleWare.shared.medal5Status = Container(child: Icon(FontAwesomeIcons.circle, color: Colors.green));
    }

    if(medal == 1)
    {
      MiddleWare.shared.selectedMedalText = Strings.shared.progressLevelBeginner;
    }
    else if(medal == 2)
    {
      MiddleWare.shared.selectedMedalText = Strings.shared.progressLevelIntermediate;
    }
    else if(medal == 3)
    {
      MiddleWare.shared.selectedMedalText = Strings.shared.progressLevelUpperIntermediate;
    }
    else if(medal == 4)
    {
      MiddleWare.shared.selectedMedalText = Strings.shared.progressLevelAdvanced;
    }
    else if(medal == 5)
    {
      MiddleWare.shared.selectedMedalText = Strings.shared.progressLevelMaster;
    }
    if(MiddleWare.shared.myGoalLevel > medal)
    {
      MiddleWare.shared.selectedMedal = "Resources/icons/CreateGoal/medal${medal}.png";
      MiddleWare.shared.startGoalBtn = MiddleWare.shared.startGoalBtnFinished;
      MiddleWare.shared.isStartable = false;
      MiddleWare.shared.startGoalBtnColor = Colors.green;
    }
    else
    {
      if(MiddleWare.shared.myGoalLevel == medal)
      {
        MiddleWare.shared.selectedMedal = "Resources/icons/CreateGoal/medal${medal}-off.png";
        MiddleWare.shared.startGoalBtn = MiddleWare.shared.startGoalBtnStart;
        MiddleWare.shared.isStartable = true;
        MiddleWare.shared.startGoalBtnColor = Colors.blue;
      }
      else
      {
        MiddleWare.shared.selectedMedal = "Resources/icons/CreateGoal/medal${medal}-off.png";
        MiddleWare.shared.startGoalBtn = MiddleWare.shared.startGoalBtnNotFinished;
        MiddleWare.shared.isStartable = false;
        MiddleWare.shared.startGoalBtnColor = Colors.grey;
      }
    }
  }

  CreateGoal(GoalObject goal){
    this.myGoal = goal;
    firstCheckGoalStatus(MiddleWare.shared.myGoalLevel);
  }

  @override
  CreateGoalState createState() => CreateGoalState();
}

class CreateGoalState extends State<CreateGoal> {

  ProgressHUD _progressHUD;
  bool _loading = false;

  @override
  void initState() {
    super.initState();
    MiddleWare.shared.listViewController = ScrollController();
    super.widget.instance = this;

    _progressHUD = new ProgressHUD(
      backgroundColor: Colors.black12,
      color: Colors.white,
      containerColor: Colors.blue,
      borderRadius: 5.0,
      text: 'Loading...',
      loading: this._loading,
    );
  }

  void showProgressHUD() {
    setState(() {
      _progressHUD.state.show();
      _loading = true;
    });
  }
  void dismissProgressHUD() {
    setState(() {
      _progressHUD.state.dismiss();
      _loading = false;
    });
  }

  @override
  Widget build(BuildContext context) {

    MiddleWare.shared.windowWidth = MediaQuery.of(context).size.width;
    MiddleWare.shared.windowHeight = MediaQuery.of(context).size.height;

    List<Widget> resMyListRadioButtons = [];
    for(var i=0;i<MiddleWare.shared.unitsRadio.length;i++)
    {
      resMyListRadioButtons.add(Container(height: 40,child: RadioListTile(
        title: Text(MiddleWare.shared.unitsRadio[i].title, style: TextStyle(color: Colors.black)),
        groupValue: MiddleWare.shared.unitRadioValue,
        value: MiddleWare.shared.unitsRadio[i].id,
        selected: true,
        activeColor: Colors.green,
        onChanged: (index){
          this.handleRadioListChange(index);
        },
      ),));
    }

    double firstRowHeight = 265;
    double secondRowHeight = MediaQuery.of(context).size.height - 0;

    return new Scaffold(
        backgroundColor: Colors.white,
        body: Builder(
          builder: (mainContext) =>
              Container(
                  color: Colors.white,
                  child: Stack(
                    children: [
                      Container(color: Colors.white,
                          child:Column(
                            children: [
                              Container(color: Colors.white,height: secondRowHeight,
                                child: new NotificationListener(
                                  child: ListView(
                                    padding: const EdgeInsets.only(top: 0),
                                    controller: MiddleWare.shared.listViewController,
                                    children: [
                                      firstRow(mainContext,firstRowHeight),
                                      SizedBox(height: 10),
                                      secondRow(mainContext,resMyListRadioButtons),
                                      SizedBox(height: 30),
                                      thirdRow(mainContext),
                                      SizedBox(height: 30),
                                      fourthRow(mainContext),
                                      SizedBox(height: 100),
                                    ],
                                  ),
                                  onNotification: (t){
                                    if (t is ScrollEndNotification) {
                                      if(MiddleWare.shared.listViewController.position.pixels >= 180){
                                        // change topBar background color to opacity black
                                        setState(() {
                                          MiddleWare.shared.topBarBackgroundColor = Color.fromRGBO(0, 0, 0, 0.7);
                                        });
                                      }else{
                                        setState(() {
                                          MiddleWare.shared.topBarBackgroundColor = Colors.transparent;
                                        });
                                      }
                                    }
                                  },
                                ), // notification Listener
                              ),
                            ],
                          )// column
                      ),
                      topBarContainer(mainContext),
                      _progressHUD,
                    ],
                  ),
              ),
        )
    );
  }

  void handleRadioListChange(index)
  {
    setState(()
    {
      for(var i=0;i<MiddleWare.shared.unitsRadio.length;i++)
      {
        if(MiddleWare.shared.unitsRadio[i].id == index)
        {
          MiddleWare.shared.goalUnitStr = MiddleWare.shared.unitsRadio[i].unit;
        }
      }
      MiddleWare.shared.unitRadioValue = index;
      if(index == 0){
        this.widget.selectedLevel = "distance";
      }else
        {
          this.widget.selectedLevel = "time";
        }
    });
  }
  void tapMedal(int medal)
  {
    setState(() {
      this.widget.firstCheckGoalStatus(medal);
    });
  }

  Container firstRow(mainContext,height)
  {
    String totalProgress = "";
    if(this.widget.selectedLevel == "distance"){
      totalProgress = "${MiddleWare.shared.progressValue} ${MiddleWare.shared.levelValueTypeDistance}";
    }else{
      totalProgress = "${MiddleWare.shared.progressValue} ${MiddleWare.shared.levelValueTypeTime}";
    }

    String currentProgress = "";
    if(this.widget.selectedLevel == "distance"){
      currentProgress = "${MiddleWare.shared.currentProgressValue} ${MiddleWare.shared.levelValueTypeDistance} (0%)";
    }else{
      currentProgress = "${MiddleWare.shared.currentProgressValue} ${MiddleWare.shared.levelValueTypeTime} (0%)";
    }

    double progressWidth = MediaQuery.of(mainContext).size.width;

    return Container(
      height: height,
      decoration: new BoxDecoration(
          image: new DecorationImage(
              image: new AssetImage(this.widget.myGoal.goalImg),
              fit: BoxFit.cover)),
      child: Stack(
        children: [
          Container(color: Color.fromRGBO(0, 0, 0, 0.5), height: height),
          Column(
            children: [
              Container(
                  height: 35,
                  margin: const EdgeInsets.only(top: 110),
                  padding: const EdgeInsets.only(left: 32,right: 32),
                  child: Row(
                    children: [
                      Container(width: (MiddleWare.shared.windowWidth - 64) / 2, child: Row(
                        children: [
                          Container(child: Image.asset("Resources/icons/CreateGoal/charts.png")),
                          Container(child: SizedBox(width: 5)),
                          Container(child: Text(MiddleWare.shared.progressTitle,style: TextStyle(color: Colors.white,fontSize: 17),
                              textAlign: TextAlign.right),alignment: Alignment.center)
                        ],
                      ),),
                      Container(
                          child: Text(totalProgress,
                              style: TextStyle(color: Colors.white,fontSize: 17),
                              textAlign: TextAlign.right),
                          width: (MiddleWare.shared.windowWidth - 64) / 2, alignment: Alignment.centerRight)
                    ],
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                  )),
              Container(
                  height: 35,
                  padding: const EdgeInsets.only(left: 32,right: 32),
                  child: Row(
                    children: [
                      Container(width: (MiddleWare.shared.windowWidth - 64) / 2, child: Row(
                        children: [
                          Container(child: Image.asset("Resources/icons/CreateGoal/miniMedal.png")),
                          Container(child: SizedBox(width: 5)),
                          Container(child: Text(MiddleWare.shared.levelTitle,style: TextStyle(color: Colors.white,fontSize: 17),
                              textAlign: TextAlign.right),alignment: Alignment.center)
                        ],
                      ),),
                      Container(
                          child: Text(currentProgress,
                              style: TextStyle(color: Colors.white,fontSize: 17),
                              textAlign: TextAlign.right),
                          width: (MiddleWare.shared.windowWidth - 64) / 2, alignment: Alignment.centerRight)
                    ],
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                  )),
              Container(child: GoalProgressWidget(progressValuePercent: 0,height: 35, width: progressWidth,progressTotal: 100,borderColor: Colors.white,),
                padding: const EdgeInsets.only(top: 20, right: 32, left: 32),
              ),
            ], // children
          ),
          //topBarContainer(mainContext),
        ],
      ),
      //padding: const EdgeInsets.only(bottom: 15),
    );
  }
  Container secondRow(mainContext,List<Widget> myList)
  {
    return Container(
        child: Column(
          children: [
            Container(child: Text(Strings.shared.secondRowFirstTitle, style: TextStyle(color: Colors.grey, fontSize: 15), textAlign: TextAlign.left), width: MiddleWare.shared.windowWidth - 64),
            SizedBox(height: 10),
            Container(width: MiddleWare.shared.windowWidth - 16,decoration: BoxDecoration(border: Border.all(width: 1, color: Colors.grey)),
              child: Container(
                child: Column(
                  children: myList,
                ),
                padding: const EdgeInsets.only(bottom: 15),
              )
            )
          ],
        ),
    );
  }
  Container thirdRow(mainContext)
  {
    return Container(
      child: Column(
        children: [
          Container(
            child: Stack(
              children: [
                Container(child: DashedContainer(
                  strokeWidth: 1.5,
                  blankLength: 8.0,
                  dashColor: Colors.grey,
                  dashedLength: 3.0,
                  child: Container(width: MiddleWare.shared.windowWidth,height: 1.0),
                ),padding: const EdgeInsets.only(top: 8),),
                Container(
                  child: Text("  ${Strings.shared.secondRowFirstChallengesTitle}  ", style: TextStyle(backgroundColor: Colors.white),),
                  color: Colors.transparent,
                  alignment: Alignment.center,
                ),
              ],
            ),
          ),
          SizedBox(height: 20),
          Row(
            children: [
              Container(child: IconButton(
                icon: Image.asset(MiddleWare.shared.medal1, width: 45, alignment: Alignment.center),
                padding: const EdgeInsets.all(0),
                onPressed: (){tapMedal(1);},
              )),
              Container(child: IconButton(
                icon: Image.asset(MiddleWare.shared.medal2, width: 45, alignment: Alignment.center),
                padding: const EdgeInsets.all(0),
                onPressed: (){tapMedal(2);},
              )),
              Container(child: IconButton(
                icon: Image.asset(MiddleWare.shared.medal3, width: 45, alignment: Alignment.center),
                padding: const EdgeInsets.all(0),
                onPressed: (){tapMedal(3);},
              )),
              Container(child: IconButton(
                icon: Image.asset(MiddleWare.shared.medal4, width: 45, alignment: Alignment.center),
                padding: const EdgeInsets.all(0),
                onPressed: (){tapMedal(4);},
              )),
              Container(child: IconButton(
                icon: Image.asset(MiddleWare.shared.medal5, width: 45, alignment: Alignment.center),
                padding: const EdgeInsets.all(0),
                onPressed: (){tapMedal(5);},
              )),
            ],
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
          ),
          SizedBox(height: 20),
          Row(
            children: [
              MiddleWare.shared.medal1Status,
              MiddleWare.shared.medal2Status,
              MiddleWare.shared.medal3Status,
              MiddleWare.shared.medal4Status,
              MiddleWare.shared.medal5Status
            ],
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
          )
        ],
      ),
    );
  }
  Container fourthRow(mainContext)
  {
    return Container(
      height: 150,
      margin: const EdgeInsets.only(left: 8,right: 8, bottom: 100),
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(
        border: Border.all(color: Colors.grey,width: 1)
      ),
      child: Column(
        children: [
          Row(
            children: [
              Image.asset(MiddleWare.shared.selectedMedal, width: 50),
              SizedBox(width: 10),
              Column(
                children: [
                  Text(MiddleWare.shared.selectedMedalText, style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.black), textAlign: TextAlign.left),
                  Text(MiddleWare.shared.selectedMedalSubText, style: TextStyle(fontSize: 16, fontWeight: FontWeight.normal, color: Colors.black),textAlign: TextAlign.left),
                ],
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
              )
            ],
          ),//end Row
          Expanded(child: Container()),
          FlatButton(
            onPressed: (){
              if(MiddleWare.shared.isStartable)
              {
                this.showProgressHUD();
                var parentTitle="";
                for(var i = 0;i<Statics.shared.models.goals.length;i++)
                {
                  if(Statics.shared.models.goals[i].id == this.widget.myGoal.parentId)
                  {
                    parentTitle = Statics.shared.models.goals[i].goalTitle;
                  }
                }

                if(MiddleWare.shared.goalUnitStr == "")
                {
                  MiddleWare.shared.goalUnitStr = MiddleWare.shared.unitsRadio.first.unit;
                }
                Firestore.instance.collection('userGoals').document()
                    .setData(
                    {
                      'userid': Statics.shared.user.uid,
                      'goalName':this.widget.myGoal.goalTitle,
                      'goalParent':parentTitle,
                      'goalUnit':MiddleWare.shared.goalUnitStr,
                      'createDate':new DateTime.now(),
                      'isRemoved':false,
                      'isArchive':false,
                    }
                ).then((val){
                  var msg = Statics.shared.snackBarMsg(Strings.shared.goalCreatedSuccessfullySnackMsg);
                  Scaffold.of(mainContext).showSnackBar(msg);
                  // refresh GoalsVC
                  final CatchDataFromFireStore _server = CatchDataFromFireStore();
                  _server.refreshAllGoals(onComplete: (){
                    this.dismissProgressHUD();
                    Future.delayed(Duration(seconds: 15)).whenComplete((){
                      // force dismiss progress
                      print("Force Dismiss Progress...");
                      this.dismissProgressHUD();
                    });
                  });
                });
              }
            },
            child: Container(width: (MiddleWare.shared.windowWidth),
                child: Stack(
                  children: [
                    Container(child: Image.asset("Resources/icons/playBtn.png",height: 30, width: 30,fit: BoxFit.fill,),alignment: Alignment.centerLeft, padding: const EdgeInsets.only(top: 5),),
                    Container(child: SizedBox(width: 2, height: 40, child: Container(color: Colors.white,),),padding: const EdgeInsets.only(left: 45),),
                    Container(child: MiddleWare.shared.startGoalBtn,alignment: Alignment.center, padding: const EdgeInsets.only(top: 8),)
                  ],
                ), // stack
                alignment: Alignment.center),
            color: MiddleWare.shared.startGoalBtnColor,
            disabledColor: Colors.grey,
          )
        ],
      ),//end Column
    );
  }


  Container topBarContainer(mainContext)
  {
    return Container(
      height: 90,
      margin: const EdgeInsets.all(0),
      child: Container(height: 250, color: MiddleWare.shared.topBarBackgroundColor,
        child: Column(
          children: [
            Container(height: 60, margin: const EdgeInsets.only(top: 30),
            color: Colors.transparent,
            child: Row(
              children: [
                Container(child: FlatButton(
                  onPressed: (){Navigator.pop(mainContext);},
                  child: Icon(Icons.arrow_back_ios, color: Colors.white),
                  padding: const EdgeInsets.all(0),
                ),width: 50,),
                Center(child: Text(this.widget.myGoal.goalTitle, style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white),textAlign: TextAlign.center)),
                Container(child: FlatButton(
                  onPressed: (){},
                  child: Icon(Icons.more_vert, color: Colors.white),
                  padding: const EdgeInsets.all(0),
                ),width: 50,),
              ],
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
            ),
          ),
          ],
        ),
      ),
    );
  }

}
