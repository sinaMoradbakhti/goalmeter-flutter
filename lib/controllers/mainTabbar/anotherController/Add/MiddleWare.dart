import 'package:flutter/material.dart';
import 'package:goal_meter/models/Statics/Strings.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:goal_meter/models/Goal/GoalUnitsObject.dart';

class MiddleWare
{
  static MiddleWare shared = MiddleWare();
  _MiddleWare(){}


  // vars that can be edited by user info
  List<RadioListTile> unitRadioList = [];
  int myGoalLevel = 2;
  List<GoalUnitsObject>unitsRadio =
  [
    GoalUnitsObject(0,Strings.shared.secondRowFirstTitleDistanceRadioButtonValue,"distance"),
    GoalUnitsObject(1,Strings.shared.secondRowFirstTitleTimeRadioButtonValue,"time"),
  ];
  // vars that can be edited by user info

  int unitRadioValue = 0;
  String progressTitle = Strings.shared.totalProgress;

  String progressValue = "45";
  String currentProgressValue = "0";

  ScrollController listViewController;
  String levelTitle = Strings.shared.progressLevelIntermediate;
  String levelValue = "0 hr (0%)";
  String levelValueTypeTime = "hr";
  String levelValueTypeDistance = "km";
  double windowWidth = 0;
  double windowHeight = 0;

  Color topBarBackgroundColor = Colors.transparent;

  String medal1 = "Resources/icons/CreateGoal/medal1.png";
  String medal2 = "Resources/icons/CreateGoal/medal2-off.png";
  String medal3 = "Resources/icons/CreateGoal/medal3-off.png";
  String medal4 = "Resources/icons/CreateGoal/medal4-off.png";
  String medal5 = "Resources/icons/CreateGoal/medal5-off.png";

  Container medal1Status = Container(child: Icon(FontAwesomeIcons.checkCircle, color: Colors.green));
  Container medal2Status = Container(child: Icon(FontAwesomeIcons.circle, color: Colors.green));
  Container medal3Status = Container(child: Icon(FontAwesomeIcons.circle, color: Colors.grey));
  Container medal4Status = Container(child: Icon(FontAwesomeIcons.circle, color: Colors.grey));
  Container medal5Status = Container(child: Icon(FontAwesomeIcons.circle, color: Colors.grey));

  String goalUnitStr = "";
  bool isStartable = false;
  String selectedMedal = "Resources/icons/CreateGoal/medal1.png";
  int selectedModalNumber = 1;
  String selectedMedalText = Strings.shared.progressLevelIntermediate;
  String selectedMedalSubText = "Run for a total of 80 kilometers";


  Color startGoalBtnColor = Colors.blue;
  Text startGoalBtn;
  Text startGoalBtnStart = Text(
      Strings.shared.startCapsLockOn,
      style: TextStyle(
          color: Colors.white,
          fontSize: 20
      )
  );
  Text startGoalBtnFinished = Text(
      Strings.shared.forthRowCreateGoalBtnStartFinished,
      style: TextStyle(
          color: Colors.white,
          fontSize: 20
      )
  );
  Text startGoalBtnNotFinished = Text(
      Strings.shared.forthRowCreateGoalBtnStartNotFinished,
      style: TextStyle(
          color: Colors.white,
          fontSize: 20
      )
  );

}