import 'package:flutter/material.dart';
import 'package:goal_meter/models/Statics/Statics.dart';
import 'package:goal_meter/models/Statics/Strings.dart';
import 'package:goal_meter/models/Goal/GoalObject.dart';
import 'package:goal_meter/views/GoalBoxWidget/BoxWidget.dart';
import 'CreateGoal.dart'; // create Goal VC ; after picked a sub goal user would push there.

class PickGoal extends StatefulWidget {

  var mainContext;
  GoalObject myGoal;
  List<GoalObject> myList = [];

  PickGoal(GoalObject goal){
    this.myGoal = goal;
    this.myList = Statics.shared.models.getGoalChildren(goal.id);
  }

  void pickedGoal(GoalObject goal)
  {
    if(mainContext != null)
    {
      Navigator.push(mainContext, new MaterialPageRoute(builder: (mainContext) => new CreateGoal(goal)));
    }
  }

  @override
  _PickGoalState createState() => _PickGoalState();
}

class _PickGoalState extends State<PickGoal> {
  @override
  Widget build(BuildContext context) {
    this.widget.mainContext = context;
    double boxSize = (MediaQuery.of(context).size.width - 21);
    List<Widget> myList = [];
    for(int i = 0; i < this.widget.myList.length; i++)
    {
      myList.add(BoxWidgetSubGoal(boxSize,this.widget.myList[i],this.widget));
      myList.add(SizedBox(height: 10));
    }

    return new Scaffold(
        appBar: AppBar(
          title: Text(Strings.shared.pickAGoalTopPage, style: TextStyle(color: Color.fromRGBO(53, 104, 89, 1), fontSize: 22)),
          backgroundColor: Colors.white,
          centerTitle: true,
          elevation: 0,
          iconTheme: IconThemeData(
              color: Color.fromRGBO(0, 0, 0, 1)
          ),
        ),
        backgroundColor: Colors.white,
        body: Builder(
          builder: (mainContext) =>
              Container(
                  margin: const EdgeInsets.only(left: 8, right: 8, top: 16, bottom: 16),
                  child: ListView(
                    children: myList,
                  )
              ),
        )
    );
  }
}
