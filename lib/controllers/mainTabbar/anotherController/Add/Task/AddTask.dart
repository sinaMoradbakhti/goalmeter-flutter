import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:goal_meter/models/Goal/MyGoalObject.dart';
import 'package:goal_meter/models/Statics/Strings.dart';
import 'package:goal_meter/models/Statics/Statics.dart';
import 'TaskProcess.dart';
import 'MiddleWareStepOne.dart';
import 'package:goal_meter/views/dialogs/PopUpDialogGoalPicker.dart';

class AddTask extends StatefulWidget {
  PopUpDialogGoalPicker popUpSelectGoal;
  AddTaskState child;

  Widget goalHint;
  bool isFirstLunched = true;
  final MiddleWareStepOne md = MiddleWareStepOne();

  @override
  AddTaskState createState(){
    return AddTaskState();
  }
}

class AddTaskState extends State<AddTask> {

  final GlobalKey _scaffold = GlobalKey();

  void refreshHint(String gid)
  {
    for(int i = 0; i < Statics.shared.data.myGoals.length; i++){
      MyGoalObject myGoal = Statics.shared.data.myGoals[i];
      if(myGoal.gId == gid){
        setState(() {
          this.widget.goalHint = Container(
            child: Row(
              children: [
                Container(
                  width: 50,
                  height: 50,
                  decoration: BoxDecoration(
                      image: DecorationImage(
                        image: new ExactAssetImage(myGoal.goal.goalImg),
                        fit: BoxFit.cover,
                      )
                  ),
                  child: Container(),
                  margin: const EdgeInsets.only(right: 8),
                  padding: const EdgeInsets.all(0),
                ),
                Text(myGoal.gName, style: TextStyle(fontSize: 17))
              ],
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
            ),
          );
          this.widget.md.selectedGoalId = myGoal.gId;
        });
      }else{
        if(gid == ""){
          setState(() {
            this.widget.goalHint = Container(
              child: Text(Strings.shared.noGoalCamelCapWithFirst, style: TextStyle(fontSize: 17)),
              margin: const EdgeInsets.only(left: 16),
            );
            this.widget.md.selectedGoalId = "noGoal";
          });
        }
      }
    }// for loop
  }
  void nextTabFunc(isOk(bool status)){

    String msgContent = "";
    if(this.widget.md.txtTaskNameCtrl.text.isEmpty){
      msgContent = msgContent + Strings.shared.snackBarBottomSheetAddTaskTaskName;

      var msg = Statics.shared.snackBarMsgOnBottomSheet(Strings.shared.snackBarBottomSheetAddTaskPleaseEnter + " " + msgContent);
      Scaffold.of(_scaffold.currentContext).showSnackBar(msg);
    }else{
      TaskProcess.shared.taskName = this.widget.md.txtTaskNameCtrl.text;
      TaskProcess.shared.goalId = this.widget.md.selectedGoalId;
      isOk(true);
    }
  }

  @override
  void dispose() {
    print("DISSSSPOOOOSSS");
    super.dispose();
  }

  @override
  Widget build(BuildContext context){

    TaskProcess.shared.currentSlide = 0;
    this.widget.child = this;
    double popUpWidth = (MediaQuery.of(context).size.width) - 64;
    double height = MediaQuery.of(context).size.height / 1.3;
    if(MediaQuery.of(context).size.height < 750){
      height = MediaQuery.of(context).size.height - 10;
    }
    double screenWidth = MediaQuery.of(context).size.width;

    if(this.widget.goalHint == null){
      this.widget.goalHint = Container(
        child: Text(Strings.shared.noGoalCamelCapWithFirst, style: TextStyle(fontSize: 17)),
        margin: const EdgeInsets.only(left: 16),
      );
    }

    if(this.widget.isFirstLunched){
      this.widget.isFirstLunched = false;
    }

    this.widget.popUpSelectGoal = PopUpDialogGoalPicker(popUpWidth: popUpWidth,
        onPressOk: (){
          Navigator.pop(_scaffold.currentContext);
        },
        popUpHeight: height,
        selectedGoal: this.widget.md.selectedGoalId
    );

    return new Scaffold(
        backgroundColor: Colors.white,
        body: Builder(
          builder: (mainContext) =>
              Container(
                  margin: const EdgeInsets.only(left: 0, right: 0, top: 16, bottom: 16),
                  child: Stack(
                    children: [
                      Container(
                        child: Stack(
                          children: [
                            Container(
                              child: Text(" ${Strings.shared.stepOneContainerTitleAddNewTask} ", style: TextStyle(color: Statics.shared.colors.addTaskSectionsTitleColor, fontWeight: FontWeight.normal, fontSize: Statics.shared.fontSizes.addTaskSectionsTitle, backgroundColor: Colors.white)),
                              transform: Matrix4.translationValues(0.0, -10.0, 0.0),
                            )
                            ,
                            rowTaskName()// row TaskName
                            ,
                            rowSelectGoal()// row SelectAGoal
                          ],
                        ),
                        height: 180,
                        width: screenWidth,
                        padding: const EdgeInsets.only(left: 8),
                        margin: const EdgeInsets.only(left: 8, right: 8),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          border: Border.all(color: Colors.grey,width: 1)
                        ),
                      ),
                    ],
                  )
              ),
        ),
        key: _scaffold,
    );
  }

  Container rowTaskName(){
    double screenWidth = MediaQuery.of(context).size.width;
    return Container(
      child: Row(
        children: [
          Container(child: Image.asset("Resources/icons/TaskNameIcon.png", scale: 1.4), width: 50,height: 50,),
          Container(child: TextFormField(
            style: TextStyle(fontSize: 18, color: Colors.blue),
            controller: this.widget.md.txtTaskNameCtrl,
            decoration: InputDecoration(labelText: Strings.shared.inputLableEnterTaskName,border: OutlineInputBorder()),
            autofocus: false,
          ), width: screenWidth-50-16-16-16-8,height: 50,),
        ],
      ),
      margin: const EdgeInsets.only(top: 35),
    );// row TaskName;
  }
  Container rowSelectGoal(){
    double screenWidth = MediaQuery.of(context).size.width;

    return Container(
      child: Row(
        children: [
          Container(child: Image.asset("Resources/icons/goalIcon.png", scale: 1.4), width: 50,height: 50,),
          Container(
            child: OutlineButton(
              onPressed: (){
                this.showPopUpPickGoalMonth();
              },
              child: Row(
                children: [
                  this.widget.goalHint,
                  Icon(Icons.arrow_drop_down, color: Colors.red,size: 30,),
                ],
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
              ),
              disabledBorderColor: Colors.grey,
              padding: const EdgeInsets.only(left: 0),
            )// OutlineButton
            ,
            width: screenWidth-50-16-16-16-8,height: 50,),
        ],
      ),
      margin: const EdgeInsets.only(top: 105),
    );
  }

  showPopUpPickGoalMonth(){
    showDialog(context: context,builder: (BuildContext context) {
      return this.widget.popUpSelectGoal.dialog(
          selectedGoalId: this.widget.md.selectedGoalId,
          onGetGoalId: (goal){
            if(goal != null){
              this.refreshHint(goal.gId);
              print("GID:${goal.gId}");
            }else{
              this.refreshHint("");
            }
          }
      );
    });
  }
}
