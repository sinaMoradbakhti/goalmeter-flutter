import 'package:flutter/material.dart';
import 'package:goal_meter/main.dart';
import 'package:goal_meter/models/Statics/Strings.dart';
import 'package:goal_meter/models/Statics/Statics.dart';
import 'MiddleWareStepFour.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'TaskProcess.dart';
import 'package:goal_meter/models/server/CatchDataFromServer.dart';
import 'package:progress_hud/progress_hud.dart';

class AddTaskStepFour extends StatefulWidget {

  final MiddleWareStepFour md = MiddleWareStepFour();

  AddTaskStepFourState child;
  var bodyContext;

  @override
  AddTaskStepFourState createState(){
    this.md.txtNotificationMinutes = TextEditingController();
    this.md.txtNotificationMinutes.text = "15";
    return AddTaskStepFourState();
  }
}

class AddTaskStepFourState extends State<AddTaskStepFour> with AutomaticKeepAliveClientMixin<AddTaskStepFour> {

  ProgressHUD _progressHUD;
  bool _loading = false;
  void showProgressHUD() {
    setState(() {
      _progressHUD.state.show();
      _loading = true;
    });
  }
  void dismissProgressHUD() {
    setState(() {
      _progressHUD.state.dismiss();
      _loading = false;
    });
  }


  @override
  bool get wantKeepAlive => true;

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  var mainContext;

  void nextTabFunc(isOk(bool status)){
    TaskProcess.shared.recieveNotification = this.widget.md.recieveNotificationOn;
    if(this.widget.md.txtNotificationMinutes.text.isNotEmpty){
      TaskProcess.shared.minutesToRecieveNotification = int.parse(this.widget.md.txtNotificationMinutes.text);
    }
    this.createTask();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void initState() {
    // TODO: implement initState
    _progressHUD = new ProgressHUD(
      backgroundColor: Colors.black12,
      color: Colors.white,
      containerColor: Colors.blue,
      borderRadius: 5.0,
      text: 'Loading...',
      loading: this._loading,
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    TaskProcess.shared.currentSlide = 3;
    this.widget.child = this;
    this.mainContext = context;
    double screenWidth = MediaQuery.of(context).size.width;
    if(this.widget.md.screenWidth == 0){
      this.widget.md.screenWidth = screenWidth;

      setState(() {
        this.widget.md.recieveNotificationOn = true;
        this.widget.md.widgetSwitchedOnNotification = rowTaskChooseNotification(context);
      });
    }

    return new Scaffold(
      backgroundColor: Colors.white,
      body: Builder(
        builder: (mainContext){
          this.mainContext = mainContext;
          return Container(
              child: Builder(
                builder: (thisContext){
                  this.widget.bodyContext = thisContext;
                  return Stack(
                    children: [
                      ListView(
                        children: [
                          Stack(
                            children: [
                              Container(
                                child: Stack(
                                  children: [
                                    Container(
                                      child: Text(" ${Strings.shared.addTaskStep4NotificationLabel} ", style: TextStyle(color: Colors.grey, fontWeight: FontWeight.normal, fontSize: 18, backgroundColor: Colors.white)),
                                      transform: Matrix4.translationValues(0.0, -10.0, 0.0),
                                    )
                                    ,
                                    rowTaskNotificationLabel(mainContext)
                                    ,
                                    this.widget.md.widgetSwitchedOnNotification
                                  ],
                                ),
                                width: screenWidth,
                                padding: const EdgeInsets.only(left: 8, bottom: 10),
                                margin: const EdgeInsets.only(left: 0, right: 0),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.all(Radius.circular(10)),
                                    border: Border.all(color: Colors.grey,width: 1)
                                ),
                              ),
                            ],
                          ) // row 1
                          ,
                          SizedBox(height: 20)
                        ],
                        padding: const EdgeInsets.only(top: 10),
                      ),// Main List View
                      Container(child: _progressHUD),
                    ],
                  );
                },
              )
            ,
            margin: const EdgeInsets.only(left: 8, right: 8, top: 16, bottom: 76),
          );
        }
        ,
        key: _scaffoldKey,
      ),
    );
  }

  Container rowTaskNotificationLabel(context){
    double screenWidth = MediaQuery.of(context).size.width;
    return Container(
      child: Row(
        children: [
          Container(child: Image.asset("Resources/icons/goldBellIcon.png", scale: 1.5), width: 50,height: 50,),
          Container(child: Text(Strings.shared.addTaskStep4NotificationTitle, style: TextStyle(fontSize: 17)),
              width: screenWidth - 50 - 32 - 16 - 60),
          Container(child: Switch(
            value: this.widget.md.recieveNotificationOn,
            onChanged: (changed){setState(() {
              this.widget.md.recieveNotificationOn = changed;
              if(changed == true){
                this.widget.md.widgetSwitchedOnNotification = rowTaskChooseNotification(context);
              }else{
                this.widget.md.widgetSwitchedOnNotification = Container();
              }
            });},
          ),height: 50, width: 60,),
        ],
      ),
      margin: const EdgeInsets.only(top: 15),
      padding: const EdgeInsets.only(bottom: 10),
    );// row TaskName;
  }
  Container rowTaskChooseNotification(context){
    double screenWidth = MediaQuery.of(context).size.width;
    return Container(
      child: Column(
        children: [
          Row(
            children: [
              Container(
                  width: 80,
                  child: TextField(decoration: InputDecoration(hintText: "15"), textAlign: TextAlign.center, controller: this.widget.md.txtNotificationMinutes)
              ),
              SizedBox(width: 5),
              Container(
                child: Text(Strings.shared.addTaskStep4NotificationDescription, style: TextStyle(color: Colors.grey, fontSize: 16)),
              )
            ],
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
          ),
        ],
      ),
      margin: const EdgeInsets.only(top: 65),
      padding: const EdgeInsets.only(left: 8, right: 0 , bottom: 8, top: 8),
    );
  }

  void createTask(){
    var msgContent = "";
    TaskProcess.shared.info();
    bool beCreate = true;
    var tasksInfo = TaskProcess.shared;

    if(tasksInfo.taskName.isEmpty){
      beCreate = false;
      msgContent += "${Strings.shared.snackBarBottomSheetAddTaskTaskName}, \n";
    }

    if(tasksInfo.taskType != "oneTime"){

      if(tasksInfo.repeatedType == "daily"){
        if(tasksInfo.isSpecificWeekly){
          if(tasksInfo.specificDays.isEmpty){
            beCreate = false;
            msgContent += "${Strings.shared.snackBarBottomSheetAddTaskDailySpecificDays}, \n";
          }
        }
      }

      if(tasksInfo.repeatedType == "weekly"){
        if(tasksInfo.isSpecificWeekly){
          if(tasksInfo.specificDays.isEmpty){
            beCreate = false;
            msgContent += "${Strings.shared.snackBarBottomSheetAddTaskDailySpecificDays}, \n";
          }
        }
      }

      if(tasksInfo.repeatedType == "monthly"){
        if(tasksInfo.isSpecificMonthly){
          if(tasksInfo.specificDays.isEmpty){
            beCreate = false;
            msgContent += "${Strings.shared.snackBarBottomSheetAddTaskDailySpecificDays}, \n";
          }
        }
      }
    }// repeated

    if(tasksInfo.isTimeOn){
      if(tasksInfo.startTime == tasksInfo.endTime){
        beCreate = false;
        msgContent += "${Strings.shared.snackBarBottomSheetAddTaskDiffrentTimes}, \n";
      }
      else{
        if(tasksInfo.startTime.hour > tasksInfo.endTime.hour){
          beCreate = false;
          msgContent += "${Strings.shared.snackBarBottomSheetAddTaskDiffrentTimes}, \n";
        }else{
          if(tasksInfo.startTime.hour == tasksInfo.endTime.hour){
            if(tasksInfo.startTime.minute > tasksInfo.endTime.minute){
              beCreate = false;
              msgContent += "${Strings.shared.snackBarBottomSheetAddTaskDiffrentTimes}, \n";
            }
          }
        }
      }
    }// is Time on

    if(this.widget.md.recieveNotificationOn){
      TaskProcess.shared.recieveNotification = true;
      if(this.widget.md.txtNotificationMinutes.text.isEmpty){
        beCreate = false;
        msgContent += "${Strings.shared.snackBarBottomSheetAddTaskReceiveNotifiTime}, \n";
      }
      else{
        TaskProcess.shared.minutesToRecieveNotification = int.parse(this.widget.md.txtNotificationMinutes.text);
      }
    }// receive notification

    if(beCreate){
      _createTaskSafely();
    }else{
      var msg = Statics.shared.snackBarMsgOnBottomSheet(Strings.shared.snackBarBottomSheetAddTaskPleaseEnter + " " + msgContent);
      Scaffold.of(this.mainContext).showSnackBar(msg);
    }
  }// create task
  void _createTaskSafely(){
    showProgressHUD();
    TaskProcess.shared.info();
    var tasksInfo = TaskProcess.shared;
    Firestore.instance.collection('userTasks').document()
        .setData(
        {
          'userid': Statics.shared.user.uid,
          'goalId':tasksInfo.goalId,
          'taskName':tasksInfo.taskName,
          'taskType':tasksInfo.taskType,
          'repeatedType':tasksInfo.repeatedType,
          'specificDays':tasksInfo.specificDays,
          'hoursPerWeek':tasksInfo.hoursPerWeek,
          'hoursPerMonth':tasksInfo.hoursPerMonth,
          'startDate':tasksInfo.startDate,
          'endDate':tasksInfo.endDate,
          'isSpecificTime':tasksInfo.isTimeOn,
          'startTime':"${tasksInfo.startTime.hour}:${tasksInfo.startTime.minute}",
          'endTime':"${tasksInfo.endTime.hour}:${tasksInfo.endTime.minute}",
          'isEndTimeNextDay':tasksInfo.isEndOnNexDay,
          'recieveNotification':tasksInfo.recieveNotification,
          'minutesToRecieveNotification':tasksInfo.minutesToRecieveNotification,
          'createDate':DateTime.now(),
          'createTime':"${TimeOfDay.now().hour}:${TimeOfDay.now().minute}",
          'isRemoved':false,
          'isArchive':false,
          'value':''
        }
    ).then((val){
      // Todo List Should be Updated Here ...
      var msg = Statics.shared.snackBarMsgOnBottomSheet(Strings.shared.taskCreatedSuccessfullySnackMsg);
      Scaffold.of(this.mainContext).showSnackBar(msg);
      final CatchDataFromFireStore _server =  CatchDataFromFireStore();
      _server.refreshAllGoals(onComplete: (){

        _server.getAllTasks(onComplete: (){
          TaskProcess.shared.delete();
          Navigator.of(this.mainContext).maybePop().then((val){
            if(val){
              //myMainTabBar.myChild.clickToChangeMenu(0);
              dismissProgressHUD();
            }
          });
        });

        // when goals gonna refresh
      });

    }).catchError((error){
      dismissProgressHUD();
      print("ERRORX:${error.toString()}");
      var msg = Statics.shared.snackBarMsgOnBottomSheet(error.toString());
      Scaffold.of(this.mainContext).showSnackBar(msg);
    });
  }
}
