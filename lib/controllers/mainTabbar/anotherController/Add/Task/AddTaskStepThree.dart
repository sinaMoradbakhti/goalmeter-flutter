import 'dart:math';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:goal_meter/models/Statics/Strings.dart';
import 'package:goal_meter/models/Statics/Statics.dart';
import 'MiddleWareStepThree.dart';
import 'dart:async';
import 'TaskProcess.dart';
import 'package:goal_meter/views/dialogs/AlertDialog.dart';

class AddTaskStepThree extends StatefulWidget {

  final MiddleWareStepThree md = MiddleWareStepThree();

  AddTaskStepThreeState child;
  bool isRepeatingTask = false;

  AddTaskStepThree({bool isRepeatingTask}){
    this.isRepeatingTask = isRepeatingTask;
  }

  @override
  AddTaskStepThreeState createState() => AddTaskStepThreeState();
}

class AddTaskStepThreeState extends State<AddTaskStepThree> with AutomaticKeepAliveClientMixin<AddTaskStepThree> {

  @override
  bool get wantKeepAlive => true;

  // Date&Time Picker
  DateTime _date = DateTime.now();
  DateTime _dateEnd = DateTime.now();
  DateTime _selectedDate = DateTime.now();
  DateTime _selectedEndDate = DateTime.now();

  TimeOfDay _time = TimeOfDay.now();
  TimeOfDay _selectedTimeStart = TimeOfDay.now();
  TimeOfDay _selectedTimeStop = TimeOfDay.now();

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(context: context, initialDate: _selectedDate, firstDate: new DateTime(2018), lastDate: new DateTime(2022));
    if(picked != null && picked != _date){
      setState(() {
        _selectedDate = picked;
        this.widget.md.txtBtnDatePicker = "${this.widget.md.convertMonthToStr(picked.month)} ${picked.day}, ${picked.year} (${this.widget.md.convertWeekDayToStr(picked.weekday)})";
      });
    }
  }
  Future<Null> _selectEndDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(context: context, initialDate: _selectedEndDate, firstDate: new DateTime(2018), lastDate: new DateTime(2022));
    if(picked != null && picked != _dateEnd){
      setState(() {
        _selectedEndDate = picked;
        this.widget.md.txtBtnEndDatePicker = "${this.widget.md.convertMonthToStr(picked.month)} ${picked.day}, ${picked.year} (${this.widget.md.convertWeekDayToStr(picked.weekday)})";
        this.widget.md.endDateWidget = Text(this.widget.md.txtBtnEndDatePicker,style: TextStyle(fontSize: 17));
        this.widget.md.endDateTarget = "date";
      });
    }
  }

  Future<Null> _selectStartTime(BuildContext context) async {
    final TimeOfDay picked = await showTimePicker(context: context, initialTime: _selectedTimeStart);
    if(picked != null && picked != _time){
      setState(() {
        _selectedTimeStart = picked;
        String hourStr = "";
        String minStr = "";
        if(picked.hour < 10){
          hourStr = "0${picked.hour}";
        }else{
          hourStr = "${picked.hour}";
        }
        if(picked.minute < 10){
          minStr = "0${picked.minute}";
        }else{
          minStr = "${picked.minute}";
        }
        this.widget.md.txtBtnStartTimePicker = "${hourStr}:${minStr}";
      });
    }
  }
  Future<Null> _selectEndTime(BuildContext context) async {
    final TimeOfDay picked = await showTimePicker(context: context, initialTime: _selectedTimeStop);
    if(picked != null && picked != _time){
      setState(() {
        _selectedTimeStop = picked;
        String hourStr = "";
        String minStr = "";
        if(picked.hour < 10){
          hourStr = "0${picked.hour}";
        }else{
          hourStr = "${picked.hour}";
        }
        if(picked.minute < 10){
          minStr = "0${picked.minute}";
        }else{
          minStr = "${picked.minute}";
        }

        this.widget.md.txtBtnendTimePicker = "${hourStr}:${minStr}";
      });
    }
  }
  // Date&Time Picker

  void nextTabFunc(isOk(bool status)){
    TaskProcess.shared.startDate = _selectedDate;
    TaskProcess.shared.endDate = _selectedEndDate;
    if(this.widget.md.endDateTarget == "date"){
      TaskProcess.shared.isEndDateForEver = false;
    }
    else{
      TaskProcess.shared.isEndDateForEver = true;
    }
    if(this.widget.isRepeatingTask == true){
      TaskProcess.shared.isEndDateOn = true;
    }
    else{
      TaskProcess.shared.isEndDateOn = false;
    }
    TaskProcess.shared.isEndOnNexDay = this.widget.md.isEndTimeNexDay;
    TaskProcess.shared.startTime = _selectedTimeStart;
    TaskProcess.shared.endTime = _selectedTimeStop;
    TaskProcess.shared.isTimeOn = this.widget.md.isSpecificTimeOn;

    isOk(true);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    TaskProcess.shared.currentSlide = 2;
    this.widget.child = this;
    double screenWidth = MediaQuery.of(context).size.width;
    if(this.widget.md.screenWidth == 0){
      this.widget.md.txtBtnDatePicker = "${this.widget.md.convertMonthToStr(_date.month)} ${_date.day}, ${_date.year} (${this.widget.md.convertWeekDayToStr(_date.weekday)})";
      this.widget.md.screenWidth = screenWidth;

      setState(() {
        this.widget.md.isSpecificTimeOn = true;
      });

      if(this.widget.isRepeatingTask == true){
        this.widget.md.txtBtnEndDatePicker = "${this.widget.md.convertMonthToStr(_dateEnd.month)} ${_dateEnd.day}, ${_dateEnd.year} (${this.widget.md.convertWeekDayToStr(_dateEnd.weekday)})";
      }
    }
    if(this.widget.md.txtBtnEndDatePicker == ""){
      this.widget.md.txtBtnEndDatePicker = "${this.widget.md.convertMonthToStr(_dateEnd.month)} ${_dateEnd.day}, ${_dateEnd.year} (${this.widget.md.convertWeekDayToStr(_dateEnd.weekday)})";
    }

    return new Scaffold(
      backgroundColor: Colors.white,
      body: Builder(
        builder: (mainContext) =>
            Container(
                margin: const EdgeInsets.only(left: 0, right: 0, top: 16, bottom: 16),
                child: ListView(
                  children: [
                    Stack(
                      children: [
                        Container(
                          child: Stack(
                            children: [
                              Container(
                                child: Text(" ${Strings.shared.addTaskStep3DateLabel} ", style: TextStyle(color: Colors.grey, fontWeight: FontWeight.normal, fontSize: 18, backgroundColor: Colors.white)),
                                transform: Matrix4.translationValues(0.0, -10.0, 0.0),
                              )
                              ,
                              rowTaskDateLabel()
                              ,
                              rowTaskChooseDate(mainContext)
                            ],
                          ),
                          width: screenWidth,
                          padding: const EdgeInsets.only(left: 8, bottom: 10),
                          margin: const EdgeInsets.only(left: 8, right: 8),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(Radius.circular(10)),
                              border: Border.all(color: Colors.grey,width: 1)
                          ),
                        ),
                      ],
                    ) // row 1
                    ,
                    SizedBox(height: 20)
                    ,
                    Stack(
                      children: [
                        Container(
                          child: Stack(
                            children: [
                              Container(
                                child: Text(" ${Strings.shared.addTaskStep3TimeLabel} ", style: TextStyle(color: Colors.grey, fontWeight: FontWeight.normal, fontSize: 18, backgroundColor: Colors.white)),
                                transform: Matrix4.translationValues(0.0, -10.0, 0.0),
                              )
                              ,
                              rowTaskTimeLabel()
                              ,
                              rowTaskChooseTime(mainContext)
                            ],
                          ),
                          width: screenWidth,
                          padding: const EdgeInsets.only(left: 8),
                          margin: const EdgeInsets.only(left: 8, right: 8),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(Radius.circular(10)),
                              border: Border.all(color: Colors.grey,width: 1)
                          ),
                        ),
                      ],
                    ) // row 2
                  ],
                  padding: const EdgeInsets.only(bottom: 80, top: 10),
                )// Main List View,
            ),
      ),
    );
  }

  Container rowTaskDateLabel(){
    double screenWidth = MediaQuery.of(context).size.width;
    return Container(
      child: Row(
        children: [
          Container(child: Image.asset("Resources/icons/calenderIcon.png", scale: 1.5), width: 50,height: 50,),
          Container(child: Text(Strings.shared.addTaskStep3ChooseDateTitle, style: TextStyle(fontSize: 17,color: Statics.shared.colors.addTaskSectionsTitleColor))),
        ],
      ),
      margin: const EdgeInsets.only(top: 15),
    );// row TaskName;
  }
  Container rowTaskChooseDate(context){
    double screenWidth = MediaQuery.of(context).size.width;
    if(this.widget.isRepeatingTask == true){
      return Container(
        child: Column(
          children: [
            Row(
              children: [
                Container(width: 80,child: Text(Strings.shared.startDateLabelForAddTaskStep3, style: TextStyle(fontSize: 17,color: Statics.shared.colors.addTaskSectionsTitleColor))),
                Container(
                  child: FlatButton(
                      child: Text(
                        this.widget.md.txtBtnDatePicker,style: TextStyle(fontSize: 17),
                      ),
                      onPressed: (){_selectDate(context);},
                      padding: const EdgeInsets.all(0),
                      ),
                  margin: const EdgeInsets.only(right: 16),
                )// main container
              ],
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
            ),
            Row(
              children: [
                Container(width: 80,child: Text(Strings.shared.endDateLabelForAddTaskStep3, style: TextStyle(fontSize: 17,color: Statics.shared.colors.addTaskSectionsTitleColor))),
                Container(
                  child: FlatButton(
                    child: this.widget.md.endDateWidget,
                    onPressed: (){
                      this.showPopUpEndDate(
                          onPressForEver: (){
                            Navigator.pop(context);
                            setState(() {
                              this.widget.md.endDateWidget = Text(Strings.shared.forEverCamelCap,style: TextStyle(fontSize: 17,color: Statics.shared.colors.addTaskSectionsTitleColor));
                              this.widget.md.endDateTarget = "forever";
                            });
                          },
                        onPressPickDate: (){
                          Navigator.pop(context);
                          _selectEndDate(context);
                        });
                    },
                    padding: const EdgeInsets.all(0),
                  ),
                  alignment: Alignment.centerRight,
                  margin: const EdgeInsets.only(right: 16),
                ),// main container
              ],
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
            )
          ],
        ),
        margin: const EdgeInsets.only(top: 65),
        padding: const EdgeInsets.only(left: 8, right: 0 , bottom: 8, top: 8),
      );
    }else{
      return Container(
        child: Column(
          children: [
            Row(
              children: [
                Container(width: 80,child: Text(Strings.shared.addTaskStep3DateLabel, style: TextStyle(fontSize: 17, color: Statics.shared.colors.addTaskSectionsTitleColor))),
                Container(
                  alignment: Alignment.centerRight,
                  child: FlatButton(child: Text(this.widget.md.txtBtnDatePicker,style: TextStyle(fontSize: 17)), onPressed: (){_selectDate(context);}),
                )
              ],
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
            ),
          ],
        ),
        margin: const EdgeInsets.only(top: 65),
        padding: const EdgeInsets.only(left: 8, right: 0 , bottom: 8, top: 8),
      );
    }
  }
  showPopUpEndDate({VoidCallback onPressForEver, VoidCallback onPressPickDate}){
    double popUpWidth = this.widget.md.screenWidth - 64;
    double height = MediaQuery.of(context).size.height / 1.5;

    if(MediaQuery.of(context).size.height < 750){
      height = MediaQuery.of(context).size.height - 10;
    }
    showDialog(context: context,builder: (BuildContext context) {
      return PopUpDialog(popUpWidth: popUpWidth,
          onPressForEver: (){onPressForEver();},
          onPressPickDate: (){onPressPickDate();},
          popUpHeight: height,
      ).dialog();
    });
  }

  Container rowTaskTimeLabel(){
    double screenWidth = MediaQuery.of(context).size.width;
    return Container(
      child: Row(
        children: [
          Container(child: Image.asset("Resources/icons/clockIcon.png", scale: 1.5), width: 50,height: 50,),
          Container(child: Text(Strings.shared.addTaskStep3ChooseTimeTitle, style: TextStyle(fontSize: 17,color: Statics.shared.colors.addTaskSectionsTitleColor)),
          width: screenWidth - 50 - 32 - 16 - 60),
          Container(child: Switch(
            value: this.widget.md.isSpecificTimeOn,
            onChanged: (changed){setState(() {
              this.widget.md.isSpecificTimeOn = changed;
            });},
          ),height: 50, width: 60,),
        ],
      ),
      margin: const EdgeInsets.only(top: 15),
      padding: const EdgeInsets.only(bottom: 10),
    );// row TaskName;
  }
  Container rowTaskChooseTime(context){
    if(this.widget.md.isSpecificTimeOn){
      double screenWidth = MediaQuery.of(context).size.width;
      return Container(
        child: Column(
          children: [
            Row(
              children: [
                Container(width: 80,child: Text(Strings.shared.addTaskStep3StartTimeLabel, style: TextStyle(fontSize: 17, color: Statics.shared.colors.addTaskSectionsTitleColor))),
                Container(
                  alignment: Alignment.centerRight,
                  child: FlatButton(child: Text(this.widget.md.txtBtnStartTimePicker,style: TextStyle(fontSize: 17)), onPressed: (){_selectStartTime(context);}),
                )
              ],
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
            ),
            Row(
              children: [
                Container(width: 80,child: Text(Strings.shared.addTaskStep3EndTimeLabel, style: TextStyle(fontSize: 17, color: Statics.shared.colors.addTaskSectionsTitleColor))),
                Container(
                  alignment: Alignment.centerRight,
                  child: FlatButton(child: Text(this.widget.md.txtBtnendTimePicker,style: TextStyle(fontSize: 17)), onPressed: (){_selectEndTime(context);}),
                )
              ],
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
            ),
            Container(child:
              CheckboxListTile
              (
                onChanged: (changed){setState(() {
                  this.widget.md.isEndTimeNexDay = changed;
                });},
                value: this.widget.md.isEndTimeNexDay,
                title: Text(Strings.shared.endTimeNextDay, style: TextStyle(color: Statics.shared.colors.addTaskSectionsTitleColor, fontSize: 17)),
            ),
              margin: const EdgeInsets.only(right: 16),
            )
          ],
        ),
        margin: const EdgeInsets.only(top: 65),
        padding: const EdgeInsets.only(left: 8, right: 0 , bottom: 8, top: 8),
      );
    }
    else{
      return Container();
    }
  }

}
