import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:goal_meter/models/Statics/Strings.dart';
import 'package:goal_meter/models/Statics/Statics.dart';
import 'MiddleWare.dart';
import 'TaskProcess.dart';
import 'package:goal_meter/views/dialogs/AlertDialogMonthDayPicker.dart';
import 'package:goal_meter/views/AddTask/CustomRadioButton.dart';

class AddTaskStepTwo extends StatefulWidget {

  PopUpDialogMonthDayPicker popUpSelectMultiDaysOfMonth;
  AddTaskStepTwoState child;
  final MiddleWare md = MiddleWare();

  @override
  AddTaskStepTwoState createState(){
    return AddTaskStepTwoState();
  }
}

class AddTaskStepTwoState extends State<AddTaskStepTwo> with AutomaticKeepAliveClientMixin<AddTaskStepTwo> {

  @override
  bool get wantKeepAlive => true;

  String _myActivitiesResult;
  final formKey = new GlobalKey<FormState>();

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    TaskProcess.shared.currentSlide = 1;
    this.widget.md.myActivities = [];
    _myActivitiesResult = '';
  }

  void nextTabFunc(isOk(bool status)){
    RefreshTaskProcess rf = new RefreshTaskProcess(this.widget.md);
    rf.refreshTaskProcess();

    if(this.widget.md.unitRadioValueRepeatinOrOnetime == 0){
      TaskProcess.shared.taskP3.isRepeatingTask = false;
      isOk(true);
    }else{
      TaskProcess.shared.taskP3.isRepeatingTask = true;
      isOk(true);
    }
  }

  var mainContext;

  @override
  Widget build(BuildContext context) {
    print("Step 2 Build");
    super.build(context);
    TaskProcess.shared.currentSlide = 1;
    mainContext = context;
    this.widget.child = this;
    double screenWidth = MediaQuery.of(context).size.width;
    if(this.widget.md.screenWidth == 0){
      this.widget.md.screenWidth = screenWidth;

      this.widget.md.expandedMonthlyMoreOption = Container(
        child: OutlineButton(
          onPressed: (){
            this.showPopUpPickDayMonth();
          },
          child: Row(
            children: [
              Text(Strings.shared.pleaseSelectOneOrMoreDaysMonthly, style: TextStyle(fontSize: 17),),
              Icon(Icons.arrow_drop_down, color: Colors.red,size: 30,),
            ],
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
          ),
          disabledBorderColor: Colors.grey,
          padding: const EdgeInsets.only(right: 8, left: 10, top: 10, bottom: 10),
        ),
        margin: const EdgeInsets.only(right: 16, left: 16),
      );

      this.widget.md.oneTimeRadioButton = CustomRadioButton(title: Strings.shared.RadioButtonTaskTypeOneTimeTask, onPressed: (){
        // on Press One Time Task
        this.radioRepeatingOneTimeChanged(0);
      },isChecked: true,);
      this.widget.md.repeatingTimeRadioButton = CustomRadioButton(title: Strings.shared.RadioButtonTaskTypeRepeatingTask,onPressed: (){
        // on Press Repeating Time Task
        this.radioRepeatingOneTimeChanged(1);
      },);
      this.widget.md.dailyRadioButton = CustomRadioButton(title: Strings.shared.daily,onPressed: (){
        // on Press daily Tasks
        this.radioDailyWeeklyMonthlyChanged(0);
      },isChecked: true,);
      this.widget.md.weeklyRadioButton = CustomRadioButton(title: Strings.shared.weekly,onPressed: (){
        // on Press weekly Tasks
        this.radioDailyWeeklyMonthlyChanged(1);
      },);
      this.widget.md.monthlyTimeRadioButton = CustomRadioButton(title: Strings.shared.monthly,onPressed: (){
        // on Press monthly Tasks
        this.radioDailyWeeklyMonthlyChanged(2);
      },);

      this.refreshOneTimeTask();
      changeSwitchWeeklySpecificDays(true);
      changeSwitchMonthlySpecificDays(true);

    }

    double popUpWidth = this.widget.md.screenWidth - 64;
    double height = MediaQuery.of(context).size.height / 1.3;
    if(MediaQuery.of(context).size.height < 750){
      height = MediaQuery.of(context).size.height - 10;
    }

    this.widget.popUpSelectMultiDaysOfMonth = PopUpDialogMonthDayPicker(popUpWidth: popUpWidth,
      onPressClose: (){
        Navigator.pop(this.context);
      },
      onPressSave: (){
        Navigator.pop(this.context);
        setState(() {
          this.widget.md.expandedMonthlyMoreOption = Container();
        });

        List<int> items = this.widget.popUpSelectMultiDaysOfMonth.returnMyList;
        List<Widget> items2 = [];
        for(int i = 0;i < items.length; i++){
          String strTitle = "";
          if(items[i] == 1){
            strTitle = "${items[i]} (${Strings.shared.multiSelectFirstDayOfMonth})";
          }else{
            if(items[i] == 31){
              strTitle = "${Strings.shared.multiSelectLastDayOfMonth}";
            }else{
              strTitle = "${items[i]}";
            }
          }
          items2.add(Container(
            child: Text(strTitle, style: TextStyle(fontSize: 17, color: Colors.blue),),
            padding: const EdgeInsets.only(top: 8, bottom: 8, right: 0, left: 0),
            alignment: Alignment.centerLeft,
          ));
        }
        this.refreshListOfMonthLySelectedSpecificDays(list: items2,list2: items);
        if(items2.length <= 0){
          this.widget.md.expandedMonthlyMoreOption = Container(
            child: OutlineButton(
              onPressed: (){
                this.showPopUpPickDayMonth();
              },
              child: Row(
                children: [
                  Text(Strings.shared.pleaseSelectOneOrMoreDaysMonthly, style: TextStyle(fontSize: 17),),
                  Icon(Icons.arrow_drop_down, color: Colors.red,size: 30,),
                ],
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
              ),
              disabledBorderColor: Colors.grey,
              padding: const EdgeInsets.only(right: 8, left: 10, top: 10, bottom: 10),
            ),
            margin: const EdgeInsets.only(right: 16, left: 16),
          );
        }
        this.changeSwitchMonthlySpecificDays(true);
      }, // press Save
      popUpHeight: height,
      context: context,
    );

    List<Widget> oneTimeW = [
      Stack(
        children: [
          Container(
            child: Stack(
              children: [
                Container(
                  child: Text(" ${Strings.shared.stepTwoContainerTitleAddNewTaskType} ", style: TextStyle(color: Statics.shared.colors.addTaskSectionsTitleColor, fontWeight: FontWeight.normal, fontSize: Statics.shared.fontSizes.addTaskSectionsTitle, backgroundColor: Colors.white)),
                  transform: Matrix4.translationValues(0.0, -10.0, 0.0),
                )
                ,
                rowTaskTypeLabel()// row TaskType Label
                ,
                rowTaskTypeRadioOneTime()// first radio button
              ],
            ),
            height: 190,
            width: screenWidth,
            padding: const EdgeInsets.only(left: 8),
            margin: const EdgeInsets.only(left: 8, right: 8),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                border: Border.all(color: Statics.shared.colors.addTaskSectionsBorderColor,width: 1)
            ),
          ),
        ],
      ) // row 1
      ,
      SizedBox(height: 20)
      ,
      this.widget.md.stackRowTwo // row 2
      ,
      SizedBox(height: 20)
    ];
    List<Widget> repeatingW = [
      Stack(
        children: [
          Container(
            child: Stack(
              children: [
                Container(
                  child: Text(" ${Strings.shared.stepTwoContainerTitleAddNewTaskType} ", style: TextStyle(color: Statics.shared.colors.addTaskSectionsTitleColor, fontWeight: FontWeight.normal, fontSize: Statics.shared.fontSizes.addTaskSectionsTitle, backgroundColor: Colors.white)),
                  transform: Matrix4.translationValues(0.0, -10.0, 0.0),
                )
                ,
                rowTaskTypeLabel()// row TaskType Label
                ,
                rowTaskTypeRadioOneTime()// first radio button
              ],
            ),
            height: 190,
            width: screenWidth,
            padding: const EdgeInsets.only(left: 8),
            margin: const EdgeInsets.only(left: 8, right: 8),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                border: Border.all(color: Statics.shared.colors.addTaskSectionsBorderColor,width: 1)
            ),
          ),
        ],
      ) // row 1
      ,
      SizedBox(height: 20)
      ,
      this.widget.md.stackRowTwo // row 2
      ,
      SizedBox(height: 20)
      ,
      this.widget.md.stackRowThree // row 3
    ];

    if(this.widget.md.unitRadioValueRepeatinOrOnetime == 0){
      return new Scaffold(
        backgroundColor: Colors.white,
        body: Builder(
          builder: (mainContext) =>
              Container(
                  margin: const EdgeInsets.only(left: 0, right: 0, top: 16, bottom: 16),
                  child: ListView(
                    children: oneTimeW,
                    padding: const EdgeInsets.only(bottom: 80, top: 10),
                  )// Main List View,
              ),
        ),
      );
    }else{
      return new Scaffold(
        backgroundColor: Colors.white,
        body: Builder(
          builder: (mainContext) =>
              Container(
                  margin: const EdgeInsets.only(left: 0, right: 0, top: 16, bottom: 16),
                  child: ListView(
                    children: repeatingW,
                    padding: const EdgeInsets.only(bottom: 80, top: 10),
                  )// Main List View,
              ),
        ),
      );
    }
  }

  void refreshRepeatingTask({int id = -1}){
    if (!mounted) return;
    setState(() {

      if(id == 0){
        this.widget.md.dailyRadioButton = CustomRadioButton(title: Strings.shared.daily,onPressed: (){
          // on Press daily Tasks
          this.radioDailyWeeklyMonthlyChanged(0);
        },isChecked: true,);
        this.widget.md.weeklyRadioButton = CustomRadioButton(title: Strings.shared.weekly,onPressed: (){
          // on Press weekly Tasks
          this.radioDailyWeeklyMonthlyChanged(1);
        });
        this.widget.md.monthlyTimeRadioButton = CustomRadioButton(title: Strings.shared.monthly,onPressed: (){
          // on Press monthly Tasks
          this.radioDailyWeeklyMonthlyChanged(2);
        },);
      }else if(id == 1){
        this.widget.md.dailyRadioButton = CustomRadioButton(title: Strings.shared.daily,onPressed: (){
          // on Press daily Tasks
          this.radioDailyWeeklyMonthlyChanged(0);
        });
        this.widget.md.weeklyRadioButton = CustomRadioButton(title: Strings.shared.weekly,onPressed: (){
          // on Press weekly Tasks
          this.radioDailyWeeklyMonthlyChanged(1);
        },isChecked: true,);
        this.widget.md.monthlyTimeRadioButton = CustomRadioButton(title: Strings.shared.monthly,onPressed: (){
          // on Press monthly Tasks
          this.radioDailyWeeklyMonthlyChanged(2);
        },);
      }else if(id == 2){
        this.widget.md.dailyRadioButton = CustomRadioButton(title: Strings.shared.daily,onPressed: (){
          // on Press daily Tasks
          this.radioDailyWeeklyMonthlyChanged(0);
        });
        this.widget.md.weeklyRadioButton = CustomRadioButton(title: Strings.shared.weekly,onPressed: (){
          // on Press weekly Tasks
          this.radioDailyWeeklyMonthlyChanged(1);
        });
        this.widget.md.monthlyTimeRadioButton = CustomRadioButton(title: Strings.shared.monthly,onPressed: (){
          // on Press monthly Tasks
          this.radioDailyWeeklyMonthlyChanged(2);
        },isChecked: true,);
      }

      this.widget.md.stackRowTwo = Stack(
        children: [
          Container(
            child: Stack(
              children: [
                Container(
                  child: Text(" ${Strings.shared.RadioButtonTaskTypeRepeatingTaskLabel} ", style: TextStyle(color: Statics.shared.colors.addTaskSectionsTitleColor, fontWeight: FontWeight.normal, fontSize: Statics.shared.fontSizes.addTaskSectionsTitle, backgroundColor: Colors.white)),
                  transform: Matrix4.translationValues(0.0, -10.0, 0.0),
                )
                ,
                rowTaskTypeRadiosRepeatingLabel(),
                rowTaskTypeRadiosRepeating(),
              ],
            ),
            height: 250,
            width: this.widget.md.screenWidth,
            padding: const EdgeInsets.only(left: 8),
            margin: const EdgeInsets.only(left: 8, right: 8),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                border: Border.all(color: Statics.shared.colors.addTaskSectionsBorderColor,width: 1)
            ),
          ),
        ],
      );
      int status = this.widget.md.unitRadioValueDailyWeeklyMonthlyChanged;
      if(status == 0){
        refreshRepeatingTaskDaily();
      }
      else if(status == 1){
        refreshRepeatingTaskWeekly();
      }
      else if(status == 2){
        refreshRepeatingTaskMonthly();
      }
    });
  }
  void refreshOneTimeTask(){
    if (!mounted) return;
    setState(() {
      this.widget.md.oneTimeRadioButton.isChecked = true;
      this.widget.md.stackRowTwo = Stack(
        children: [
          Container(
            child: Stack(
              children: [
                Container(
                  child: Text(" ${Strings.shared.RadioButtonTaskTypeOneTimeTaskLabel} ", style: TextStyle(color: Statics.shared.colors.addTaskSectionsTitleColor, fontWeight: FontWeight.normal, fontSize: Statics.shared.fontSizes.addTaskSectionsTitle, backgroundColor: Colors.white)),
                  transform: Matrix4.translationValues(0.0, -10.0, 0.0),
                )
                ,
                Container(
                  child: Text(Strings.shared.continueToNextStep, style: TextStyle(fontSize: 17, color: Statics.shared.colors.addTaskSectionsBorderColor)),
                  margin: const EdgeInsets.only(top: 30, left: 5, right: 5),
                )
              ],
            ),
            height: 80,
            width: this.widget.md.screenWidth,
            padding: const EdgeInsets.only(left: 8),
            margin: const EdgeInsets.only(left: 8, right: 8),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                border: Border.all(color: Statics.shared.colors.addTaskSectionsBorderColor,width: 1)
            ),
          ),
        ],
      );
    });
  }
  void refreshRepeatingTaskDaily(){
    if (!mounted) return;
    setState(() {

      this.widget.md.stackRowThree = Stack(
        children: [
          Container(
            child: Stack(
              children: [
                Container(
                  child: Text(" ${Strings.shared.daily} ", style: TextStyle(color: Statics.shared.colors.addTaskSectionsTitleColor, fontWeight: FontWeight.normal, fontSize: Statics.shared.fontSizes.addTaskSectionsTitle, backgroundColor: Colors.white)),
                  transform: Matrix4.translationValues(0.0, -10.0, 0.0),
                )
                ,
                rowTaskTypeRadiosRepeatingDailyLabel(),
                rowTaskTypeRadiosRepeatingDaily(),
              ],
            ),
            height: 200,
            width: this.widget.md.screenWidth,
            padding: const EdgeInsets.only(left: 8),
            margin: const EdgeInsets.only(left: 8, right: 8),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                border: Border.all(color: Statics.shared.colors.addTaskSectionsBorderColor,width: 1)
            ),
          ),
        ],
      );
    });
  }
  void refreshRepeatingTaskWeekly(){
    if (!mounted) return;
    setState(() {

      this.widget.md.stackRowThree = Stack(
        children: [
          Container(
            child: Stack(
              children: [
                Container(
                  child: Text(" ${Strings.shared.weekly} ", style: TextStyle(color: Statics.shared.colors.addTaskSectionsTitleColor, fontWeight: FontWeight.normal, fontSize: Statics.shared.fontSizes.addTaskSectionsTitle, backgroundColor: Colors.white)),
                  transform: Matrix4.translationValues(0.0, -10.0, 0.0),
                )
                ,
                rowTaskTypeRadiosRepeatingWeeklyLabel(),
                rowTaskTypeRadiosRepeatingWeekly(),
              ],
            ),
            //height: 250,
            width: this.widget.md.screenWidth,
            padding: const EdgeInsets.only(left: 8, bottom: 10),
            margin: const EdgeInsets.only(left: 8, right: 8),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                border: Border.all(color: Statics.shared.colors.addTaskSectionsBorderColor,width: 1)
            ),
          ),
        ],
      );
    });
  }
  void refreshRepeatingTaskMonthly(){
    if (!mounted) return;
    setState(() {
      this.widget.md.stackRowThree = Stack(
        children: [
          Container(
            child: Stack(
              children: [
                Container(
                  child: Text(" ${Strings.shared.monthly} ", style: TextStyle(color: Statics.shared.colors.addTaskSectionsTitleColor, fontWeight: FontWeight.normal, fontSize: Statics.shared.fontSizes.addTaskSectionsTitle, backgroundColor: Colors.white)),
                  transform: Matrix4.translationValues(0.0, -10.0, 0.0),
                )
                ,
                rowTaskTypeRadiosRepeatingMonthlyLabel(),
                rowTaskTypeRadiosRepeatingMonthly(),
              ],
            ),
            width: this.widget.md.screenWidth,
            padding: const EdgeInsets.only(left: 8, bottom: 10,right: 8),
            margin: const EdgeInsets.only(left: 8, right: 8),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                border: Border.all(color: Statics.shared.colors.addTaskSectionsBorderColor,width: 1)
            ),
          ),
        ],
      );
    });
  }

  void radioRepeatingOneTimeChanged(index){
    if (!mounted) return;
    setState(() {
      this.widget.md.unitRadioValueRepeatinOrOnetime = index;
      if(index == 0){
        this.refreshOneTimeTask();

        this.widget.md.oneTimeRadioButton = CustomRadioButton(title: Strings.shared.RadioButtonTaskTypeOneTimeTask, onPressed: (){
          // on Press One Time Task
          this.radioRepeatingOneTimeChanged(0);
        },isChecked: true,);
        this.widget.md.repeatingTimeRadioButton = CustomRadioButton(title: Strings.shared.RadioButtonTaskTypeRepeatingTask,onPressed: (){
          // on Press Repeating Time Task
          this.radioRepeatingOneTimeChanged(1);
        },);

      }
      else if(index == 1){
        this.refreshRepeatingTask();

        this.widget.md.oneTimeRadioButton = CustomRadioButton(title: Strings.shared.RadioButtonTaskTypeOneTimeTask, onPressed: (){
          // on Press One Time Task
          this.radioRepeatingOneTimeChanged(0);
        });
        this.widget.md.repeatingTimeRadioButton = CustomRadioButton(title: Strings.shared.RadioButtonTaskTypeRepeatingTask,onPressed: (){
          // on Press Repeating Time Task
          this.radioRepeatingOneTimeChanged(1);
        },isChecked: true,);
      }
    });
  }
  void radioDailyWeeklyMonthlyChanged(index){
    if (!mounted) return;
    setState(() {
      this.widget.md.unitRadioValueDailyWeeklyMonthlyChanged = index;

      this.refreshRepeatingTask(id: index);

      if(index == 0){
        refreshRepeatingTaskDaily();
      }// daily
      else if(index == 1){
        refreshRepeatingTaskWeekly();
      }// weekly
      else if(index == 2){
        refreshRepeatingTaskMonthly();
      }// monthly
    });
  }

  Container rowTaskTypeLabel(){
    double screenWidth = MediaQuery.of(context).size.width;
    return Container(
      child: Row(
        children: [
          Container(child: Image.asset("Resources/icons/taskTypeIcon.png", scale: 1.2), width: 50,height: 50,),
          Container(child: Text(Strings.shared.firstRowStep2AddTaskTitle, style: TextStyle(fontSize: 17,color: Statics.shared.colors.addTaskSectionsTitleColor))),
        ],
      ),
      margin: const EdgeInsets.only(top: 15),
    );// row TaskName;
  }
  Container rowTaskTypeRadioOneTime(){
    double screenWidth = MediaQuery.of(context).size.width;
    return Container(
      child: Column(
        children: [
          Container(child: this.widget.md.oneTimeRadioButton,margin: const EdgeInsets.only(left: 48, right: 16)),
          Container(child: this.widget.md.repeatingTimeRadioButton,margin: const EdgeInsets.only(left: 48, right: 16)),
        ],
      ),
      margin: const EdgeInsets.only(top: 65),
    );// row TaskName;
  }

  Container rowTaskTypeRadiosRepeatingLabel(){
    double screenWidth = MediaQuery.of(context).size.width;
    return Container(
      child: Row(
        children: [
          Container(child: Image.asset("Resources/icons/tril_icon.png", scale: 1.5), width: 50,height: 50,),
          Container(child: Text(Strings.shared.stepTwoContainerTitleAddNewTaskRowRepeatingTask, style: TextStyle(fontSize: 17,color: Statics.shared.colors.addTaskSectionsTitleColor))),
        ],
      ),
      margin: const EdgeInsets.only(top: 15),
    );// row TaskName;
  }
  Container rowTaskTypeRadiosRepeating(){
    double screenWidth = MediaQuery.of(context).size.width;
    return Container(
      child: Column(
        children: [
          Container(child: this.widget.md.dailyRadioButton,margin: const EdgeInsets.only(left: 48, right: 16)),
          Container(child: this.widget.md.weeklyRadioButton,margin: const EdgeInsets.only(left: 48, right: 16)),
          Container(child: this.widget.md.monthlyTimeRadioButton,margin: const EdgeInsets.only(left: 48, right: 16)),
        ],
      ),
      margin: const EdgeInsets.only(top: 65),
    );// row TaskName;
  }

  Container rowTaskTypeRadiosRepeatingDailyLabel(){
    double screenWidth = MediaQuery.of(context).size.width;
    return Container(
      child: Row(
        children: [
          Container(child: Image.asset("Resources/icons/dailyRepeatingTaskIcon.png", scale: 1.5), width: 50,height: 50,),
          Container(child: Text(Strings.shared.stepTwoContainerTitleAddNewTaskRowRepeatingDailyTask, style: TextStyle(fontSize: 17, color: Statics.shared.colors.addTaskSectionsTitleColor))),
        ],
      ),
      margin: const EdgeInsets.only(top: 15),
    );// row TaskName;
  }
  Container rowTaskTypeRadiosRepeatingDaily(){
    double screenWidth = MediaQuery.of(context).size.width;
    return Container(
      child: Column(
        children: [
          Row(
            children: this.widget.md.dailyDaysTop,
            mainAxisAlignment: MainAxisAlignment.end,
          ),
          Row(
            children: this.widget.md.dailyDaysBottom,
            mainAxisAlignment: MainAxisAlignment.end,
          )
        ],
      )
      ,
      margin: const EdgeInsets.only(top: 65, right: 16),
    );// row TaskName;
  }

  Container rowTaskTypeRadiosRepeatingWeeklyLabel(){
    double screenWidth = MediaQuery.of(context).size.width;
    return Container(
      child: Row(
        children: [
          Container(child: Image.asset("Resources/icons/weeklyRepeatingTaskIcon.png", scale: 1.5), width: 50,height: 50,),
          Container(child: Text(Strings.shared.stepTwoContainerTitleAddNewTaskRowRepeatingWeeklyTask, style: TextStyle(fontSize: 17,color: Statics.shared.colors.addTaskSectionsTitleColor))),
        ],
      ),
      margin: const EdgeInsets.only(top: 15),
    );// row TaskName;
  }
  Container rowTaskTypeRadiosRepeatingWeekly(){

    double screenWidth = MediaQuery.of(context).size.width;
    return Container(
      child: Column(
        children: [
          Row(
            children: [
              Container(width: 80,
                  child: TextFormField(
                      decoration: InputDecoration(
                          hintText: Strings.shared.textfieldPlaceHolderHoursPerWeek,
                      ),
                    textAlign: TextAlign.center,
                    controller: this.widget.md.txtControllerHoursPerWeek,
                    enableInteractiveSelection: false,
                  )
              ),
              SizedBox(width: 10),
              Container(
                  child: Text(Strings.shared.labelHoursPerWeek, style: TextStyle(color: Colors.grey)))
            ],
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
          ),
          SizedBox(height: 10),
          Row(
          children: [
            Container(child: Text(Strings.shared.onSpecificDaysOfWeek, style: TextStyle(fontSize: 15, color: Colors.grey)),margin: const EdgeInsets.only(left: 48)),
            Container(child: Switch(value: this.widget.md.isSpecificWeeklyDaysTrue, onChanged: (changed){changeSwitchWeeklySpecificDays(changed);},)),
          ],
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
          ),
          SizedBox(height: 10),
          this.widget.md.expandedWeeklyMoreOption
        ],
      ),
      margin: const EdgeInsets.only(top: 65, right: 8),
    );
  }
  void changeSwitchWeeklySpecificDays(bool sth){
    if(sth){
      if (!mounted) return;
      setState(() {
        this.widget.md.isSpecificWeeklyDaysTrue = true;
        this.widget.md.expandedWeeklyMoreOption = Column(children: [
          Row(
            children: this.widget.md.weeklyDaysTop,
            mainAxisAlignment: MainAxisAlignment.end,
          ),
          Row(
            children: this.widget.md.weeklyDaysBottom,
            mainAxisAlignment: MainAxisAlignment.end,
          )
        ],);
      });
    }else{
      if (!mounted) return;
      setState(() {
        this.widget.md.isSpecificWeeklyDaysTrue = false;
        this.widget.md.expandedWeeklyMoreOption = Row();
      });
    }
    refreshRepeatingTaskWeekly();
  }

  Container rowTaskTypeRadiosRepeatingMonthlyLabel(){
    double screenWidth = MediaQuery.of(context).size.width;
    return Container(
      child: Row(
        children: [
          Container(child: Image.asset("Resources/icons/weeklyRepeatingTaskIcon.png", scale: 1.5), width: 50,height: 50,),
          Container(child: Text(Strings.shared.stepTwoContainerTitleAddNewTaskRowRepeatingMonthlyTask, style: TextStyle(fontSize: 17,color: Statics.shared.colors.addTaskSectionsTitleColor))),
        ],
      ),
      margin: const EdgeInsets.only(top: 15),
    );// row TaskName;
  }
  Container rowTaskTypeRadiosRepeatingMonthly(){
    double screenWidth = MediaQuery.of(context).size.width;
    return Container(
      child: Column(
        children: [
          Row(
            children: [
              Container(width: 80,
                  child: TextFormField(
                    decoration: InputDecoration(
                      hintText: Strings.shared.textfieldPlaceHolderHoursPerWeek,
                    ),
                    textAlign: TextAlign.center,
                    controller: this.widget.md.txtControllerHoursPerMonth,
                    enableInteractiveSelection: false,
                  )
              ),
              SizedBox(width: 10),
              Container(
                  child: Text(Strings.shared.labelHoursPerWeek, style: TextStyle(color: Colors.grey)))
            ],
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
          ),
          SizedBox(height: 10),
          Row(
            children: [
              Container(child: Text(Strings.shared.onSpecificDaysOfMonth, style: TextStyle(fontSize: 15, color: Colors.grey)),margin: const EdgeInsets.only(left: 48),),
              Container(child: Switch(value: this.widget.md.isSpecificMonthlyDaysTrue, onChanged: (changed){changeSwitchMonthlySpecificDays(changed);},)),
            ],
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
          ),
          SizedBox(height: 10),
          this.widget.md.expandedMonthlyMoreOption,
          SizedBox(height: 10),
        ],
      ),
      margin: const EdgeInsets.only(top: 65),
    );
  }
  void changeSwitchMonthlySpecificDays(bool sth){
    if(sth){
      if (!mounted) return;
      setState(() {
        this.widget.md.isSpecificMonthlyDaysTrue = true;
        this.widget.md.expandedMonthlyMoreOption = Container(
          child: Column(
            children: [
              this.widget.md.expandedMonthlyMoreOption,
              SizedBox(height: 10),
              this.widget.md.listOfMonthlySpecificDay,
            ],
          ),
        );
      });
    }else{
      if (!mounted) return;
      setState(() {
        this.widget.md.isSpecificMonthlyDaysTrue = false;
        this.widget.md.expandedMonthlyMoreOption = Row();
      });
    }
    refreshRepeatingTaskMonthly();
  }

  showPopUpPickDayMonth(){
    showDialog(context: context,builder: (BuildContext context) {
      return this.widget.popUpSelectMultiDaysOfMonth.dialog(selectedList: this.widget.md.selectedMonthlyBox);
    });
  }

  void refreshListOfMonthLySelectedSpecificDays({List<Widget> list, List<int> list2}){
    setState(() {
      String res = "";
      for(var i=0; i<list2.length; i++){
        if(res == ""){
          res = "${list2[i]}";
        }
        else{
          res += ",${list2[i]}";
        }
      }

      this.widget.md.selectedMonthlyBox = list2;
      this.widget.md.resultOfMonthlyDays = res;
      this.widget.md.listOfMonthlySpecificDay = FlatButton(
        child: Column(
          children: [
            Container(child: Text(Strings.shared.daysOfMonthSentence, style: TextStyle(
                color: Statics.shared.colors.addTaskSectionsTitleColor,
                fontSize: Statics.shared.fontSizes.addTaskSectionsTitle
            ),),
              alignment: Alignment.centerLeft,
              margin: const EdgeInsets.only(left: 16),
            ),
            Container(
              padding: EdgeInsets.all(16),
              child: Column(children: list),
              margin: const EdgeInsets.only(bottom: 10, left: 16, right: 16, top: 5),
            )
          ],
        ),
        onPressed: (){
          this.showPopUpPickDayMonth();
        },
        padding: const EdgeInsets.all(0),
      );
      refreshRepeatingTaskMonthly();
    });
  }
}
