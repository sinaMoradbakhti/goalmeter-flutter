import 'package:flutter/material.dart';
import 'TaskProcess.dart';
import 'package:goal_meter/models/Statics/Strings.dart';
import 'package:goal_meter/views/AddTask/TabCircle.dart';

class AddTaskTabBarView extends StatefulWidget {

  bool isFirstInit = true;

  List<TabCircle> tabCircles =
  [
    TabCircle(isColored: true),
    TabCircle(isColored: false),
    TabCircle(isColored: false),
    TabCircle(isColored: false),
  ];

  AddTaskTabBarViewState child;

  Widget getBackBtnOffWidget(){
    return Column(
      children: [
        Row(
          children: [
            Icon(Icons.arrow_back_ios, color: Colors.grey),
            SizedBox(width: 5,),
            Text(Strings.shared.backWordCPSLock, style: TextStyle(fontSize: 20, color: Colors.grey)),
          ],
        ),
      ],
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
    );
  }
  Widget getBackBtnOnWidget(){
    return Column(
      children: [
        Row(
          children: [
            Icon(Icons.arrow_back_ios, color: Color.fromRGBO(53, 104, 89, 1)),
            SizedBox(width: 5,),
            Text(Strings.shared.backWordCPSLock, style: TextStyle(fontSize: 20, color: Color.fromRGBO(53, 104, 89, 1))),
          ],
        ),
      ],
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
    );
  }
  Widget getNextBtnOnWidget(){
    return Column(
      children: [
        SizedBox(height: 10,),
        Text(Strings.shared.nextWord, style: TextStyle(fontSize: 16, color: Colors.grey)),
        Row(
          children: [
            Text("${Strings.shared.buttonArrowNextStepBtn} ${TaskProcess.shared.currentSlide + 1}", style: TextStyle(fontSize: 20, color: Color.fromRGBO(53, 104, 89, 1))),
            SizedBox(width: 5,),
            Icon(Icons.arrow_forward_ios, color: Color.fromRGBO(53, 104, 89, 1),)
          ],
        ),
      ],
    );
  }
  Widget getFinishBtnWidget(){
    return Container(
      child: Text(Strings.shared.addTaskStep4CreateTaskBtn,
          style: TextStyle(fontSize: 22, color: Colors.white)
      ), // text
      color: Colors.blue,
      padding: const EdgeInsets.only(top: 8, bottom: 8, right: 12, left: 12),
    );
  }

  Widget nextBtnWidget = Column();
  Widget backBtnWidget = Column();

  String titleOfPage = Strings.shared.addNewTaskTitle;

  @override
  AddTaskTabBarViewState createState() => AddTaskTabBarViewState();
}

class AddTaskTabBarViewState extends State<AddTaskTabBarView> with TickerProviderStateMixin,AutomaticKeepAliveClientMixin<AddTaskTabBarView> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  bool get wantKeepAlive => true;

  final GlobalKey _scaffold = GlobalKey();

  void changeCurrentSlideTo({int pid = 0}){
    setState(() {
      TaskProcess.shared.currentSlide = pid;
      print("THIS PAGE:  ${TaskProcess.shared.currentSlide}   ${pid}");
      this.widget.tabCircles[0].isColored = false;
      this.widget.tabCircles[1].isColored = false;
      this.widget.tabCircles[2].isColored = false;
      this.widget.tabCircles[3].isColored = false;

      this.widget.tabCircles[pid].isColored = true;
      switch(pid){
        case 0:
          this.widget.backBtnWidget = this.widget.getBackBtnOffWidget();
          this.widget.nextBtnWidget = this.widget.getNextBtnOnWidget();
          this.widget.titleOfPage = Strings.shared.stepOneContainerTitleAddNewTask;
          break;
        case 1:
          this.widget.backBtnWidget = this.widget.getBackBtnOnWidget();
          this.widget.nextBtnWidget = this.widget.getNextBtnOnWidget();
          this.widget.titleOfPage = Strings.shared.step2TitleAddTask;
          break;
        case 2:
          this.widget.backBtnWidget = this.widget.getBackBtnOnWidget();
          this.widget.nextBtnWidget = this.widget.getNextBtnOnWidget();
          this.widget.titleOfPage = Strings.shared.step3TitleAddTask;
          break;
        case 3:
          this.widget.backBtnWidget = this.widget.getBackBtnOnWidget();
          this.widget.nextBtnWidget = this.widget.getFinishBtnWidget();
          this.widget.titleOfPage = Strings.shared.step4TitleAddTask;
          break;
        default:
          this.widget.backBtnWidget = this.widget.getBackBtnOffWidget();
          this.widget.nextBtnWidget = this.widget.getNextBtnOnWidget();
          this.widget.titleOfPage = Strings.shared.stepOneContainerTitleAddNewTask;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    this.widget.child = this;
    TaskProcess.shared.parentClass = this.widget;
    if(this.widget.isFirstInit){
      print("STAAAAART");
      TaskProcess.shared.mainTbc = TabController(length: 4, vsync: this);
      changeCurrentSlideTo(pid: TaskProcess.shared.currentSlide);
      this.widget.isFirstInit = false;
    }

    return new Scaffold(
      appBar: AppBar(
        title: Text(this.widget.titleOfPage, style: TextStyle(color: Color.fromRGBO(53, 104, 89, 1), fontSize: 22)),
        backgroundColor: Colors.white,
        centerTitle: true,
        leading: IconButton(
          onPressed: (){
            TaskProcess.shared.delete();
            Navigator.of(context).pop();
          },
          icon: Icon(Icons.arrow_back_ios),
        ),
        elevation: 0,
        iconTheme: IconThemeData(
            color: Color.fromRGBO(0, 0, 0, 1)
        ),
      ),
      backgroundColor: Colors.white,
      body: Stack(
        children: <Widget>[
          TabBarView(
            children: [TaskProcess.shared.taskP1,TaskProcess.shared.taskP2, TaskProcess.shared.taskP3, TaskProcess.shared.taskP4],
            controller: TaskProcess.shared.mainTbc,
            physics: NeverScrollableScrollPhysics(),
          ),
        ],
      ),
      bottomSheet: Container(height: 70,
        decoration: BoxDecoration(
            border: Border(top: BorderSide(color: Colors.grey,width: 1))
        ),
        child: Row(
          children: [
            FlatButton(
              child: this.widget.backBtnWidget,
              onPressed: (){
                if(TaskProcess.shared.currentSlide != 0){
                  TaskProcess.shared.mainTbc.animateTo(TaskProcess.shared.currentSlide - 1,duration: Duration(milliseconds: 150));
                  changeCurrentSlideTo(pid: TaskProcess.shared.currentSlide - 1);
                }
                // back
              },
            ),
            Container(
              child: Row(
                children: this.widget.tabCircles,
              ),
            ),
            FlatButton(
              child: this.widget.nextBtnWidget,
              onPressed: (){
                switch(TaskProcess.shared.currentSlide + 1){
                  case 1:
                    TaskProcess.shared.taskP1.child.nextTabFunc((st){
                      TaskProcess.shared.mainTbc.animateTo(1,duration: Duration(milliseconds: 150));
                      changeCurrentSlideTo(pid: 1);
                    });
                    break;
                  case 2:
                    TaskProcess.shared.taskP2.child.nextTabFunc((st){
                      TaskProcess.shared.mainTbc.animateTo(2,duration: Duration(milliseconds: 150));
                      changeCurrentSlideTo(pid: 2);
                    });
                    break;
                  case 3:
                    TaskProcess.shared.taskP3.child.nextTabFunc((st){
                      TaskProcess.shared.mainTbc.animateTo(3,duration: Duration(milliseconds: 150));
                      changeCurrentSlideTo(pid: 3);
                    });
                    break;
                  case 4:
                    TaskProcess.shared.taskP4.child.nextTabFunc((st){});
                    break;
                }
                // next
              },
            ),
          ],
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
        ),),
      key: _scaffold,
    );
  }
}
