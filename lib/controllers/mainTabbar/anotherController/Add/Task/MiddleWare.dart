import 'package:flutter/material.dart';
import 'package:goal_meter/views/AddTask/CircularDayPicker.dart';
import 'package:goal_meter/models/Statics/Strings.dart';
import 'TaskProcess.dart';
import 'package:goal_meter/views/AddTask/CustomRadioButton.dart';

class MiddleWare
{
//  static MiddleWare shared = MiddleWare();
//  _MiddleWare(){
//    shared = MiddleWare();
//  }

//  void delete(){
//    _MiddleWare();
//  }

  MiddleWare(){}

  CustomRadioButton oneTimeRadioButton;
  CustomRadioButton repeatingTimeRadioButton;

  CustomRadioButton dailyRadioButton;
  CustomRadioButton weeklyRadioButton;
  CustomRadioButton monthlyTimeRadioButton;

  List myActivities;
  String resultOfMonthlyDays = "";
  List<int> selectedMonthlyBox = [];
  int unitRadioValueRepeatinOrOnetime = 0;
  int unitRadioValueDailyWeeklyMonthlyChanged = 0;
  Stack stackRowTwo;
  Stack stackRowThree = Stack();
  double screenWidth = 0;
  TextEditingController txtControllerHoursPerWeek = TextEditingController();
  TextEditingController txtControllerHoursPerMonth = TextEditingController();
  Widget expandedWeeklyMoreOption = Row();
  bool isSpecificWeeklyDaysTrue = false;
  Widget expandedMonthlyMoreOption = Row();
  bool isSpecificMonthlyDaysTrue = false;
  Widget listOfMonthlySpecificDay = Container();

  List<CircularDayPicker> weeklyDaysTop = [
    new CircularDayPicker(dayName: Strings.shared.mondayMon, isChecked: true),
    new CircularDayPicker(dayName: Strings.shared.tuesdayTue, isChecked: false),
    new CircularDayPicker(dayName: Strings.shared.wednesdayWed, isChecked: false),
    new CircularDayPicker(dayName: Strings.shared.thursdayThu, isChecked: false),
    new CircularDayPicker(dayName: Strings.shared.fridayFri, isChecked: false)
  ];
  List<CircularDayPicker> weeklyDaysBottom = [
    new CircularDayPicker(dayName: Strings.shared.saturdaySat, isChecked: false),
    new CircularDayPicker(dayName: Strings.shared.sundaySun, isChecked: false),
  ];

  List<CircularDayPicker> dailyDaysTop = [
    new CircularDayPicker(dayName: Strings.shared.mondayMon, isChecked: true),
    new CircularDayPicker(dayName: Strings.shared.tuesdayTue, isChecked: false),
    new CircularDayPicker(dayName: Strings.shared.wednesdayWed, isChecked: false),
    new CircularDayPicker(dayName: Strings.shared.thursdayThu, isChecked: false),
    new CircularDayPicker(dayName: Strings.shared.fridayFri, isChecked: false)
  ];
  List<CircularDayPicker> dailyDaysBottom = [
    new CircularDayPicker(dayName: Strings.shared.saturdaySat, isChecked: false),
    new CircularDayPicker(dayName: Strings.shared.sundaySun, isChecked: false),
  ];
}

class RefreshTaskProcess
{
  MiddleWare md = MiddleWare();
  RefreshTaskProcess(MiddleWare obj){
    this.md = obj;
  }

  void refreshTaskProcess(){

    if(md.unitRadioValueRepeatinOrOnetime == 0){
      TaskProcess.shared.taskType = "oneTime";
      TaskProcess.shared.repeatedType = "";
      TaskProcess.shared.specificDays = "";
      TaskProcess.shared.hoursPerMonth = 0;
      TaskProcess.shared.hoursPerWeek = 0;
    }else{
      TaskProcess.shared.taskType = "repeating";
      switch(md.unitRadioValueDailyWeeklyMonthlyChanged){
        case 0:
          TaskProcess.shared.repeatedType = "daily";
          TaskProcess.shared.specificDays = this._refreshTaskProcessDailyWhileLoopOne();
          TaskProcess.shared.hoursPerMonth = 0;
          TaskProcess.shared.hoursPerWeek = 0;
          break;
        case 1:
          TaskProcess.shared.repeatedType = "weekly";
          if(md.isSpecificWeeklyDaysTrue){
            TaskProcess.shared.specificDays = this._refreshTaskProcessWeeklyWhileLoopOne();
          }else{
            TaskProcess.shared.specificDays = "";
          }
          TaskProcess.shared.hoursPerMonth = 0;
          TaskProcess.shared.hoursPerWeek = 0;
          if(md.txtControllerHoursPerWeek.text.isNotEmpty){
            TaskProcess.shared.hoursPerWeek = int.parse(md.txtControllerHoursPerWeek.text);
          }
          break;
        case 2:
          TaskProcess.shared.repeatedType = "monthly";
          if(md.isSpecificMonthlyDaysTrue){
            TaskProcess.shared.specificDays = md.resultOfMonthlyDays;
          }else{
            TaskProcess.shared.specificDays = "";
          }
          TaskProcess.shared.hoursPerMonth = 0;
          TaskProcess.shared.hoursPerWeek = 0;
          if(md.txtControllerHoursPerMonth.text.isNotEmpty){
            TaskProcess.shared.hoursPerMonth = int.parse(md.txtControllerHoursPerMonth.text);
          }
          break;
      }
    }
  }// refresh Task Process


  String _refreshTaskProcessDailyWhileLoopOne(){
    String days = "";
    int i = 0;
    String DayName = "";

    if(md.isSpecificWeeklyDaysTrue){
      TaskProcess.shared.isSpecificWeekly = true;
    }
    if(md.isSpecificMonthlyDaysTrue){
      TaskProcess.shared.isSpecificMonthly = true;
    }

    while(i != md.dailyDaysTop.length){
      switch(i){
        case 0:
          DayName = "mon";
          break;
        case 1:
          DayName = "tue";
          break;
        case 2:
          DayName = "wed";
          break;
        case 3:
          DayName = "thu";
          break;
        case 4:
          DayName = "fri";
          break;
      }
      if(md.dailyDaysTop[i].isChecked){
        if(days != ""){
          days+=",${DayName}";
        }
        else{
          days = "${DayName}";
        }
      }
      i++;
    }// while loop 1
    return _refreshTaskProcessDailyWhileLoopTwo(days);
  }
  String _refreshTaskProcessDailyWhileLoopTwo(String days){
    String totalDays = "${days}";
    int i = 0;
    String DayName = "";
    while(i != md.dailyDaysBottom.length){
      switch(i){
        case 0:
          DayName = "sat";
          break;
        case 1:
          DayName = "sun";
          break;
      }
      if(md.dailyDaysBottom[i].isChecked){
        if(totalDays != ""){
          totalDays+=",${DayName}";
        }
        else{
          totalDays = "${DayName}";
        }
      }
      i++;
    }// while loop 1
    return totalDays;
  }

  String _refreshTaskProcessWeeklyWhileLoopOne(){
    String days = "";
    int i = 0;
    String DayName = "";
    while(i != md.weeklyDaysTop.length){
      switch(i){
        case 0:
          DayName = "mon";
          break;
        case 1:
          DayName = "tue";
          break;
        case 2:
          DayName = "wed";
          break;
        case 3:
          DayName = "thu";
          break;
        case 4:
          DayName = "fri";
          break;
      }
      if(md.weeklyDaysTop[i].isChecked){
        if(days != ""){
          days+=",${DayName}";
        }
        else{
          days = "${DayName}";
        }
      }
      i++;
    }// while loop 1
    return _refreshTaskProcessWeeklyWhileLoopTwo(days);
  }
  String _refreshTaskProcessWeeklyWhileLoopTwo(String days){
    String totalDays = "${days}";
    int i = 0;
    String DayName = "";
    while(i != md.weeklyDaysBottom.length){
      switch(i){
        case 0:
          DayName = "sat";
          break;
        case 1:
          DayName = "sun";
          break;
      }
      if(md.weeklyDaysBottom[i].isChecked){
        if(totalDays != ""){
          totalDays+=",${DayName}";
        }
        else{
          totalDays = "${DayName}";
        }
      }
      i++;
    }// while loop 1
    return totalDays;
  }
}
