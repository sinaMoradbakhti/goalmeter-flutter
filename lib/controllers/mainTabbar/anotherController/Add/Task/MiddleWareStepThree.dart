import 'package:flutter/material.dart';
import 'package:goal_meter/models/Statics/Strings.dart';

class MiddleWareStepThree
{
  MiddleWareStepThree(){}

  String endDateTarget = "forever";
  double screenWidth = 0;
  String txtBtnDatePicker = "";
  String txtBtnEndDatePicker = "";
  String txtBtnStartTimePicker = "00:00";
  String txtBtnendTimePicker = "00:00";
  bool isEndTimeNexDay = false;
  bool isSpecificTimeOn = false;

  Widget endDateWidget = Text(Strings.shared.forEverCamelCap,style: TextStyle(fontSize: 17));

  String convertWeekDayToStr(int weekday){
    switch(weekday){
      case 1:
        return "Mon";
      case 2:
        return "Tue";
      case 3:
        return "Wed";
      case 4:
        return "Thu";
      case 5:
        return "Fri";
      case 6:
        return "Sat";
      case 7:
        return "Sun";
      default:
        return "Nan";
    }
  }
  String convertMonthToStr(int month){
    switch(month){
      case 1:
        return "Jan";
      case 2:
        return "Feb";
      case 3:
        return "Mar";
      case 4:
        return "Apr";
      case 5:
        return "May";
      case 6:
        return "Jun";
      case 7:
        return "Jul";
      case 8:
        return "Aug";
      case 9:
        return "Sep";
      case 10:
        return "Oct";
      case 11:
        return "Nov";
      case 12:
        return "Dec";
      default:
        return "Nan";
    }
  }
}