import 'package:flutter/material.dart';
import 'package:goal_meter/controllers/mainTabbar/anotherController/Add/Task/AddTask.dart';
import 'package:goal_meter/controllers/mainTabbar/anotherController/Add/Task/AddTaskStepFour.dart';
import 'package:goal_meter/controllers/mainTabbar/anotherController/Add/Task/AddTaskStepThree.dart';
import 'package:goal_meter/controllers/mainTabbar/anotherController/Add/Task/AddTaskStepTwo.dart';
import 'AddTaskTabBarView.dart';

class TaskProcess
{
  AddTaskTabBarView parentClass;
  int currentSlide = 0;
  TabController mainTbc;
  static TaskProcess shared = new TaskProcess();

  AddTask taskP1 = new AddTask();
  AddTaskStepTwo taskP2 = new AddTaskStepTwo();
  AddTaskStepThree taskP3 = new AddTaskStepThree();
  AddTaskStepFour taskP4 = new AddTaskStepFour();

  void delete(){
    TaskProcess.shared =  new TaskProcess();
  }
  _TaskProcess(){}

  // Step 1
  String taskName = "";
  String goalId = "";
  // Step 2
  String taskType = "";
  String repeatedType = "";
  String specificDays = "";
  int hoursPerWeek = 0;
  int hoursPerMonth = 0;
  bool isSpecificWeekly = false;
  bool isSpecificMonthly = false;
  // Step 3
  DateTime startDate = DateTime.now();
  DateTime endDate = DateTime.now();
  bool isEndDateForEver = true;
  bool isEndDateOn = false;
  TimeOfDay startTime = TimeOfDay.now();
  TimeOfDay endTime = TimeOfDay.now();
  bool isEndOnNexDay = false;
  bool isTimeOn = false;
  // Step 4
  bool recieveNotification = false;
  int minutesToRecieveNotification = 15;


  void info(){
    print("taskName: " + TaskProcess.shared.taskName);
    print("goalId: " + TaskProcess.shared.goalId);
    print("taskType: " + TaskProcess.shared.taskType);
    print("repeatedType: " + TaskProcess.shared.repeatedType);
    print("specificDays: " + TaskProcess.shared.specificDays);
    print("hoursPerWeek: " + "${TaskProcess.shared.hoursPerWeek}");
    print("hoursPerMonth: " + "${TaskProcess.shared.hoursPerMonth}");

    print("isTimeOn: " + "${TaskProcess.shared.isTimeOn}");
    print("startDate: " + "${TaskProcess.shared.startDate}");
    print("endDate: " + "${TaskProcess.shared.endDate}");
    print("isEndDateForEver: " + "${TaskProcess.shared.isEndDateForEver}");
    print("isEndDateOn: " + "${TaskProcess.shared.isEndDateOn}");
    print("startTime: " + "${TaskProcess.shared.startTime}");
    print("endTime: " + "${TaskProcess.shared.endTime}");
    print("isEndOnNexDay: " + "${TaskProcess.shared.isEndOnNexDay}");

    print("recieveNotification: " + "${TaskProcess.shared.recieveNotification}");
    print("minutesToRecieveNotification: " + "${TaskProcess.shared.minutesToRecieveNotification}");
  }
}