import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:goal_meter/main.dart';
import 'package:goal_meter/models/Goal/GoalObject.dart';
import 'package:goal_meter/models/Statics/Strings.dart';
import 'package:goal_meter/models/Statics/Statics.dart';
import 'MiddleWare.dart';
import 'package:goal_meter/views/myGoals/MyGoalBoxWidget.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:goal_meter/models/Goal/MyGoalObject.dart';
import 'SingleGoal/SingleGoalVC.dart';
import 'package:goal_meter/models/server/CatchDataFromServer.dart';
import 'package:progress_hud/progress_hud.dart';

class GoalsVC extends StatefulWidget {

  bool isFirstLunch = true;

  GoalsVCState myChild;
  List<MyGoalWidget> _myGoalsWidget = [];

  @override
  GoalsVCState createState()=>GoalsVCState();
}

class GoalsVCState extends State<GoalsVC> {

  ProgressHUD _progressHUD;
  bool _loading = false;

  GlobalKey _scaffold = GlobalKey();

  @override
  void initState() {
    super.initState();
    _progressHUD = new ProgressHUD(
      backgroundColor: Colors.black12,
      color: Colors.white,
      containerColor: Colors.blue,
      borderRadius: 5.0,
      text: 'Loading...',
      loading: this._loading,
    );
  }

  void showProgressHUD() {
    setState(() {
      _progressHUD.state.show();
      _loading = true;
    });
  }
  void dismissProgressHUD() {
    setState(() {
      _progressHUD.state.dismiss();
      _loading = false;
    });
  }
  void goalClicked()
  {
    Navigator.of(myMainTabBar.myChild.scaffold.currentContext).push(new MaterialPageRoute(builder: (context) => new SingleGoalVC()));
  }
  void goalMoreClicked(String index,String goal)
  {
    showProgressHUD();
    if(index == "Delete"){
      // Delete Command
      if(goal != ""){
        Firestore.instance.collection('userGoals').document(goal).delete().then((val){
          this.widget.isFirstLunch = true;
          if (this.mounted) {
            setState(() {
              print("Refreshing Goals...");
              this.refreshListView();
            });
          }else{
            print("State unmount");
            dismissProgressHUD();
          }
        });
      }
    }

  }
  void clickedAddGoalBtn()
  {
    print("CLICKED..... Hereee");
    myMainTabBar.myChild.clickToChangeMenu(4);
  }
  void refreshListView(){

    // refresh GoalsVC
    final CatchDataFromFireStore _server = CatchDataFromFireStore();
    _server.refreshAllGoals(onComplete: (){
      List<MyGoalWidget> myList = [];
      for(int i = 0; i < Statics.shared.data.myGoals.length; i++){
        myList.add(
            MyGoalWidget(
              goal: Statics.shared.data.myGoals[i],
              isYourGoal: true,
              onPressGoal: (){
                this.goalClicked();
              },
              goalMoreClicked: (){
                this.goalMoreClicked("Delete", Statics.shared.data.myGoals[i].gId);
              },
            )// MyGoalWidget
        );
      }// for loop

      setState(() {
        this.widget._myGoalsWidget = myList;
      }); // set state
      dismissProgressHUD();

      this.widget._myGoalsWidget.add(MyGoalWidget(goal: MyGoalObject("",false,"",false,"",GoalObject("","",0,0)),isYourGoal: false
        ,
        onPressAddGoalBtn: (){
          this.clickedAddGoalBtn();
        },
      ));
    });

  }// refresh goals method

  @override
  void dispose() {
    print("DISPOSEEEE GOALS ....");
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    this.widget.myChild = this;
    MiddleWare.shared.windowWidth = MediaQuery.of(context).size.width;
    MiddleWare.shared.windowHeight = MediaQuery.of(context).size.height;
    if(this.widget.isFirstLunch){
      this.widget.isFirstLunch = false;

      this.widget._myGoalsWidget = [];
      for(int i = 0; i < Statics.shared.data.myGoals.length; i++){
        this.widget._myGoalsWidget.add(
            MyGoalWidget(
                goal: Statics.shared.data.myGoals[i],
                isYourGoal: true,
                onPressGoal: (){
                  this.goalClicked();
                },
                goalMoreClicked: (){
                  this.goalMoreClicked("Delete", Statics.shared.data.myGoals[i].gId);
                },
            )// MyGoalWidget
        );
      }// for loop
      this.widget._myGoalsWidget.add(MyGoalWidget(goal: MyGoalObject("",false,"",false,"",GoalObject("","",0,0)),isYourGoal: false
      ,
        onPressAddGoalBtn: (){
          this.clickedAddGoalBtn();
        },
      ));
    }

    return Scaffold(
        appBar: AppBar(
          title: Text(Strings.shared.goalsControllerTitle, style: TextStyle(color: Color.fromRGBO(53, 104, 89, 1), fontSize: 22)),
          backgroundColor: Colors.white,
          centerTitle: true,
          elevation: 0,
          iconTheme: IconThemeData(
              color: Color.fromRGBO(0, 0, 0, 1)
          ),
        ),
        backgroundColor: Colors.white,
        body: Builder(
          builder: (mainContext) =>
              Container(
                margin: const EdgeInsets.only(left: 16, right: 16, top: 16, bottom: 16),
                child: Stack(
                  children: [
                    ListView(
                      children: this.widget._myGoalsWidget,
                    ),
                    Container(child: _progressHUD,),
                  ],
                ),
              ),
        ),
        key: _scaffold,
    );
  }
}
