import 'package:flutter/material.dart';
import 'package:goal_meter/models/Statics/Statics.dart';
import 'package:goal_meter/models/Statics/Strings.dart';

class MiddleWare
{
  static MiddleWare shared = MiddleWare();
  _MiddleWare(){}

  double windowWidth = 0;
  double windowHeight = 0;

  // vars that can be edited by user info
  List<RadioListTile> unitRadioList = [];
  int myGoalLevel = 2;
  // vars that can be edited by user info

  int unitRadioValue = 0;
  String progressTitle = Strings.shared.totalProgress;
  String progressValue = "45 kilometers";
  String levelTitle = Strings.shared.progressLevelIntermediate;
  String levelValue = "0 hours(0%)";
}