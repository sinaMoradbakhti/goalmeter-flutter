import 'package:flutter/material.dart';
import 'package:goal_meter/models/Statics/Strings.dart';
import 'package:goal_meter/models/Statics/Statics.dart';

class SingleGoalVC extends StatefulWidget {
  @override
  _SingleGoalVCState createState() => _SingleGoalVCState();
}

class _SingleGoalVCState extends State<SingleGoalVC> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: AppBar(
          title: Text("first Goal", style: TextStyle(color: Colors.white, fontSize: 22)),
          backgroundColor: Color.fromRGBO(0, 0, 0, 0.5),
          actions: [
            Container(
              child: IconButton(
                icon: Icon(Icons.more_vert),
                onPressed: (){},
                iconSize: 25,
              )
            )
          ],
          elevation: 0,
          iconTheme: IconThemeData(
              color: Color.fromRGBO(255, 255, 255, 1),
          ),
        ),
        backgroundColor: Colors.white,
        body: Builder(
          builder: (mainContext) =>
              Container(
                child: Container(
                  child: ListView(
                    children: [
                      Container(
                        height: 120,
                        color: Colors.red,
                      )
                    ],
                    padding: const EdgeInsets.all(0),
                  ),
                ),
              ),
        )
    );
  }
}
