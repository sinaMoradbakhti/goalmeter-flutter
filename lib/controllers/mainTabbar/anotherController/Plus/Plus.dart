import 'package:flutter/material.dart';

class PlusVC extends StatefulWidget {
  @override
  PlusVCState createState() => PlusVCState();
}

class PlusVCState extends State<PlusVC> {

  GlobalKey _scaffold = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: Container(),
      appBar: AppBar(
        title: Text("Plus", style: TextStyle(color: Color.fromRGBO(53, 104, 89, 1), fontSize: 22)),
        backgroundColor: Colors.white,
        centerTitle: true,
        elevation: 0,
        iconTheme: IconThemeData(
            color: Color.fromRGBO(0, 0, 0, 1)
        ),
      ),
      key: _scaffold,
    );
  }
}
