import 'package:flutter/material.dart';
import 'package:goal_meter/controllers/mainTabbar/MainTabBar.dart';
import 'package:goal_meter/main.dart';
import 'package:goal_meter/models/Statics/Statics.dart';
import 'package:goal_meter/models/server/CatchDataFromServer.dart';
import 'dart:io';

class SplashScreen extends StatefulWidget {

  String caption1 = "please wait";
  String subTitle1 = "initializing your data";
  Widget recheckBtn = Container();
  bool isFirstInit = true;

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  final _server = new CatchDataFromFireStore();

  void _push(BuildContext ctx){
    myMainTabBar = new MainTabBar();
    Navigator.of(ctx).pushReplacement(
        new MaterialPageRoute(builder: (mainContext) => myMainTabBar)
    );
  }
  void _startInitializing(BuildContext ctx){
    setState(() {
      this.widget.subTitle1 = "initializing your goals";
    });
    _server.refreshAllGoals(onComplete: (){
      setState(() {
        this.widget.subTitle1 = "initializing your tasks";
      });
      _server.getAllTasks(onComplete: (){
        var future = new Future.delayed(const Duration(seconds: 2),(){
          setState(() {
            this.widget.subTitle1 = "initializing completed";
          });
        });
        future.whenComplete((){
          setState(() {
            this.widget.subTitle1 = "loading app data";
            this.widget.caption1 = "Welcome";
          });

          print("GOALS: ${Statics.shared.data.myGoals.length}");
          print("TASKS: ${Statics.shared.data.myTasks.length}");
          _push(ctx);
        });

      });
    });
  }

  void _checkInternetAccess(BuildContext ctx) async {
    try {
      setState(() {
        this.widget.recheckBtn = Container();
        this.widget.caption1 = "Please Wait";
        this.widget.subTitle1 = "initializing your connection";
      });
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        print('network connected');
        _startInitializing(ctx);
      }
    } on SocketException catch (_) {
      print('network not connected');
      setState(() {
        this.widget.caption1 = "Connecting Failed";
        this.widget.subTitle1 = "please check your network connection";
        this.widget.recheckBtn = Center(child: IconButton(
          icon: Icon(Icons.refresh, color: Colors.black,),
          onPressed: (){
            _checkInternetAccess(ctx);
            this.widget.isFirstInit = false;
          },
        ));
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    if(this.widget.isFirstInit){
      _checkInternetAccess(context);
      this.widget.isFirstInit = false;
    }
    return Scaffold(
      body: Builder(
        builder: (mainContext) =>
            Container(
              child: Column(
                children: [
                  Container(
                    height: MediaQuery.of(mainContext).size.height/1.3,
                    width: MediaQuery.of(mainContext).size.width,
                    child: Image.asset("Resources/Images/get_started_bg_img.jpg", fit: BoxFit.fitWidth),
                  ),
                  Container(child:
                    Column(
                      children: [
                        Text(this.widget.caption1, style: TextStyle(color: Colors.black, fontSize: 24),),
                        SizedBox(height: 10,),
                        Text(this.widget.subTitle1, style: TextStyle(color: Colors.black, fontSize: 16, fontWeight: FontWeight.w200),),
                        SizedBox(height: 10,),
                        this.widget.recheckBtn
                      ],
                    )
                  ),
                ],
              ),
              color: Colors.white,
            ),
      ), // Body
      backgroundColor: Colors.white,
    );
  }
}
