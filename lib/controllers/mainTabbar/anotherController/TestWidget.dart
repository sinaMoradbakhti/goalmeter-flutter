import 'package:flutter/material.dart';
import 'package:goal_meter/views/GoalProgressWidget/GoalProgressWidget.dart';

class TestWidget extends StatefulWidget {
  @override
  _TestWidgetState createState() => _TestWidgetState();
}

class _TestWidgetState extends State<TestWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Test Widget", style: TextStyle(color: Color.fromRGBO(53, 104, 89, 1), fontSize: 22)),
        backgroundColor: Colors.white,
        centerTitle: true,
        elevation: 0,
        iconTheme: IconThemeData(
            color: Color.fromRGBO(0, 0, 0, 1)
        ),
      ),
      backgroundColor: Colors.white,
      body: Builder(
        builder: (mainContext) =>
            Container(
              margin: const EdgeInsets.only(left: 16, right: 16, top: 16, bottom: 16),
              child: GoalProgressWidget(width: 400,height: 40,progressValuePercent: 30),
            ),
      ),
    );
  }
}
