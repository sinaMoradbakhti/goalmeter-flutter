import 'package:flutter/material.dart';
import 'package:goal_meter/views/AddTask/TaskWidget.dart';
import 'package:goal_meter/models/Task/TaskObject.dart';

class MiddleWare
{
  MiddleWare(){}

  Widget loadingWidget = Container();

  double screenWidth = 0;

  List<DropdownMenuItem> myGoals = [];
  List<TaskObject> myTasks = [];
  List<TaskObject> oneTimeTasks = [];
  List<TaskObject> myDailyTasks = [];
  List<TaskObject> myWeeklyTasks = [];
  List<TaskObject> myMonthlyTasks = [];

  List<Widget> myTasksWidgets = [];
  TaskWidget goalHint;
}