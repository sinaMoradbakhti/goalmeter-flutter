import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:goal_meter/controllers/mainTabbar/anotherController/TodoList/MiddleWare.dart';
import 'package:goal_meter/models/Goal/GoalObject.dart';
import 'package:goal_meter/models/Task/TaskObject.dart';
import 'MiddleWare.dart';
import 'package:goal_meter/views/AddTask/TaskWidget.dart';
import 'package:goal_meter/models/Goal/MyGoalObject.dart';
import 'package:goal_meter/models/Statics/Statics.dart';
import 'package:goal_meter/models/Statics/Strings.dart';
import 'package:goal_meter/views/TodoList/TodoListItem.dart';
import 'package:goal_meter/views/dialogs/PopUpDialogGoalSelector.dart';

class ToDoListVC extends StatefulWidget {

  final MiddleWare md = MiddleWare();

  MyGoalObject selectedGoal;
  String selectedGoalId = "";
  PopUpDialogGoalSelector popUpSelectGoal;
  ToDoListVCState myChild;
  bool isFirstLunched = true;

  void logTasks(){
    print("=================== [One Time Tasks] ===================");
    for(int i = 0; i < this.md.oneTimeTasks.length; i++){
      print("item(${i}) : ${this.md.oneTimeTasks[i].taskName} --> ${this.md.oneTimeTasks[i].startDate.toDate().toString()}");
    }
    print("=================== [Daily Tasks] ===================");
    for(int i = 0; i < this.md.myDailyTasks.length; i++){
      print("item(${i}) : ${this.md.myDailyTasks[i].taskName} --> ${this.md.myDailyTasks[i].startDate.toDate().toString()}");
    }
    print("=================== [Weekly Tasks] ===================");
    for(int i = 0; i < this.md.myWeeklyTasks.length; i++){
      print("item(${i}) : ${this.md.myWeeklyTasks[i].taskName} --> ${this.md.myWeeklyTasks[i].startDate.toDate().toString()}");
    }
    print("=================== [Monthly Tasks] ===================");
    for(int i = 0; i < this.md.myMonthlyTasks.length; i++){
      print("item(${i}) : ${this.md.myMonthlyTasks[i].taskName} --> ${this.md.myMonthlyTasks[i].startDate.toDate().toString()}");
    }
  }// log task method
  GoalObject findGoal({String goalID = ""}){
    GoalObject myTempGoal;
    for(int z = 0; z<Statics.shared.data.myGoals.length;z++){
      if(Statics.shared.data.myGoals[z].gId == goalID){
        myTempGoal = Statics.shared.data.myGoals[z].goal;
      }
    }
    return myTempGoal;
  }
  List<Widget> showMonthlyTasks(){
    List<Widget> monthlyList = [];
    for(int i = 0; i < this.md.myMonthlyTasks.length; i++)
    {
      TaskObject task = this.md.myMonthlyTasks[i];
      GoalObject myTempGoal = findGoal(goalID: task.goalId);
      monthlyList.add(TodoListItem(myGoal: myTempGoal,myTask: task));
      monthlyList.add(SizedBox(height: 5));
    }// monthly Tasks loop
    return monthlyList;
  }
  List<Widget> showWeeklyTasks(){
    List<Widget> monthlyList = [];
    for(int i = 0; i < this.md.myWeeklyTasks.length; i++)
    {
      TaskObject task = this.md.myWeeklyTasks[i];
      GoalObject myTempGoal = findGoal(goalID: task.goalId);
      monthlyList.add(TodoListItem(myGoal: myTempGoal,myTask: task));
      monthlyList.add(SizedBox(height: 5));
    }// monthly Tasks loop
    return monthlyList;
  }
  void showTasks(){
    List<Widget> myList = [];
    //myChild.refreshTaskWidgets(widgets: []);

    int oldYear = 0;
    int oldMonth = 0;
    int currentYear = 2019;
    int currentMonth = 1;
    bool dateChanged = false;
    for(int i = 0; i < this.md.myTasks.length; i++)
    {
      dateChanged = false;
      TaskObject task = this.md.myTasks[i];
      currentYear = task.startDate.toDate().year;
      currentMonth = task.startDate.toDate().month;

      if(i == 0){
        oldYear = currentYear;
        oldMonth = currentMonth;
        dateChanged = true;
      }
      else{
        if(oldYear != currentYear || oldMonth != currentMonth){
          oldYear = currentYear;
          oldMonth = currentMonth;
          dateChanged = true;
        }
      }

      if(dateChanged){
        int totalNum = 0;
        int doneNum = 0;

        TodoListCoverItem cItem = TodoListCoverItem(year: currentYear,month: currentMonth,imageUrl: getSeasonCover(month: currentMonth),totalTasks: 60,taskDoneCount: 15,);
        myList.add(cItem);
        myList.add(SizedBox(height: 10));
        /* Monthly */
        List<Widget> monthList = showMonthlyTasks();
        if(monthList.length > 0){
          myList.add(TodoListWeeklySplitorItem(title: "monthly"));
          myList.add(TodoListGroupedBox(chldren: monthList,));
          myList.add(SizedBox(height: 5));
        }
        /* Monthly */

        /* Weekly */
        String monthName = this.monthName(month: currentMonth);
        String today = DateTime.now().day.toString() + " $monthName";
        String todayUntilEndWeek = (DateTime.now().day + 7).toString() + " $monthName";
        if(DateTime.now().day + 7 >= 30){
          todayUntilEndWeek = "";
        }
        List<Widget> weekList = showWeeklyTasks();
        if(weekList.length > 0){
          //myList.add(TodoListWeeklySplitorItem(title: "This week ($today ~ $todayUntilEndWeek)"));
          myList.add(TodoListWeeklySplitorItem(title: "weekly"));
          myList.add(TodoListGroupedBox(chldren: weekList,));
          myList.add(SizedBox(height: 40));
        }
        /* Weekly */

        if(this.md.oneTimeTasks.length > 0)
        {
          for(int j = 0; j < this.md.oneTimeTasks.length; j++)
          {
            TaskObject temp = this.md.oneTimeTasks[j];
            if(temp.startDate.toDate().year == task.startDate.toDate().year && temp.startDate.toDate().month == task.startDate.toDate().month){
              int currentDay = temp.startDate.toDate().day;
              String currentDayName = dayName(day: currentDay);

              GoalObject myTempGoal = findGoal(goalID: temp.goalId);
              myList.add(TodoListItem(day: currentDay,dayName: currentDayName,myGoal: myTempGoal,myTask: temp));

              totalNum += 1;
              //doneNum += 1;
            }
          }// loop
        }// one time tasks

        cItem.totalTasks = totalNum + this.md.myMonthlyTasks.length + this.md.myWeeklyTasks.length;
        cItem.taskDoneCount = doneNum;
      }// date changed
    }// All Tasks loop
    myChild.refreshTaskWidgets(widgets: myList);
  }// show Tasks method
  void initializeTasks({List<TaskObject> myList}){
    print("Starting initializing tasks....");
    this.md.oneTimeTasks = [];
    this.md.myDailyTasks = [];
    this.md.myWeeklyTasks = [];
    this.md.myMonthlyTasks = [];

    this.md.myTasks = myList;
    this.md.myTasks.sort((a,b)=>a.startDate.compareTo(b.startDate));
    //this.md.myTasks.where((item)=>(item.startDate.toDate().hour.toString() == "2018" && item.startDate.toDate().month.toString() == "07"));

    for(int i = 0; i < this.md.myTasks.length; i++){
      TaskObject item = this.md.myTasks[i];
      if(item.taskType == "oneTime"){
        this.md.oneTimeTasks.add(item);
      }// one time task
      else{
        if(item.repeatedType == "daily"){
          this.md.myDailyTasks.add(item);
        }// daily repeating task
        else if(item.repeatedType == "weekly"){
          this.md.myWeeklyTasks.add(item);
        }// weekly repeating task
        else if(item.repeatedType == "monthly"){
          this.md.myMonthlyTasks.add(item);
        }// monthly repeating task
      }// repeating task
    }// for loop
    //this.logTasks();
    this.showTasks();
  }
  String dayName({int day = 1}){
    switch(day){
      case 1:
        return "Mon";
        break;
      case 2:
        return "Tue";
        break;
      case 3:
        return "Wed";
        break;
      case 4:
        return "Thu";
        break;
      case 5:
        return "Fri";
        break;
      case 6:
        return "Sat";
        break;
      case 7:
        return "Sun";
        break;
      default:
        return "Mon";
        break;
    }
  }
  String monthName({int month = 1}){
    switch(month){
      case 1:
        return "Jan";
        break;
      case 2:
        return "Feb";
        break;
      case 3:
        return "Mar";
        break;
      case 4:
        return "Apr";
        break;
      case 5:
        return "May";
        break;
      case 6:
        return "Jun";
        break;
      case 7:
        return "Jul";
        break;
      case 8:
        return "Aug";
        break;
      case 9:
        return "Sep";
        break;
      case 10:
        return "Oct";
        break;
      case 11:
        return "Nov";
        break;
      case 12:
        return "Dec";
        break;
      default:
        return "Jan";
        break;
    }
  }
  String getSeasonCover({int month = 1}){
    List<int> winter = [1,2,3];
    List<int> spring = [4,5,6];
    List<int> summer = [7,8,9];
    List<int> fall = [10,11,12];
    if(winter.where((item)=>item == month).length > 0){
      return "Resources/Images/Seasons/winter.jpg";
    }
    else if(spring.where((item)=>item == month).length > 0){
      return "Resources/Images/Seasons/spring.jpg";
    }
    else if(summer.where((item)=>item == month).length > 0){
      return "Resources/Images/Seasons/summer.jpg";
    }
    else{
      return "Resources/Images/Seasons/fall.jpg";
    }
  }

  @override
  ToDoListVCState createState() => ToDoListVCState();
}

class ToDoListVCState extends State<ToDoListVC> with AutomaticKeepAliveClientMixin {

  @override
  bool get wantKeepAlive => true;

  GlobalKey _scaffold = GlobalKey();
  //Timer _timer;

  void refreshLoadingWidget(){
    setState(() {
      this.widget.md.loadingWidget = Container(child: Center(child: CircularProgressIndicator(),),color: Color.fromRGBO(0, 0, 0, 0.5),);
    });
  }
  void refreshTaskWidgets({List<Widget> widgets}){
    setState(() {
      this.widget.md.myTasksWidgets = widgets;
    });
  }
  void refreshLoading(){
    this.refreshLoadingWidget();
//    _timer = new Timer(const Duration(milliseconds: 2000), () {
//      this.widget.firstInitGoalsList();
//      this.widget.initializeTasks();
//      MiddleWare.shared.loadingWidget = Container();
//    });

  }
  void showPopUpSelectGoal(){
    showDialog(context: context,builder: (BuildContext context) {
      return this.widget.popUpSelectGoal.dialog(
          selectedGoalId: this.widget.selectedGoalId,
          onGetGoalId: (goal){
            if(goal != null){
              setState(() {
                this.widget.selectedGoalId = goal.gId;
                this.widget.selectedGoal = goal;
              });
            }else{
              setState(() {
                this.widget.selectedGoalId = "";
                this.widget.selectedGoal = null;
              });
            }
          },
        myList: Statics.shared.data.myGoals
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    print("TSK COUNT : ${Statics.shared.data.myTasks.length}");
    print("GOL COUNT : ${Statics.shared.data.myGoals.length}");
    this.widget.myChild = this;
    double screenWidth = MediaQuery.of(context).size.width;
    this.widget.md.screenWidth = screenWidth;
    if(this.widget.isFirstLunched){
      this.widget.isFirstLunched = false;
    }

    this.widget.initializeTasks(myList: Statics.shared.data.myTasks);

    double popUpWidth = (MediaQuery.of(context).size.width) - 64;
    double height = MediaQuery.of(context).size.height / 1.3;
    if(MediaQuery.of(context).size.height < 750){
      height = MediaQuery.of(context).size.height - 10;
    }

    this.widget.popUpSelectGoal = PopUpDialogGoalSelector(popUpWidth: popUpWidth,
        onPressOk: (){
          Navigator.pop(_scaffold.currentContext);
        },
        popUpHeight: height,
        selectedGoal: this.widget.selectedGoalId
    );
    this.widget.showTasks();

    Widget myGoalHint = Text(Strings.shared.allGoalCapsLock,
        style: TextStyle(color: Color.fromRGBO(53, 104, 89, 1), fontSize: 17)
    );
    if(this.widget.selectedGoalId != "" && this.widget.selectedGoal != null){
      myGoalHint = Row(
        children: [
          Container(child: Container(),margin: const EdgeInsets.only(right: 5,),decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(40)),
              image: DecorationImage(
                image: new ExactAssetImage(this.widget.selectedGoal.goal.goalImg),
                fit: BoxFit.cover,
              )
          ),
            width: 40,
            height: 40,
          ),
          SizedBox(width: 8,),
          Text(this.widget.selectedGoal.gName,
            style: TextStyle(color: Color.fromRGBO(53, 104, 89, 1), fontSize: 17)),
          ],
        );
    }

    return Scaffold(
      body: Stack(
        children: [
          ListView(
            children: this.widget.md.myTasksWidgets,
            padding: const EdgeInsets.only(top: 100),
          ),
          Container(
            margin: const EdgeInsets.only(top: 0),
            padding: const EdgeInsets.only(top: 31),
            height: 90,
            color: Colors.white,
            child: FlatButton(
              onPressed: (){
                this.showPopUpSelectGoal();
              },
              child: Row(
                children: [
                  myGoalHint,
                  SizedBox(width: 10),
                  Icon(Icons.arrow_drop_down, size: 30, color: Colors.red)
                ],
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
              ), // children
            ),// Flat Button
          ),
          Container(width: MediaQuery.of(context).size.width,height: 1,color: Color.fromRGBO(0, 0, 0, 0.1),
            margin: const EdgeInsets.only(top: 90),
          ),
          this.widget.md.loadingWidget,
        ],
      ),
      key: _scaffold,
    );
  }
}
