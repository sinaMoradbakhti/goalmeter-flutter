import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:goal_meter/controllers/mainTabbar/anotherController/Add/Add.dart';
import 'package:goal_meter/controllers/mainTabbar/anotherController/Add/Task/AddTaskTabBarView.dart';
import 'package:goal_meter/controllers/mainTabbar/anotherController/Goals/Goals.dart';
import 'package:goal_meter/controllers/mainTabbar/anotherController/Plus/Plus.dart';
import 'package:goal_meter/controllers/mainTabbar/anotherController/Setting/Setting.dart';
import 'package:goal_meter/controllers/mainTabbar/anotherController/TestWidget.dart';
import 'package:goal_meter/controllers/mainTabbar/anotherController/TodoList/TodoList.dart';
import 'models/Statics/Statics.dart';
import 'models/Statics/Strings.dart';
import 'models/DropDownWidgets/languages.dart';
import 'controllers/beginController/functions.dart';
import 'controllers/RegisterLoginController/RegisterLoginController.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'controllers/mainTabbar/anotherController/SplashScreen/SplashScreen.dart';
import 'controllers/mainTabbar/MainTabBar.dart';

final GoogleSignIn googleSignIn = GoogleSignIn();
MainTabBar myMainTabBar;

void main() async {

  SharedPreferences prefs = await SharedPreferences.getInstance();
  bool isAppLoggedIn = prefs.getBool(Statics.shared.sharedPreferencesIndexes.isAppLoggedIn) ?? false;
  Statics.shared.app.isAppLogin = isAppLoggedIn;

//  int appLang = prefs.getInt(Statics.shared.sharedPreferencesIndexes.appLang) ?? 0;
//  print("Application Language Index: $appLang");

  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {

    if(!Statics.shared.app.isAppLogin)
      {
        return MaterialApp(
          title: 'GoalMeter',
          theme: ThemeData(
            // This is the theme of your application.
            //
            // Try running your application with "flutter run". You'll see the
            // application has a blue toolbar. Then, without quitting the app, try
            // changing the primarySwatch below to Colors.green and then invoke
            // "hot reload" (press "r" in the console where you ran "flutter run",
            // or simply save your changes to "hot reload" in a Flutter IDE).
            // Notice that the counter didn't reset back to zero; the application
            // is not restarted.
            primarySwatch: Colors.blue,
          ),
          home: MyHomePage(),
        );
      }
    else
      {
        return MaterialApp(
          title: 'GoalMeter',
          theme: ThemeData(
            // This is the theme of your application.
            //
            // Try running your application with "flutter run". You'll see the
            // application has a blue toolbar. Then, without quitting the app, try
            // changing the primarySwatch below to Colors.green and then invoke
            // "hot reload" (press "r" in the console where you ran "flutter run",
            // or simply save your changes to "hot reload" in a Flutter IDE).
            // Notice that the counter didn't reset back to zero; the application
            // is not restarted.
            primarySwatch: Colors.blue,
          ),
          routes: <String, WidgetBuilder> {
            '/AddVC': (BuildContext context) => new AddVC(),
            '/AddTaskBarVC': (BuildContext context) => new AddTaskTabBarView(),
            '/SettingVC' : (BuildContext context) => new SettingVC(),
            '/PlusVC' : (BuildContext context) => new PlusVC(),
            '/TodoListVC' : (BuildContext context) => new ToDoListVC(),
            '/GoalsVC' : (BuildContext context) => new GoalsVC()
          },
          home: SplashScreen(), //TestWidget()
        );
      }
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  Languages hintObj = Languages(languagesList.first.index,languagesList.first.title,languagesList.first.flag);

  void navigateToNextPage(bool googleStatus)
  {
    Statics.shared.app.isSignedInWithGoogle = googleStatus;
    Navigator.push(context, new MaterialPageRoute(builder: (context) => new RegisterLoginController()));
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Colors.white,
      body: new Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          new Container(
            child: Image.asset('Resources/Images/get_started_bg_img.jpg', fit: BoxFit.fitWidth,width: (MediaQuery.of(context).size.width)),
            padding: const EdgeInsets.only(top: 50),
          ),
          new Container(child:
              DropdownButton(
                elevation: 4,
                hint: LanguagesDropdownWidget(hintObj.flag,hintObj.title),
                items: dropDownLanguagesList(),
                onChanged: (index){
                  for(int i =0; i<languagesList.length; i++)
                  {
                    if(languagesList[i].title == index)
                    {
                      setState(() {
                        hintObj = languagesList[i];
                      });
                    }
                  }
                },
              )
              ,
              height: 60, alignment: Alignment.topCenter),
        ],
      ),
      bottomSheet: Builder(
        builder: (bottomContext) => Container(height: 70,
          child: ButtonTheme(
            minWidth: (MediaQuery.of(context).size.width),
            height: 45,
            child: FlatButton(
              child: Text(Strings.shared.getStartedButton, style: TextStyle(fontSize: 24, fontWeight: FontWeight.normal, color: Colors.white)),
              color: Colors.blue,
              padding: const EdgeInsets.all(15),
              onPressed: (){
                setLanguageInCache(this.hintObj.index);
                googleSignIn.isSignedIn().then((bool st) => (navigateToNextPage(st))).catchError((e) => (navigateToNextPage(false)));
              },
            ),
          ),
        ),
      ),
    );
  }
}
