import 'package:flutter/material.dart';

class Languages
{
  int index;
  String title;
  Image flag;

  Languages(int index, String title, Image flag)
  {
    this.index = index;
    this.title = title;
    this.flag = flag;
  }
}

List<Languages> languagesList = [
  Languages(0,"English",Image.asset('Resources/Images/flags/usa-flag.png',width: 30)),
  Languages(1,"Spanish",Image.asset('Resources/Images/flags/spain-flag.png',width: 30)),
  Languages(2,"Dutch",Image.asset('Resources/Images/flags/germany-flag.png',width: 30))
];

class LanguagesDropdownWidget extends StatefulWidget {
  Image icon;
  String title;
  LanguagesDropdownWidget(Image icon, String title)
  {
    this.icon = icon;
    this.title = title;
  }
  @override
  _LanguagesDropdownWidgetState createState() => _LanguagesDropdownWidgetState();
}

class _LanguagesDropdownWidgetState extends State<LanguagesDropdownWidget> {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        this.widget.icon,
        SizedBox(width: 10, height: 60,),
        Text(this.widget.title, style: TextStyle(fontSize: 20)),
        SizedBox(width: 100)
      ],
    );
  }
}
