import 'package:flutter/material.dart';

class GoalObject
{
  int id = 0;
  int parentId = 0;
  String goalTitle = "";
  String goalImg = "";

  GoalObject(String title, String img, int parentId, int id)
  {
    this.goalTitle = title;
    this.goalImg = img;
    this.id = id;
    this.parentId = parentId;
  }
}