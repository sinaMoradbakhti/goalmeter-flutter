import 'package:flutter/material.dart';
import 'package:goal_meter/models/Goal/GoalObject.dart';
import 'package:goal_meter/models/Task/TaskObject.dart';

class MyGoalObject
{
  String gId = "";
  String gName = "";
  bool isArchive = false;
  String goalUnit = "";
  bool isRemoved = false;
  String goalParent = "";
  GoalObject goal;
  List<TaskObject> tasks = [];

  MyGoalObject(String gName,bool isArchive, String goalUnit, bool isRemoved,String goalParent, GoalObject goal)
  {
    this.gName = gName;
    this.isArchive = isArchive;
    this.goalUnit = goalUnit;
    this.isRemoved = isRemoved;
    this.goalParent = goalParent;
    this.goal = goal;
  }
}