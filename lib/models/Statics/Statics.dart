import 'package:flutter/material.dart';
import 'package:goal_meter/models/Goal/GoalObject.dart';
import 'package:goal_meter/models/Task/TaskObject.dart';
import 'package:goal_meter/models/Goal/MyGoalObject.dart';

class Statics
{
  static Statics shared = Statics();
  var data = _Data.shared;
  var user = _UserInfo();
  var app = _StaticsApp();
  var colors = _myColors();
  var fontSizes = _myFontSizes();
  var sharedPreferencesIndexes = _SharedPreferencesIndexes();
  var models = _Models();

  SnackBar snackBarMsg(String content) {
    return SnackBar(
        content: Text(
          content,
          style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.normal,
              color: Colors.white
          ),
        )
    );
  }// show snackbar func

  SnackBar snackBarMsgOnBottomSheet(String content) {
    return SnackBar(
        content: Text(
          "${content}\n\n",
          style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.normal,
              color: Colors.white
          ),
        )
    );
  }// show snackbar func

  _Statics()
  {
    _initialize();
  }

  void _initialize() async
  {

  }
}

class _myFontSizes
{
  double addTaskSectionsTitle = 16;
}
class _myColors
{
  Color addTaskSectionsBorderColor = Color.fromRGBO(196, 196, 196, 1);
  Color addTaskSectionsTitleColor = Color.fromRGBO(130,130,130,1);
}
class _Data
{
  static final _Data shared = _Data();

  List<MyGoalObject> myGoals = [];
  List<TaskObject> myTasks = [];
  _Data(){}

//  void refreshAllGoals() async
//  {
//    Statics.shared.data.myGoals = [];
//    UserObject.shared.getId().then((userId){
//      Firestore.instance
//          .collection('userGoals')
//          .where("userid", isEqualTo: userId)
//          .snapshots()
//          .listen((data){
//        data.documents.forEach(
//                (doc){
//              String gName = doc.data["goalName"];
//              String gId = doc.documentID;
//              bool isArchive = doc.data["isArchive"];
//              String goalUnit = doc.data["goalUnit"];
//              bool isRemoved = doc.data["isRemoved"];
//              String goalParent = doc.data["goalParent"];
//              GoalObject goal;
//              int goalParentId = 0;
//              for(var i=0;i<Statics.shared.models.goals.length;i++){
//                if(Statics.shared.models.goals[i].goalTitle == goalParent){
//                  goalParentId = Statics.shared.models.goals[i].id;
//                }
//              }
//              List<GoalObject> subs = Statics.shared.models.getGoalChildren(goalParentId);
//              for(var i=0;i<subs.length;i++)
//              {
//                if(subs[i].goalTitle == gName)
//                {
//                  goal = subs[i];
//                }
//              }
//              MyGoalObject thisGoal = MyGoalObject(gName,isArchive,goalUnit,isRemoved,goalParent,goal);
//              thisGoal.gId = gId;
//              Statics.shared.data.myGoals.add(thisGoal);
//              print("This Goal Added x (${thisGoal.gName})");
//              print("This Goal Added x (${Statics.shared.data.myGoals.length})");
//              Statics.shared.data.refreshAllTasks(goalId: gId);
//            }
//        );});
//    }
//    );
//  }
//  void refreshAllTasks({String goalId = ""}) async
//  {
//    if(goalId != ""){
//      UserObject.shared.getId().then((userId){
//        Firestore.instance
//            .collection('userTasks')
//            .where("userid", isEqualTo: userId).where("goalId", isEqualTo: goalId)
//            .snapshots()
//            .listen((data){
//          data.documents.forEach(
//                  (doc){
//                    TaskObject tsk = TaskObject(doc.data);
//                    for(int i = 0;i<Statics.shared.data.myGoals.length;i++){
//                      if(Statics.shared.data.myGoals[i].gId == tsk.goalId){
//                        Statics.shared.data.myGoals[i].tasks.add(tsk);
//                        print("Task Added Successfully.");
//                      }
//                    }// for loop
//              }
//          );});
//      }
//      );
//    }else{
//      UserObject.shared.getId().then((userId){
//        Firestore.instance
//            .collection('userTasks')
//            .where("userid", isEqualTo: userId)
//            .snapshots()
//            .listen((data){
//          data.documents.forEach(
//                  (doc){
//                    TaskObject tsk = TaskObject(doc.data);
//                    for(int i = 0;i<Statics.shared.data.myGoals.length;i++){
//                      if(Statics.shared.data.myGoals[i].gId == tsk.goalId){
//                        Statics.shared.data.myGoals[i].tasks.add(tsk);
//                        print("Task Added Successfully.");
//                      }
//                    }// for loop
//              }
//          );});
//      }
//      );
//    }
//  }
//  void getAllTasks() async
//  {
//    Statics.shared.data.myTasks = [];
//    UserObject.shared.getId().then((userId){
//      Firestore.instance
//          .collection('userTasks')
//          .where("userid", isEqualTo: userId)
//          .snapshots()
//          .listen((data){
//        data.documents.forEach(
//                (doc){
//              TaskObject tsk = TaskObject(doc.data);
//              Statics.shared.data.myTasks.add(tsk);
//              print("Task Added Successfully (All Tasks).");
//            }
//        );});
//    }
//    );
//  }
}

class _UserInfo
{
  String uid = "";
  String fullName = "";
  String email = "";
  String phoneNumber = "";
  String photoURL = "";

  UserInfo(){}
}
class _Models
{
  List<GoalObject> goals =
  [
    GoalObject("Exercise","Resources/Images/goals/predef_group_bg_exercise.png",0,1),
    GoalObject("Sports","Resources/Images/goals/predef_group_bg_sports.png",0,2),
    GoalObject("Study","Resources/Images/goals/predef_group_bg_study.png",0,3),
    GoalObject("Diet","Resources/Images/goals/predef_group_bg_diet.png",0,4),
    GoalObject("Lifestyle","Resources/Images/goals/predef_group_bg_lifestyle.png",0,5),
    GoalObject("Work","Resources/Images/goals/predef_group_bg_work.png",0,6),
    GoalObject("Relationship","Resources/Images/goals/predef_group_bg_relationship.png",0,7),
    GoalObject("Financial","Resources/Images/goals/predef_group_bg_financial.png",0,8),
    GoalObject("Health","Resources/Images/goals/predef_group_bg_health.png",0,9),
    GoalObject("Bad Habits","Resources/Images/goals/predef_group_bg_bad_habit.png",0,10),
  ];

  List<GoalObject> getGoalChildren(int parentId)
  {
    List<GoalObject> myList = [];
    switch(parentId)
    {
      case 3:
        myList.add(GoalObject("Study Hard","Resources/Images/goals/funny_cartoon_52_study_hard.png",3,1));
        myList.add(GoalObject("Read a Book","Resources/Images/goals/funny_cartoon_53_read_book.png",3,2));
        myList.add(GoalObject("Prepare For An Exam","Resources/Images/goals/funny_cartoon_54_prepare_exam.png",1,3));
        break;
      case 1:
        myList.add(GoalObject("Exercise","Resources/Images/goals/funny_cartoon_51_exercise.png",1,1));
        myList.add(GoalObject("Stretching","Resources/Images/goals/funny_cartoon_45_stretch.png",1,2));
        myList.add(GoalObject("Running","Resources/Images/goals/funny_cartoon_6_running.png",1,3));
        myList.add(GoalObject("Bicycling","Resources/Images/goals/funny_cartoon_16_ride_a_bike.png",1,4));
        myList.add(GoalObject("Swimming","Resources/Images/goals/funny_cartoon_18_swimming.png",1,5));
        myList.add(GoalObject("Yoga","Resources/Images/goals/funny_cartoon_50_yoga.png",1,6));
        myList.add(GoalObject("Climbing","Resources/Images/goals/funny_cartoon_23_climbing.png",1,7));
        myList.add(GoalObject("Dancing","Resources/Images/goals/funny_cartoon_47_dance.png",1,8));
        myList.add(GoalObject("Soccer","Resources/Images/goals/funny_cartoon_21_soccer.png",1,9));
        myList.add(GoalObject("Footbal","Resources/Images/goals/funny_cartoon_22_footbal.png",1,10));
        myList.add(GoalObject("Skiing","Resources/Images/goals/funny_cartoon_48_skiing.png",1,11));
        break;
    }
    myList.add(GoalObject("Create custom goal","Resources/Images/goals/create_custom_goal_bg.png",999,999));
    return myList;
  }
}

class _SharedPreferencesIndexes
{
  String isAppLoggedIn = "IS_APP_LOGGED_IN";
  String appLang = "APP_LANG";
  // user logged in info
  String userLoginFullName = "USER_LOGGED_IN_FULLNAME";
  String userLoginEmail = "USER_LOGGED_IN_EMAIL";
  String userLoginPhotoURL = "USER_LOGGED_IN_PHOTOURL";
  String userLoginUID = "USER_LOGGED_IN_UID";
  String userLoginPN = "USER_LOGGED_IN_PHONENUMBER";
}
class _StaticsApp
{
  bool isAppLogin = false;
  bool isSignedInWithGoogle = false;

  StaticsApp()
  {
    _checkLoginstatus();
  }
  void _checkLoginstatus(){
  }
  void setAppLoginStatus(bool status)
  {
    _checkLoginstatus();
  }
}

