import 'package:flutter/material.dart';

class Strings
{
  static Strings shared = Strings();
  _Strings(){}

  final String backWordCPSLock = "BACK";
  final String nextWord = "Next";
  final String getStartedButton = "Get Started";
  final String signUpWithEmail = "Sign up with email";
  final String signInFirstPage = "Sign in";
  final String signUpFirstPage = "Sign up";
  final String fillTheFieldsSnackBar = "please Fill all the fields!";
  final String haveAnAccountQ = "Have an account?";
  final String goalCreatedSuccessfullySnackMsg = "your Goal created successfully.";

  final String fullNameHint = "Name";
  final String emailHint = "Email address";
  final String passwordHint = "Password";

  final String userSingUpSuccess = "Signing up was Successful, Welcome to GoalMeter";
  final String userSingInSuccess = "Signing in was Successful, Welcome to GoalMeter";

  final String modalProgressLoading = "Please wait...";

  // AddVC
  final String pickACategoryTopPage = "Pick a category";
  // PickGoal
  final String pickAGoalTopPage = "Pick a goal";
  // Create Goal
  final String secondRowFirstTitle = "How would you like to measure your progress?";
  final String totalProgress = "Total progress";

  final String progressLevelIntermediate = "Intermediate";
  final String progressLevelBeginner = "Beginner";
  final String progressLevelUpperIntermediate = "Professional";
  final String progressLevelAdvanced = "advanced";
  final String progressLevelMaster = "Master";

  final String startCapsLockOn = "START";
  // goal units
  final String secondRowFirstTitleDistanceRadioButtonValue = "Distance: kilometers / meters";
  final String secondRowFirstTitleTimeRadioButtonValue = "Time: hours / minutes";
  // goal units
  final String secondRowFirstChallengesTitle = "Challenges";
  final String forthRowCreateGoalBtnStartFinished = "Challenge Completed";
  final String forthRowCreateGoalBtnStartNotFinished = "Finish previous level";

  // goals
  final String goalsControllerTitle = "My Goals";
  final String goalsControllerBtnAddNewGoal = "Add new goal";
  // goals

  // add task
  final String addNewTaskTitle = "Add New Task";
  final String stepOneContainerTitleAddNewTask = "Add New Task";
  final String inputLableSelectAGoal = "Select a goal";
  final String inputLableEnterTaskName = "Task name";
  final String buttonArrowBack = "BACK";
  final String buttonArrowNextStepBtn = "STEP";
  final String buttonArrowNextStep2 = "STEP 2";
  final String step2TitleAddTask = "Step 2: Task Type";
  final String stepTwoContainerTitleAddNewTaskType = "Task Type";
  final String firstRowStep2AddTaskTitle = "One-time task or repeating task?";
  final String RadioButtonTaskTypeOneTimeTask = "One-time task";
  final String RadioButtonTaskTypeOneTimeTaskLabel = "One-time Task";
  final String RadioButtonTaskTypeRepeatingTask= "Repeating task";
  final String RadioButtonTaskTypeRepeatingTaskLabel= "Repeating Task";
  final String continueToNextStep = "Continue to next step.";
  final String stepTwoContainerTitleAddNewTaskRowRepeatingTask = "How often is your task repeated?";
  final String daily = "Daily";
  final String weekly = "Weekly";
  final String monthly = "Monthly";
  final String buttonArrowNextStep3 = "STEP 3";
  final String buttonArrowNextStep4 = "STEP 4";
  final String step3TitleAddTask = "Step 3: Date and Time";
  final String stepTwoContainerTitleAddNewTaskRowRepeatingDailyTask = "Which days of the week?";
  final String stepTwoContainerTitleAddNewTaskRowRepeatingWeeklyTask = "How many ‘hours’ per week?";
  final String stepTwoContainerTitleAddNewTaskRowRepeatingMonthlyTask = "How many ‘hours’ per month?";
  final String textfieldPlaceHolderHoursPerWeek = "0";
  final String labelHoursPerWeek = "‘hours’ per week";
  final String onSpecificDaysOfWeek = "On specific days of week?";
  final String onSpecificDaysOfMonth = "On specific days of month?";
  final String labelHoursPerMonth = "‘hours’ per month";
  final String monthlyMySelectedDays = "my selected days";
  final String pleaseSelectOneOrMoreDaysMonthly = "Pick days of month";
  final String daysOfMonthSentence = "Days of Month";

  final String mondayMon = "Mon";
  final String tuesdayTue = "Tue";
  final String wednesdayWed = "Wed";
  final String thursdayThu = "Thu";
  final String fridayFri = "Fri";
  final String saturdaySat = "Sat";
  final String sundaySun = "Sun";

  final String multiSelectFirstDayOfMonth = "first day of month";
  final String multiSelectLastDayOfMonth = "Last day of month";
  final String cancelCapsLockOn = "CANCEL";
  final String okCapsLockOn = "OK";
  final String closeWordCamelCap = "CLOSE";
  final String saveWordCamelCap = "SAVE";

  //Step 3
  final String addTaskStep3ChooseDateTitle = "What date would you like to do it?";
  final String addTaskStep3DateLabel = "Date";

  final String addTaskStep3ChooseTimeTitle = "Would you like to do it on a specific time of day?";
  final String addTaskStep3TimeLabel = "Time";
  final String addTaskStep3StartTimeLabel = "Start time";
  final String addTaskStep3EndTimeLabel = "End time";
  final String endTimeNextDay = "End time next day";
  final String endDateLabelForAddTaskStep3 = "End date";
  final String startDateLabelForAddTaskStep3 = "Start date";
  final String forEverCamelCap = "Forever";
  final String pickADate = "Pick a date";
  //Step4
  final String step4TitleAddTask = "Step 4: Notification";
  final String addTaskStep4NotificationLabel = "Notification";
  final String addTaskStep4NotificationTitle = "Would you like to receive notifications?";
  final String addTaskStep4NotificationDescription= "minutes before task starts";
  final String addTaskStep4CreateTaskBtn= "Create Task";

  final String taskCreatedSuccessfullySnackMsg = "your Task created successfully.";
  final String noGoalCamelCapWithFirst = "No Goal";
  final String allGoalCamelCapWithFirst = "All Goal";
  final String allGoalCapsLock = "ALL GOALS";

  final String snackBarBottomSheetAddTaskPleaseEnter = "please enter";
  final String snackBarBottomSheetAddTaskTaskName = "Task Name";
  final String snackBarBottomSheetAddTaskDailySpecificDays = "choose specific days";
  final String snackBarBottomSheetAddTaskDiffrentTimes = "choose diffrent times";
  final String snackBarBottomSheetAddTaskStartIsUp = "start Time should be upper than end Time";
  final String snackBarBottomSheetAddTaskReceiveNotifiTime = "you Should choose minutes for receiving notification time";
  // add task
}



