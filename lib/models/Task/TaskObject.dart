import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class TaskObject
{
  String tId;
  String userid;
  String goalId;
  String taskName;
  String taskType;
  String repeatedType;
  String specificDays;
  int hoursPerWeek;
  int hoursPerMonth;
  Timestamp startDate;
  Timestamp endDate;
  bool isSpecificTime;
  String startTime;
  String endTime;
  bool isEndTimeNextDay;
  bool recieveNotification;
  int minutesToRecieveNotification;
  Timestamp createDate;
  String createTime;
  bool isRemoved;
  bool isArchive;
  String value;

  TaskObject(var obj){
    if(obj != null){
      this.userid = obj['userid'];
      this.goalId = obj['goalId'];
      this.taskName = obj['taskName'];
      this.taskType = obj['taskType'];
      this.repeatedType = obj['repeatedType'];
      this.specificDays = obj['specificDays'];
      this.hoursPerWeek = obj['hoursPerWeek'];
      this.hoursPerMonth = obj['hoursPerMonth'];
      this.startDate = obj['startDate'];
      this.endDate = obj['endDate'];
      this.isSpecificTime = obj['isSpecificTime'];
      this.startTime = obj['startTime'];
      this.endTime = obj['endTime'];
      this.isEndTimeNextDay = obj['isEndTimeNextDay'];
      this.minutesToRecieveNotification = obj['minutesToRecieveNotification'];
      this.createDate = obj['createDate'];
      this.createTime = obj['createTime'];
      this.isRemoved = obj['isRemoved'];
      this.isArchive = obj['isArchive'];
      this.value = obj['value'];
    }
  }
}