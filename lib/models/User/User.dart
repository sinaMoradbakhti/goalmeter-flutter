import 'package:shared_preferences/shared_preferences.dart';
import 'package:goal_meter/models/Statics/Statics.dart';

class UserObject
{
  static UserObject shared = UserObject();
  _UserObject(){updateInfoFromCache();}

  String fullName = "";
  String email = "";
  String photoURL = "";
  String uid = "";
  String phoneNumber = "";

  Future<String> getFullName() async
  {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String myFullName = prefs.getString(Statics.shared.sharedPreferencesIndexes.userLoginFullName) ?? "";
    return myFullName;
  }
  Future<String> getId() async
  {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String myUID = prefs.getString(Statics.shared.sharedPreferencesIndexes.userLoginUID) ?? "";
    return myUID;
  }
  Future<String> getEmail() async
  {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String myEmail = prefs.getString(Statics.shared.sharedPreferencesIndexes.userLoginEmail) ?? "";
    return myEmail;
  }
  Future<String> myPhoto() async
  {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String myPhoto = prefs.getString(Statics.shared.sharedPreferencesIndexes.userLoginPhotoURL) ?? "";
    return myPhoto;
  }
  Future<String> myPhoneNumber() async
  {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String myPN = prefs.getString(Statics.shared.sharedPreferencesIndexes.userLoginPN) ?? "";
    return myPN;
  }

  Future<UserObject> updateInfoFromCache() async
  {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String myFullName = prefs.getString(Statics.shared.sharedPreferencesIndexes.userLoginFullName) ?? "";
    String myEmail = prefs.getString(Statics.shared.sharedPreferencesIndexes.userLoginEmail) ?? "";
    String myPhoto = prefs.getString(Statics.shared.sharedPreferencesIndexes.userLoginPhotoURL) ?? "";
    String myUID = prefs.getString(Statics.shared.sharedPreferencesIndexes.userLoginUID) ?? "";
    String myPN = prefs.getString(Statics.shared.sharedPreferencesIndexes.userLoginPN) ?? "";

    this.fullName = myFullName;
    this.email = myEmail;
    this.uid = myUID;
    this.photoURL = myPhoto;
    this.phoneNumber = myPN;

    return this;
  }
  updateInfoToCache(String myFullName,String myEmail,String myPhoto, String myUID,myPhoneNumber) async
  {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(Statics.shared.sharedPreferencesIndexes.userLoginFullName,myFullName);
    prefs.setString(Statics.shared.sharedPreferencesIndexes.userLoginEmail,myEmail);
    prefs.setString(Statics.shared.sharedPreferencesIndexes.userLoginPhotoURL,myPhoto);
    prefs.setString(Statics.shared.sharedPreferencesIndexes.userLoginUID,myUID);
    prefs.setString(Statics.shared.sharedPreferencesIndexes.userLoginPN,myPhoneNumber);
  }
}