import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:goal_meter/models/User/User.dart';
import 'package:goal_meter/models/Goal/GoalObject.dart';
import 'package:goal_meter/models/Goal/MyGoalObject.dart';
import 'package:goal_meter/models/Task/TaskObject.dart';
import 'package:goal_meter/models/Statics/Statics.dart';

class CatchDataFromFireStore
{
  void refreshAllGoals({VoidCallback onComplete}) async
  {
    List<MyGoalObject> myList = [];
    UserObject.shared.getId().then((userId){
      Firestore.instance
          .collection('userGoals')
          .where("userid", isEqualTo: userId)
          .snapshots()
          .listen((data){
        data.documents.forEach(
                (doc){
              String gName = doc.data["goalName"];
              String gId = doc.documentID;
              bool isArchive = doc.data["isArchive"];
              String goalUnit = doc.data["goalUnit"];
              bool isRemoved = doc.data["isRemoved"];
              String goalParent = doc.data["goalParent"];
              GoalObject goal;
              int goalParentId = 0;
              for(var i=0;i<Statics.shared.models.goals.length;i++){
                if(Statics.shared.models.goals[i].goalTitle == goalParent){
                  goalParentId = Statics.shared.models.goals[i].id;
                }
              }
              List<GoalObject> subs = Statics.shared.models.getGoalChildren(goalParentId);
              for(var i=0;i<subs.length;i++)
              {
                if(subs[i].goalTitle == gName)
                {
                  goal = subs[i];
                }
              }
              MyGoalObject thisGoal = MyGoalObject(gName,isArchive,goalUnit,isRemoved,goalParent,goal);
              thisGoal.gId = gId;
              this.refreshAllTasks(goalId: gId, onComplete: (result){
                thisGoal.tasks = result;
                myList.add(thisGoal);
              },onFail: (st){
                if(st){
                  thisGoal.tasks = [];
                  myList.add(thisGoal);
                }
              });
            }
        );

        var future = new Future.delayed(const Duration(seconds: 2));
        future.whenComplete((){
          Statics.shared.data.myGoals = myList;
          print("${myList.length} Goals Got from Server.");
          onComplete();
        });

          });
    }
    );
  }
  void refreshAllTasks({String goalId = "", onComplete(List<TaskObject> result), onFail(bool st)}) async
  {
    List<TaskObject> myTasks = [];
    if(goalId != ""){
      UserObject.shared.getId().then((userId){
        Firestore.instance
            .collection('userTasks')
            .where("userid", isEqualTo: userId).where("goalId", isEqualTo: goalId)
            .snapshots()
            .listen((data){
          data.documents.forEach(
                  (doc){
                    String tId = doc.documentID;
                    TaskObject tsk = TaskObject(doc.data);
                    tsk.tId = tId;
                    myTasks.add(tsk);
              }
          );
          onComplete(myTasks);
        }).onError((error){
          onFail(true);
        });
      }
      ).catchError((error){
        onFail(true);
      });
    }else{
      UserObject.shared.getId().then((userId){
        Firestore.instance
            .collection('userTasks')
            .where("userid", isEqualTo: userId)
            .snapshots()
            .listen((data){
          data.documents.forEach(
                  (doc){
                    String tId = doc.documentID;
                    TaskObject tsk = TaskObject(doc.data);
                    tsk.tId = tId;
                    myTasks.add(tsk);
              }
          );
          onComplete(myTasks);
            }).onError((error){onFail(true);});
      }
      ).catchError((error){onFail(true);});
    }
  }
  void getAllTasks({VoidCallback onComplete}) async
  {
    List<TaskObject> myList = [];
    UserObject.shared.getId().then((userId){
      Firestore.instance
          .collection('userTasks')
          .where("userid", isEqualTo: userId)
          .snapshots()
          .listen((data){
        data.documents.forEach(
                (doc){
                  String tId = doc.documentID;
                  TaskObject tsk = TaskObject(doc.data);
                  tsk.tId = tId;
                  myList.add(tsk);
            }
        );

        var future1 = new Future.delayed(const Duration(seconds: 2));
        future1.whenComplete((){
          Statics.shared.data.myTasks = myList;
          print("${myList.length} Tasks Got from Server.");
          onComplete();
        });

          }).onError((error){
            print("ERROR TASKING LOADING : ${error.toString()}");
      });
    }
    );
  }
}