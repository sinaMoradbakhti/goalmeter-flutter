import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:dashed_container/dashed_container.dart';

class CircularDayPicker extends StatefulWidget {

  String dayName = "";
  bool isChecked = false;

  CircularDayPicker({String dayName,bool isChecked}){
    this.dayName = dayName;
    this.isChecked = isChecked;
  }

  @override
  _CircularDayPickerState createState() => _CircularDayPickerState();
}


class _CircularDayPickerState extends State<CircularDayPicker> {
  @override
  Widget build(BuildContext context) {
    if(this.widget.isChecked){
      return Container(child: IconButton(
          padding: const EdgeInsets.all(0),
          iconSize: 50,
          icon: Container(
            width: 40, height: 40,
            decoration: BoxDecoration(
              color: Colors.blue,
              borderRadius: BorderRadius.all(Radius.circular(50)),
            ),
            child: Center(child: Text(this.widget.dayName, style: TextStyle(color: Colors.white))),
          ), // container
          onPressed: (){setState(() {
            if(this.widget.isChecked){
              this.widget.isChecked = false;
            }
            else{
              this.widget.isChecked = true;
            }
          });}
      ), margin: const EdgeInsets.all(5),);
    }
    else{
      return Container(child: IconButton(
          padding: const EdgeInsets.all(0),
          iconSize: 50,
          icon: DashedContainer(
            child: Container(
              width: 40, height: 40,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(50)),
              ),
              child: Center(child: Text(this.widget.dayName, style: TextStyle(color: Colors.black))),
            ), // container
            borderRadius: 20,
            dashColor: Colors.black,
            dashedLength: 1.5,
            blankLength: 4,
            strokeWidth: 1.5,
          ), // dashed container
          onPressed: (){setState(() {
            if(this.widget.isChecked){
              this.widget.isChecked = false;
            }
            else{
              this.widget.isChecked = true;
            }
          });}
      ),margin: const EdgeInsets.all(5),);
    }
  }
}
