import 'package:flutter/material.dart';
import 'package:goal_meter/models/Statics/Strings.dart';
import 'package:goal_meter/models/Statics/Statics.dart';

class CustomRadioButton extends StatefulWidget {

  String title = "";
  bool isChecked = false;
  VoidCallback onPressed;

  CustomRadioButton({String title = "", bool isChecked = false,VoidCallback onPressed}){
    this.title = title;
    this.isChecked = isChecked;
    this.onPressed = onPressed;
  }

  @override
  _CustomRadioButtonState createState() => _CustomRadioButtonState();
}

class _CustomRadioButtonState extends State<CustomRadioButton> {
  @override
  Widget build(BuildContext context) {

    if(this.widget.isChecked){
      return Container(child: FlatButton(
        child: Container(
          child: Row(
            children: [
              Text(this.widget.title, style: TextStyle(fontSize: 17, color: Colors.blue)),
              Container(width: 20,child: Image.asset("Resources/icons/icons8-green-check-mark-80.png", width: 20,height: 20, fit: BoxFit.contain,),
                alignment: Alignment.center,
              )
            ],
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
          ), // Row
          decoration: BoxDecoration(
            border: Border.all(color: Colors.blue, width: 1),
            borderRadius: BorderRadius.all(Radius.circular(5)),
          ),
          padding: const EdgeInsets.only(bottom: 12, top: 12, right: 16, left: 16),
        ),
        padding: const EdgeInsets.all(0),
        onPressed: (){
          this.widget.onPressed();
        },
      ),margin: const EdgeInsets.only(bottom: 5),);
    }// isChecked == true
    else{
      return Container(child: FlatButton(
        child: Container(
          child: Row(
            children: [
              Text(this.widget.title, style: TextStyle(fontSize: 17, color: Colors.grey)),
              Container(width: 20,child: Container(),
                alignment: Alignment.center,
              )
            ],
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
          ), // Row
          padding: const EdgeInsets.only(bottom: 12, top: 12, right: 16, left: 16),
        ),
        padding: const EdgeInsets.all(0),
        onPressed: (){
          this.widget.onPressed();
        },
      ),margin: const EdgeInsets.only(bottom: 5),);
    }
  }
}