import 'package:flutter/material.dart';

class TabCircle extends StatefulWidget {
  bool isColored = false;

  TabCircle({bool isColored = false}){
    this.isColored = isColored;
  }

  @override
  _TabCircleState createState() => _TabCircleState();
}

class _TabCircleState extends State<TabCircle> {

  Color _offColor = Color.fromRGBO(196, 196, 196, 1);
  Color _onColor = Color.fromRGBO(148, 10, 0, 1);

  @override
  Widget build(BuildContext context) {
    Color selectedColor = _offColor;
    if(this.widget.isColored){
      selectedColor = _onColor;
    }
    return Container(width: 7,height: 7,decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(3.5)),
        color: selectedColor,
    ),
      margin: const EdgeInsets.only(left: 2.5, right: 2.5),
    );
  }
}
