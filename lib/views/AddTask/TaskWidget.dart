import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:goal_meter/models/Goal/MyGoalObject.dart';

class TaskWidget extends StatelessWidget {

  String _title = "";
  Image _img;
  String _imgName = "";
  MyGoalObject myGoal;

  TaskWidget({Image img, String title, String imgName, MyGoalObject myGoal}){
    _title = title;
    _img = img;
    _imgName = imgName;
    myGoal = myGoal;
  }

  @override
  Widget build(BuildContext context) {
    if(_img != null){
      return Container(
        child: Row(
          children: [
            Container(child: Container(),margin: const EdgeInsets.only(right: 5,),decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(40)),
              image: DecorationImage(
                image: new ExactAssetImage(this._imgName),
                fit: BoxFit.cover,
              )
            ),
              width: 40,
              height: 40,
            ),
            SizedBox(width: 8,),
            Text(_title, style: TextStyle(
              color: Colors.black,
              fontSize: 18,
            ))
          ],
        ),
        padding: const EdgeInsets.only(right: 16, left: 16, top: 0, bottom: 0),
        //width: (MediaQuery.of(context).size.width)-50-16-16-16-8-24,
        margin: const EdgeInsets.only(bottom: 16),
        height: 40,
      );
    }
    else{
      return Column(
        children: [
          Container(
            child: Row(
              children: [
                Text(_title, style: TextStyle(
                  color: Colors.black,
                  fontSize: 18,
                ))
              ],
            ),
            padding: const EdgeInsets.only(right: 16, left: 66, bottom: 10, top: 10),
          ),
          Container(height: 0.5, color: Color.fromRGBO(0, 0, 0, 0.1))
        ],
      );
    }
  }
}

class TaskWidgetSelectable extends StatefulWidget {

  bool isSelected = false;
  TaskWidget task;
  VoidCallback onSelectItem;

  TaskWidgetSelectable({
    bool isSelected = false,
    TaskWidget task,
    VoidCallback onSelectItem,
  })
  {
    this.isSelected = isSelected;
    this.task = task;
    this.onSelectItem = onSelectItem;
  }

  @override
  TaskWidgetSelectableState createState() => TaskWidgetSelectableState();
}

class TaskWidgetSelectableState extends State<TaskWidgetSelectable> {

  void refreshSelect(){
    setState(() {
      if(this.widget.isSelected){
        this.widget.isSelected = false;
      }else{
        this.widget.isSelected = true;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    Widget img;

    if(this.widget.isSelected){
      img = Image.asset("Resources/icons/icons8-green-check-mark-80.png", fit: BoxFit.fitWidth);
    }else{
      img = Container();
    }

    if(this.widget.task._imgName == ""){
      return FlatButton(
        child: Container(
          child: Row(
            children: [
              Container(child: this.widget.task,),
              Container(width: 25, height:60,child: img,margin: const EdgeInsets.only(right: 16)),
            ],
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
          ),
        ),
        onPressed: (){
          this.widget.onSelectItem();
        },
        padding: const EdgeInsets.all(0),
      );
    }// no goal task
    else{
      return FlatButton(
        child: Container(
          child: Row(
            children: [
              Container(child: this.widget.task,),
              Container(width: 25, height:60,child: img,margin: const EdgeInsets.only(right: 16),padding: const EdgeInsets.only(bottom: 16),),
            ],
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
          ),
        ),
        onPressed: (){
          this.widget.onSelectItem();
        },
        padding: const EdgeInsets.all(0),
      );
    }
  }
}

