import 'package:flutter/material.dart';
import 'package:goal_meter/controllers/mainTabbar/anotherController/Add/Add.dart';
import 'package:goal_meter/controllers/mainTabbar/anotherController/Add/PickGoal.dart';
import 'package:goal_meter/models/Goal/GoalObject.dart';
import 'package:goal_meter/models/Goal/MyGoalObject.dart';

class BoxWidget extends StatelessWidget {

  double boxSize = 0;
  GoalObject thisGoal;
  MyGoalObject myGoal;
  AddVC myParent;

  BoxWidget(double boxSize, GoalObject goal,AddVC parent,MyGoalObject myGoal){
    this.boxSize = boxSize;
    this.myGoal = myGoal;
    myParent = parent;
    if(goal != null)
      {
        this.thisGoal = goal;
      }
    else
      {
        this.thisGoal = GoalObject("","",0,0);
      }
  }
  BoxWidget.BoxWidgetWithoutGoalOBJ(double boxSize, GoalObject goal,AddVC parent,)
  {
    this.boxSize = boxSize;
    this.myGoal = myGoal;
    myParent = parent;
    if(goal != null)
    {
      this.thisGoal = goal;
    }
    else
    {
      this.thisGoal = GoalObject("","",0,0);
    }
  }

  @override
  Widget build(BuildContext context) {
    if(thisGoal.goalImg == "")
      {
        return Container(
          width: boxSize,
          height: boxSize,
          color: Colors.white,
          child: Container(),
        );
      }
    else
      {
        return FlatButton(
            child: Container(
              width: boxSize,
              height: boxSize,
              color: Colors.white,
              child: Container(
                decoration: BoxDecoration(image: new DecorationImage(image: new AssetImage(this.thisGoal.goalImg), fit: BoxFit.cover), border: Border.all(width: 1,color: Colors.grey)),
                child: Column(
                  children: [
                    Expanded(child: Container()),
                    Container(color: Color.fromRGBO(0, 0, 0, 0.5),
                      child: Text(
                        thisGoal.goalTitle,
                        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 20),
                        textAlign: TextAlign.left,
                      ),
                      padding: const EdgeInsets.all(15),
                      width: boxSize,
                    )
                  ],
                ),
              ),
            ),
            padding: const EdgeInsets.all(0),
            onPressed: (){
              this.myParent.pickedGoal(thisGoal);
            },
        );
      }
  }
}

class BoxWidgetSubGoal extends StatelessWidget {

  double boxSize = 0;
  GoalObject thisGoal;
  PickGoal myParent;

  BoxWidgetSubGoal(double boxSize, GoalObject goal,PickGoal parent){
    this.boxSize = boxSize;
    myParent = parent;
    if(goal != null)
    {
      this.thisGoal = goal;
    }
    else
    {
      this.thisGoal = GoalObject("","",0,0);
    }
  }

  @override
  Widget build(BuildContext context) {
    if(thisGoal.goalImg == "")
    {
      return Container(
        width: boxSize,
        height: boxSize,
        color: Colors.white,
        child: Container(),
      );
    }
    else
    {
      return FlatButton(
        child: Container(
          width: boxSize,
          height: boxSize / 2,
          color: Colors.white,
          child: Container(
            decoration: BoxDecoration(image: new DecorationImage(image: new AssetImage(this.thisGoal.goalImg), fit: BoxFit.cover), border: Border.all(width: 1,color: Colors.grey)),
            child: Column(
              children: [
                Expanded(child: Container()),
                Container(color: Color.fromRGBO(0, 0, 0, 0.5),
                  child: Text(
                    thisGoal.goalTitle,
                    style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 20),
                    textAlign: TextAlign.left,
                  ),
                  padding: const EdgeInsets.all(15),
                  width: boxSize,
                )
              ],
            ),
          ),
        ),
        padding: const EdgeInsets.all(0),
        onPressed: (){
          this.myParent.pickedGoal(thisGoal);
        },
      );
    }
  }
}

