import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';

class GoalProgressWidget extends StatefulWidget {

  Color progressColorDark = Color.fromRGBO(0, 79, 29, 1);
  Color progressColorLight = Color.fromRGBO(0, 255, 102, 1);
  Color borderColor = Colors.grey;
  double height = 50;
  double width = 400;
  String _iconRunnerImagePath = "Resources/icons/greenRunner.png";
  String _iconFlagPath = "Resources/icons/oFlag.png";
  String _iconHintPath = "Resources/icons/numPlaceHolder.png";
  double _firstRunnerBackgroundWidth = 0;
  Alignment _flagPosition = Alignment(0.95,-3.8);
  int progressValuePercent = 0;
  int progressTotal = 100;

  GoalProgressWidget({
    double width = 200,
    double height = 50,
    Color borderColor = Colors.grey,
    int progressValuePercent = 0,
    int progressTotal = 0,
  })
  {
    this.width = width;
    this.height = height;
    this.borderColor = borderColor;
    this.progressValuePercent = progressValuePercent;
    this.progressTotal = progressTotal;
  }

  @override
  _GoalProgressWidgetState createState() => _GoalProgressWidgetState();
}

class _GoalProgressWidgetState extends State<GoalProgressWidget> {
  @override
  Widget build(BuildContext context) {

    double hintPosition = -1;
    BorderRadius radius = BorderRadius.all(Radius.circular(this.widget.height / 2));
    double one100Percent = this.widget.width - 70;
    print("WW: ${this.widget.width}");
    print("WPERCENT: ${one100Percent}");

    this.widget._firstRunnerBackgroundWidth = (this.widget.progressValuePercent * one100Percent) / 100;

    if(this.widget._firstRunnerBackgroundWidth < this.widget.height * 2){
      this.widget._firstRunnerBackgroundWidth += this.widget.height / 2;
    }

    hintPosition = (((this.widget.progressValuePercent - 50) * 2) / 100);

    Widget flag = Container(
      width: this.widget.width,
      height: this.widget.height,
      child: Container(
        width: this.widget.height / 1.7,
        height: this.widget.height / 1.7,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: new ExactAssetImage(this.widget._iconFlagPath),
            fit: BoxFit.cover,
          ),
        ),
      ),
      alignment: this.widget._flagPosition,
    );
    Widget runner = Container(
      width: this.widget.width,
      height: this.widget.height,
      child: Container(
        width: this.widget.height,
        height: this.widget.height,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(25)),
          image: DecorationImage(
            image: new ExactAssetImage(this.widget._iconRunnerImagePath),
            fit: BoxFit.cover,
          ),
        ),
      ),
      alignment: Alignment(hintPosition,0),
    );
    Widget runnerBackground = PhysicalModel(
      child: Container(
        width: this.widget._firstRunnerBackgroundWidth,
        //alignment: Alignment(-1,0),
        decoration: BoxDecoration(
          gradient: LinearGradient(colors: [this.widget.progressColorDark, this.widget.progressColorLight]),
          borderRadius: radius,
        ),
      ),
      color: Colors.transparent,
      borderRadius: radius,
    );
    Widget progress = PhysicalModel(
      child: Container(
        height: this.widget.height,
        width: this.widget.width,
        decoration: BoxDecoration(
          border: Border.all(color: this.widget.borderColor, width: 1.0),
          borderRadius: radius,
        ),
        child: runnerBackground,
        alignment: Alignment(-1,0),
      ),
      color: Colors.transparent,
      borderRadius: radius,
      clipBehavior: Clip.antiAlias,
    );
    Widget total = Text("");
    Widget hint = Container(
      width: this.widget.width,
      height: this.widget.height,
      child: Container(
        width: this.widget.height,
        height: this.widget.height / 1.7,
        decoration: BoxDecoration(
          color: Colors.transparent,
          image: DecorationImage(
            image: new ExactAssetImage(this.widget._iconHintPath),
            fit: BoxFit.fill,
          ),
        ),
        child: Padding(child: Text(this.widget.progressValuePercent.toString(), textAlign: TextAlign.center, style: TextStyle(fontSize: 14)),padding: const EdgeInsets.only(top: 2.5),),
      ),
      alignment: Alignment(hintPosition,3),
    );
    if(this.widget.progressTotal != 0){
      total = Container(child: Text(this.widget.progressTotal.toString(), style: TextStyle(color: this.widget.borderColor,fontSize: 15)),height: this.widget.height, alignment: Alignment(1,0),margin: const EdgeInsets.only(right: 8),);
    }

    return Container(
      height: this.widget.height + (this.widget.height - 20),
      width: this.widget.width,
      child: Stack(
        children: <Widget>[
          flag,
          total,
          progress,
          runner,
          hint,
        ],
        //alignment: this.widget._flagPosition,
      ), // Stack
      alignment: Alignment.center,
    ); // main Container
  }
}
