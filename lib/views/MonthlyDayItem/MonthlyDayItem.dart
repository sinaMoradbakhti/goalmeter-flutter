import 'package:flutter/material.dart';
import 'package:goal_meter/models/Statics/Strings.dart';

class MonthlyDayItem extends StatefulWidget {

  bool isSelected = false;
  int dayNumber = 1;
  bool isLastDayOfMonth = false;

  MonthlyDayItem({int dayNumber = 1, bool isChecked = false, bool isLastDayOfMonth = false}){
    this.dayNumber = dayNumber;
    this.isSelected = isChecked;
    this.isLastDayOfMonth = isLastDayOfMonth;
  }

  @override
  _MonthlyDayItemState createState() => _MonthlyDayItemState();
}

class _MonthlyDayItemState extends State<MonthlyDayItem> {
  @override
  Widget build(BuildContext context) {

    String strTitle = "";

    if(this.widget.dayNumber == 1){
      strTitle = "${this.widget.dayNumber} (${Strings.shared.multiSelectFirstDayOfMonth})";
    }else{
      if(this.widget.isLastDayOfMonth){
        strTitle = "${Strings.shared.multiSelectLastDayOfMonth}";
      }
      else{
        strTitle = "${this.widget.dayNumber}";
      }
    }

    Text txtDay = Text("");
    Widget img;
    if(this.widget.isSelected){
      txtDay = Text(strTitle, style: TextStyle(fontSize: 20, color: Colors.blue));
      img = Image.asset("Resources/icons/icons8-green-check-mark-80.png", fit: BoxFit.contain,);
    }else{
      txtDay = Text(strTitle, style: TextStyle(fontSize: 20,));
      img = Container();
    }

    return FlatButton(
      child: Container(
        child: Row(
          children: [
            Container(width: 50, height:25,child: img),
            Container(child: txtDay),
          ],
        ),
        padding: const EdgeInsets.only(bottom: 10, top: 10, right: 8, left: 8),
      ),
      onPressed: (){
        setState(() {
          if(this.widget.isSelected){
            this.widget.isSelected = false;
          }else{
            this.widget.isSelected = true;
          }
        });
      },
      padding: const EdgeInsets.all(0),
    );
  }
}
