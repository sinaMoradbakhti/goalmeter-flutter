import 'package:flutter/material.dart';
import 'package:goal_meter/models/Goal/GoalObject.dart';
import 'package:goal_meter/models/Task/TaskObject.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';

class TodoListItem extends StatefulWidget {
  String dayName = "";
  int day = 0;
  GoalObject myGoal;
  TaskObject myTask;

  TodoListItem({
  int day = 0,
  String dayName = "",
  GoalObject myGoal,
  TaskObject myTask,
  }){
    this.day = day;
    this.dayName = dayName;
    this.myGoal = myGoal;
    this.myTask = myTask;
  }

  @override
  _TodoListItemState createState() => _TodoListItemState();
}
class _TodoListItemState extends State<TodoListItem> {
  @override
  Widget build(BuildContext context) {

    double screenWidth = MediaQuery.of(context).size.width;
    Widget taskImageWgt;
    if(this.widget.myGoal == null){
      taskImageWgt = Container(width: 60, height: 50, color: Colors.grey);
    }else{
      taskImageWgt = Image.asset(this.widget.myGoal.goalImg, height: 50,width: 60, fit: BoxFit.cover,);
    }

    const double mainPaddingSize = 8;
    double taskDataWidth = screenWidth - 45 - 5 - (mainPaddingSize * 2);
    double taskProgress = 0.35;

    if(this.widget.day == 0 && this.widget.dayName == ""){
      taskDataWidth = taskDataWidth + 50 - 16;
      return Container(
        child: Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(5)),
              border: Border.all(color: Colors.grey)
          ),
          width: taskDataWidth,
          height: 50,
          child: Row(
            children: [
              taskImageWgt,
              SizedBox(width: 10),
              Container(width: taskDataWidth - 60 - 40 - 15,
                child: Column(
                  children: [
                    Container(height: 30,child: Text(this.widget.myTask.taskName, style: TextStyle(fontSize: 17, color: Colors.black))),
                    new LinearPercentIndicator(
                      lineHeight: 3,
                      percent: taskProgress,
                      backgroundColor: Colors.grey,
                      progressColor: Colors.green,
                      padding: const EdgeInsets.all(0),
                    ),
                  ],
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                ),
              ),//Data Task
              Container(width: 40,height: 50,child: Icon(Icons.arrow_forward_ios, color: Colors.grey,size: 18,))
            ],
          ), // row Task Image + Progress
        ),
        padding: const EdgeInsets.only(left: mainPaddingSize, right: mainPaddingSize),
        margin: const EdgeInsets.only(bottom: 5),
      );
    }else{
      return Container(
        child: Row(
          children: [
            Container(
              child:Column(
                children: [
                  Text(this.widget.day.toString(), style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black, fontSize: 24)),
                  Text(this.widget.dayName, style: TextStyle(fontWeight: FontWeight.normal, color: Colors.black, fontSize: 18))
                ],
              ),// date column
              width: 45,
            ),
            SizedBox(width: 5),
            Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(5)),
                  border: Border.all(color: Colors.grey)
              ),
              width: taskDataWidth,
              height: 50,
              child: Row(
                children: [
                  taskImageWgt,
                  SizedBox(width: 10),
                  Container(width: taskDataWidth - 60 - 40 - 15,
                    child: Column(
                      children: [
                        Container(height: 30,child: Text(this.widget.myTask.taskName, style: TextStyle(fontSize: 17, color: Colors.black))),
                        new LinearPercentIndicator(
                          lineHeight: 3,
                          percent: taskProgress,
                          backgroundColor: Colors.grey,
                          progressColor: Colors.green,
                          padding: const EdgeInsets.all(0),
                        ),
                      ],
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                    ),
                  ),//Data Task
                  Container(width: 40,height: 50,child: Icon(Icons.arrow_forward_ios, color: Colors.grey,size: 18,))
                ],
              ), // row Task Image + Progress
            )
          ],
        ),// row
        padding: const EdgeInsets.only(left: mainPaddingSize, right: mainPaddingSize),
        margin: const EdgeInsets.only(bottom: 5),
      );
    }
  }
}

class TodoListCoverItem extends StatelessWidget {

  int year = 0;
  int month = 0;
  String imageUrl = "";
  int taskDoneCount = 0;
  int totalTasks = 0;
  TodoListCoverItem({int year = 0,int month = 0,String imageUrl, int taskDoneCount = 0, int totalTasks = 0}){
    this.year = year;
    this.month = month;
    this.imageUrl = imageUrl;
    this.taskDoneCount = taskDoneCount;
    this.totalTasks = totalTasks;
  }

  String convertMonthToName(int month){
    switch(month){
      case 1:
        return "Jan";
        break;
      case 2:
        return "Feb";
        break;
      case 3:
        return "Mar";
        break;
      case 4:
        return "Apr";
        break;
      case 5:
        return "May";
        break;
      case 6:
        return "Jun";
        break;
      case 7:
        return "Jul";
        break;
      case 8:
        return "Aug";
        break;
      case 9:
        return "Sep";
        break;
      case 10:
        return "Oct";
        break;
      case 11:
        return "Nov";
        break;
      case 12:
        return "Dec";
        break;
      default:
        return "Jan";
    }
  }

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    return Container(
      height: 200,
      child: Stack(
        children: [
          Container(child:
            Stack(
              children: [
                Image.asset(this.imageUrl, fit: BoxFit.cover,width: screenWidth,),
                Container(
                  height: 60,
                  width: screenWidth,
                  margin: const EdgeInsets.only(top: 15),
                  child: Padding(padding: const EdgeInsets.only(left: 64),
                    child: RichText(
                      text: TextSpan(
                        children: [
                          TextSpan(
                            text: this.year.toString(),
                            style: TextStyle(fontSize: 28, color: Colors.black, fontWeight: FontWeight.w700)
                          ),
                          TextSpan(
                              text: "     ",
                          ),
                          TextSpan(
                              text: convertMonthToName(this.month),
                              style: TextStyle(fontSize: 26, color: Colors.black, fontWeight: FontWeight.w500)
                          ),
                        ]
                      ),
                    ),
                  ),
                  alignment: Alignment.centerLeft,
                ),
              ],
            )
          ),
          Container(
            color: Color.fromRGBO(0, 0, 0, 0.6),
            height: 60,
            width: screenWidth,
            margin: const EdgeInsets.only(top: 140),
            child: Padding(padding: const EdgeInsets.only(right: 16),
              child: Text("${this.taskDoneCount}/${this.totalTasks} tasks done", style: TextStyle(fontSize: 21, color: Colors.white),textAlign: TextAlign.right,),
            ),
            alignment: Alignment.centerRight,
          ),
        ],
      ),
    );
  }
}

class TodoListWeeklySplitorItem extends StatelessWidget {
  String title = "";

  TodoListWeeklySplitorItem({String title = ""}){
    this.title = title;
  }

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    return Container(
      child: Stack(children: [
        Container(width: screenWidth, height: 1,decoration: BoxDecoration(
            border: Border(top: BorderSide(color: Colors.grey))
        ),
            margin: const EdgeInsets.only(top: 10)
        ),
        Container(
          child: Text(" ${title} ", style: TextStyle(fontSize: 14, backgroundColor: Colors.white),textAlign: TextAlign.center,),
          alignment: Alignment.center,
        ),
      ],
      ),
      margin: const EdgeInsets.only(top: 5, bottom: 5),
    );
  }
}

class TodoListGroupedBox extends StatelessWidget {

  List<Widget> children = [];

  TodoListGroupedBox({List<Widget> chldren}){
    this.children = chldren;
  }

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    return Container(
      decoration: BoxDecoration(
        border: Border.all(color: Colors.grey),
        borderRadius: BorderRadius.all(Radius.circular(5)),
      ),
      child: Column(
        children: this.children,
      ),
      padding: const EdgeInsets.only(top: 10),
      margin: const EdgeInsets.only(left: 8, right: 8),
    );
  }
}



