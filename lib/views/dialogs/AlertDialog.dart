import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/widgets.dart';
import 'package:goal_meter/models/Statics/Statics.dart';
import 'package:goal_meter/models/Statics/Strings.dart';

class PopUpDialog
{
  VoidCallback onPressForEver;
  VoidCallback onPressPickDate;
  double popUpHeight = 0;
  double popUpWidth = 0;

  PopUpDialog({
    VoidCallback onPressForEver,
    VoidCallback onPressPickDate,
    double popUpWidth = 0,
    double popUpHeight = 0,
  }){
    this.onPressForEver = onPressForEver;
    this.onPressPickDate = onPressPickDate;
    this.popUpWidth = popUpWidth;
    this.popUpHeight = popUpHeight;
  }

  AlertDialog dialog(){

    return AlertDialog(
      content: Container(
        child: Column(
          children: [
            FlatButton(
              child: Container(
                child: Row(
                  children: [
                    Container(width: 50,)
                    ,
                    Text(Strings.shared.forEverCamelCap, style: TextStyle(fontSize: 19),)
                  ],
                ),
              ),
              onPressed: (){
                onPressForEver();
              },
            ),
            Container(decoration: BoxDecoration(
              border: Border(top: BorderSide(
                color: Colors.grey,
                width: 1,
              )),
            ),
              height: 1,
              margin: const EdgeInsets.only(top: 5, bottom: 5),
            ),
            FlatButton(
              child: Container(
                child: Row(
                  children: [
                    Container(width: 50,child: Image.asset("Resources/icons/calenderIcon.png", width: 30,height: 30, fit: BoxFit.contain,),
                      alignment: Alignment.center,
                    )
                    ,
                    Text(Strings.shared.pickADate, style: TextStyle(fontSize: 19),)
                  ],
                ),
              ),
              onPressed: (){
                onPressPickDate();
              },
            ),
          ], // Column Children
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
        ), // Column
        width: popUpWidth,
        padding: const EdgeInsets.only(top: 15, bottom: 15),
        //height: 330,
      ), // Content Container
      backgroundColor: Colors.white,
      contentPadding: const EdgeInsets.all(0),
    );
  }

}