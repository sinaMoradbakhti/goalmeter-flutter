import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/widgets.dart';
import 'package:goal_meter/models/Statics/Statics.dart';
import 'package:goal_meter/models/Statics/Strings.dart';
import 'package:goal_meter/views/MonthlyDayItem/MonthlyDayItem.dart';


class PopUpDialogMonthDayPicker
{
  VoidCallback onPressClose;
  VoidCallback onPressSave;

  double popUpHeight = 0;
  double popUpWidth = 0;
  var myContext;
  List<MonthlyDayItem> myList = [];
  List<int> returnMyList = [];

  PopUpDialogMonthDayPicker({
    VoidCallback onPressClose,
    VoidCallback onPressSave,
    double popUpWidth = 0,
    double popUpHeight = 0,
    var context,
  }){
    this.onPressClose = onPressClose;
    this.onPressSave = onPressSave;
    this.popUpWidth = popUpWidth;
    this.popUpHeight = popUpHeight;
    this.myContext = context;
  }


  AlertDialog dialog({List<int> selectedList}){

    List<MonthlyDayItem> myMonthlyList = [];
    for(int i = 0; i < 31; i++){
      if(i == 30){
        if(selectedList.contains(i+1)){
          myMonthlyList.add(MonthlyDayItem(dayNumber: i+1,isLastDayOfMonth: true, isChecked: true,));
        }else{
          myMonthlyList.add(MonthlyDayItem(dayNumber: i+1,isLastDayOfMonth: true));
        }
      }
      else{
        if(selectedList.contains(i+1)){
          myMonthlyList.add(MonthlyDayItem(dayNumber: i+1,isChecked: true,));
        }else{
          myMonthlyList.add(MonthlyDayItem(dayNumber: i+1,));
        }
      }
    }// for loop
    this.myList = myMonthlyList;

    return AlertDialog(
      content: Container(
        child: Column(
          children:
          [
            Container(
              child: ListView(
                children: myMonthlyList,
              ),
              height: MediaQuery.of(this.myContext).size.height / 1.5,
              padding: const EdgeInsets.only(top: 40, bottom: 40),
            ),
            Container(decoration: BoxDecoration(
              border: Border(top: BorderSide(
                color: Colors.grey,
                width: 1,
              )),
            ),
              height: 1,
              margin: const EdgeInsets.only(top: 5, bottom: 5),
            ),
            Container(
              child: Row(
                children: [
                  Container(),
                  FlatButton(
                    child: Text(Strings.shared.closeWordCamelCap, style: TextStyle(fontSize: 19,color: Colors.grey),),
                    onPressed: (){
                      // close pop up
                      this.onPressClose();
                    },
                  ),
                  FlatButton(
                    child: Text(Strings.shared.saveWordCamelCap, style: TextStyle(fontSize: 19),),
                    onPressed: (){
                      // save pop up
                      List<int> days = [];
                      for(int i = 0; i < this.myList.length; i++){
                        MonthlyDayItem item = this.myList[i];
                        if(item.isSelected){
                         days.add(item.dayNumber);
                        }
                      }// for loop
                      this.returnMyList = days;
                      this.onPressSave();
                    },
                  )
                ],
                mainAxisAlignment: MainAxisAlignment.end,
              ),
            )
          ], // Column Children
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
        ), // Column
        width: popUpWidth,
        //height: 330,
      ), // Content Container
      backgroundColor: Colors.white,
      contentPadding: const EdgeInsets.all(0),
    );
  }

}