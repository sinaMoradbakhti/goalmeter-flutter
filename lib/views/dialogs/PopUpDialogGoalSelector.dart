import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/widgets.dart';
import 'package:goal_meter/models/Goal/MyGoalObject.dart';
import 'package:goal_meter/models/Statics/Statics.dart';
import 'package:goal_meter/views/AddTask/TaskWidget.dart';
import 'package:goal_meter/models/Statics/Strings.dart';

class PopUpDialogGoalSelector
{
  VoidCallback onPressOk;

  double popUpHeight = 0;
  double popUpWidth = 0;
  String selectedGoal = "";
  List<MyGoalObject> myList = [];

  PopUpDialogGoalSelector({
    VoidCallback onPressOk,
    double popUpWidth = 0,
    double popUpHeight = 0,
    String selectedGoal = "",
  }){
    this.onPressOk = onPressOk;
    this.popUpWidth = popUpWidth;
    this.popUpHeight = popUpHeight;
    this.selectedGoal = selectedGoal;
  }

  AlertDialog dialog({String selectedGoalId, onGetGoalId(MyGoalObject goal), List<MyGoalObject> myList}){

    this.myList = myList;

    bool isSelectedAllGoal = false;
    bool isSelectedGoal = false;
    if(this.selectedGoal == ""){
      isSelectedAllGoal = true;
      isSelectedGoal = false;
    }
    TaskWidgetSelectable noGoal = TaskWidgetSelectable(
      task: TaskWidget(
        title: Strings.shared.allGoalCamelCapWithFirst,
        imgName: "",
        img: null,
      ),// TaskWidget
      onSelectItem: (){
        onPressOk();
        MyGoalObject goal;
        onGetGoalId(goal);
      },
      isSelected: isSelectedAllGoal,
    );// TaskWidgetSelectable

    List<TaskWidgetSelectable> otherItems = [];
    for(int i = 0; i < this.myList.length; i++){

      if(this.myList[i].gId == this.selectedGoal){
        isSelectedGoal = true;
      }else{
        isSelectedGoal = false;
      }

      TaskWidgetSelectable tsk = TaskWidgetSelectable(
        task: TaskWidget(
          title: this.myList[i].goal.goalTitle,
          imgName: this.myList[i].goal.goalImg,
          myGoal: this.myList[i],
          img: Image.asset(this.myList[i].goal.goalImg, width: 50, height: 100, fit: BoxFit.cover),
        ),// TaskWidget
        onSelectItem: (){
          onPressOk();
          onGetGoalId(this.myList[i]);
        },
        isSelected: isSelectedGoal,
      );// TaskWidgetSelectable
      otherItems.add(tsk);
    }

    return AlertDialog(
      content: Container(
        child: Column(
          children:
          [
            Container(child: noGoal,
              padding: const EdgeInsets.only(top: 16, bottom: 16),
            ),
            Container(
              child: ListView(
                children: otherItems,
              ),
              height: popUpHeight / 1.5,
              padding: const EdgeInsets.only(top: 0, bottom: 0),
            ),
          ], // Column Children
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
        ), // Column
        width: popUpWidth,
        //height: 330,
      ), // Content Container
      backgroundColor: Colors.white,
      contentPadding: const EdgeInsets.all(0),
    );
  }

}