import 'package:flutter/material.dart';
import 'package:goal_meter/controllers/mainTabbar/anotherController/Goals/MiddleWare.dart';
import 'package:goal_meter/models/Statics/Strings.dart';
import 'package:goal_meter/models/Goal/MyGoalObject.dart';
import 'package:goal_meter/views/GoalProgressWidget/GoalProgressWidget.dart';

class MyGoalWidget extends StatefulWidget {

  MyGoalObject myGoal;
  bool isYourGoal = true;
  VoidCallback onPressGoal;
  VoidCallback goalMoreClicked;
  VoidCallback onPressAddGoalBtn;

  MyGoalWidget({MyGoalObject goal,bool isYourGoal,VoidCallback onPressGoal, VoidCallback goalMoreClicked, VoidCallback onPressAddGoalBtn})
  {
    this.myGoal = goal;
    this.isYourGoal = isYourGoal;
    this.onPressGoal = onPressGoal;
    this.goalMoreClicked = goalMoreClicked;
    this.onPressAddGoalBtn = onPressAddGoalBtn;
  }

  @override
  _MyGoalWidgetState createState() => _MyGoalWidgetState();
}

class _MyGoalWidgetState extends State<MyGoalWidget> {
  @override
  Widget build(BuildContext context) {
    if(this.widget.isYourGoal)
    {
      PopupMenuButton popupMore = PopupMenuButton(
        itemBuilder: (buildContext) => <PopupMenuItem<String>>[
          new PopupMenuItem<String>(
              child: const Text('Delete'), value: 'Delete'),
        ],
          onSelected: (index) {
          this.widget.goalMoreClicked();
          },
        icon: Icon(Icons.more_vert, color: Colors.white),
      );
      return FlatButton(
        child: Container(
          //height: 200,
          decoration: new BoxDecoration(
            image: new DecorationImage(
              image: new AssetImage(this.widget.myGoal.goal.goalImg),
              fit: BoxFit.cover,
            ),
            borderRadius: BorderRadius.all(Radius.circular(8)),
          ),
          margin: const EdgeInsets.only(bottom: 15),

          child: Container(
            child: Column(
              children: [
                Container(
                  child: Stack(
                    children: [
                      Container(child: Text(this.widget.myGoal.goal.goalTitle,
                        style: TextStyle(color: Colors.white,fontSize: 22),
                      ),
                        alignment: Alignment.center,
                        width: MediaQuery.of(context).size.width,
                      ),
                      Container(child: popupMore,width: 40,),
                    ],
                    alignment: Alignment.centerRight,
                  ),
                  alignment: Alignment.center,
                  padding: const EdgeInsets.only(top: 0),
                  width: MediaQuery.of(context).size.width,
                ),
                firstRow(context)
              ],
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(8)),
              color: Color.fromRGBO(0, 0, 0, 0.5),
            ),
          ),
        ),
        onPressed: (){
          this.widget.onPressGoal();
        },
        padding: const EdgeInsets.all(0),
      );
    }
    else {
        Color btnAddGoal = Colors.grey;
        return Container(
          height: 55,
          color: Colors.white,
          margin: const EdgeInsets.only(bottom: 10),
          child: OutlineButton(
            onPressed: (){
              this.widget.onPressAddGoalBtn();
            },
            color: btnAddGoal,
            textColor: btnAddGoal,
            child: Stack(
              children: [
                Container(
                  child: Text(Strings.shared.goalsControllerBtnAddNewGoal, style: TextStyle(color: btnAddGoal, fontSize: 17, fontWeight: FontWeight.normal), textAlign: TextAlign.center),
                  width: MiddleWare.shared.windowWidth - 16 - 30,
                  alignment: Alignment.center,
                ),
                Container(width: 30, child: Icon(Icons.add_circle_outline, color: btnAddGoal), alignment: Alignment.center,)
              ],
            ),
          ),
        );
      }
  }

  Container firstRow(mainContext)
  {
    double progressWidth = MediaQuery.of(mainContext).size.width;
    return Container(
      child: Stack(
        children: [
          Column(
            children: [
              Container(
                  height: 35,
                  margin: const EdgeInsets.only(top: 20),
                  padding: const EdgeInsets.only(left: 16,right: 16),
                  child: Row(
                    children: [
                      Container(width: (MiddleWare.shared.windowWidth - 64 - 32) / 2, child: Row(
                        children: [
                          Container(child: Image.asset("Resources/icons/CreateGoal/charts.png",)),
                          Container(child: SizedBox(width: 5)),
                          Container(child: Text(MiddleWare.shared.progressTitle,style: TextStyle(color: Colors.white,fontSize: 17),
                              textAlign: TextAlign.right),alignment: Alignment.center)
                        ],
                      ),),
                      Container(
                          child: Text(MiddleWare.shared.progressValue,
                              style: TextStyle(color: Colors.white,fontSize: 17),
                              textAlign: TextAlign.right),
                          width: (MiddleWare.shared.windowWidth - 64 - 32) / 2, alignment: Alignment.centerRight)
                    ],
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                  )),
              Container(
                  height: 35,
                  padding: const EdgeInsets.only(left: 16,right: 16),
                  child: Row(
                    children: [
                      Container(width: (MiddleWare.shared.windowWidth - 64 - 32) / 2, child: Row(
                        children: [
                          Container(child: Image.asset("Resources/icons/CreateGoal/miniMedal.png")),
                          Container(child: SizedBox(width: 5)),
                          Container(child: Text(MiddleWare.shared.levelTitle,style: TextStyle(color: Colors.white,fontSize: 17),
                              textAlign: TextAlign.right),alignment: Alignment.center)
                        ],
                      ),),
                      Container(
                          child: Text(MiddleWare.shared.levelValue,
                              style: TextStyle(color: Colors.white,fontSize: 17),
                              textAlign: TextAlign.right),
                          width: (MiddleWare.shared.windowWidth - 64 - 32) / 2, alignment: Alignment.centerRight)
                    ],
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                  )),
              SizedBox(height: 20,),
              Container(child: GoalProgressWidget(progressValuePercent: 0,height: 35, width: progressWidth,progressTotal: 100,borderColor: Colors.white,),
                padding: const EdgeInsets.only(top: 8,bottom: 8, left: 16, right: 16),
              ),
            ],
          ),
        ],
      ),
    );
  }
}


//LinearProgressIndicator(
//value: 0.15,
//backgroundColor: Color.fromRGBO(255, 255, 255, 0.3),
//valueColor: new AlwaysStoppedAnimation<Color>(Colors.green),
//)